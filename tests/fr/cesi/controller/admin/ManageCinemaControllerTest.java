package fr.cesi.controller.admin;

import fr.cesi.controller.TestBase;
import fr.cesi.model.Movie;
import fr.cesi.model.Tarif;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import org.junit.Test;

import static org.junit.Assert.*;

public class ManageCinemaControllerTest extends TestBase {

    private TextField tarifName;
    private TextField tarifValue;
    private Button addTarifBtn;
    private TableView<Tarif> tarifTable;

    public ManageCinemaControllerTest() {
        super("view/admin/ManagePrice.fxml");
    }

    @Override
    public void beforeTest() {
        this.tarifName = find("#tarifName");
        this.tarifValue = find("#tarifValue");
        this.addTarifBtn = find("#addTarifBtn");
        this.tarifTable = find("#tarifTable");
    }

    @Test
    public void addPriceTest() {
        this.tarifName.setText("Test");
        this.tarifValue.setText("10.5");
        clickOn(this.addTarifBtn);

        // Get items on table
        SelectionModel selectionModel = this.tarifTable.getSelectionModel();
        selectionModel.select(this.tarifTable.getItems().size() - 1);
        Tarif tarif = (Tarif) selectionModel.getSelectedItem();
        assertEquals("Test", tarif.getName());
        assertEquals(10.5, tarif.getPrice(), 3);
    }
}