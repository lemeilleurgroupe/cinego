package fr.cesi.controller.admin;

import fr.cesi.controller.TestBase;
import fr.cesi.state.AdminState;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AdminLoginControllerTest extends TestBase {

    private TextField username;
    private PasswordField password;

    private Button loginButton;
    private Label connectionLabel;

    public AdminLoginControllerTest() {
        super("view/admin/ConnexionAdmin.fxml");
    }


    @Before
    public void beforeTest() {
        /* Just retrieving the tested widgets from the GUI. */
        this.username = find("#txtLogin");
        this.password = find("#txtMdp");
        this.loginButton = find("#btnConnexion");
        this.connectionLabel = find("#lblResult");
    }

    @Test
    public void loginAdminTest() {
        assertEquals("", this.connectionLabel.getText());

        String login = "Loick";
        String password = "Loick";

        this.username.setText(login);
        assertEquals(login, this.username.getText());

        this.password.setText(password);
        assertEquals(password, this.password.getText());

        clickOn(this.loginButton);

        assertEquals("Connexion réussie", this.connectionLabel.getText());
        assertNotNull(AdminState.getState().getConnectedUser());
        assertEquals("Virot", AdminState.getState().getConnectedUser().getNom());
        assertEquals("Loick", AdminState.getState().getConnectedUser().getPrenom());
        assertEquals("Loick", AdminState.getState().getConnectedUser().getEmail());
        assertEquals("89562055bc02c93fd030ccf1054c1866ec915f9480eb1c4aef3539a857383f8f", AdminState.getState().getConnectedUser().getPassword());
    }

    @Test
    public void loginAdminErrorTest() {
        assertEquals("", this.connectionLabel.getText());

        String login = "azertyuiop";
        String password = "azrtyuiop";

        this.username.setText(login);
        assertEquals(login, this.username.getText());

        this.password.setText(password);
        assertEquals(password, this.password.getText());

        clickOn(this.loginButton);

        assertEquals("Login ou mot de passe incorrect", this.connectionLabel.getText());
        assertEquals(null, AdminState.getState().getConnectedUser());
    }
}