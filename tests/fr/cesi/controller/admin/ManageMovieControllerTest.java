package fr.cesi.controller.admin;

import fr.cesi.controller.TestBase;
import fr.cesi.model.Movie;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.junit.Test;
import org.testfx.util.WaitForAsyncUtils;

import java.awt.*;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class ManageMovieControllerTest extends TestBase {

    private TextField searchField;
    private ListView<Movie> moviesList;
    private Button searchBtn;
    private Button addBtn;
    private Button deleteBtn;
    private TableView<Movie> movieTable;

    public ManageMovieControllerTest() {
        super("view/admin/ManageMovie.fxml");
    }

    @Override
    public void beforeTest() {
        this.searchField = find("#searchField");
        this.moviesList = find("#moviesList");
        this.searchBtn = find("#searchBtn");
        this.addBtn = find("#addBtn");
        this.deleteBtn = find("#deleteBtn");
        this.movieTable = find("#movieTable");
    }

    @Test
    public void searchButtonClick() {
        Movie testMovie = searchAndSelectTotalRecall();
        assertEquals("Total Recall", testMovie.getTitle());
    }

    @Test
    public void addAndRemoveMovieButtonClick() {
        Movie testMovie = searchAndSelectTotalRecall();
        SelectionModel<Movie> selector = this.moviesList.getSelectionModel();
        selector.select(testMovie);
        clickOn(this.addBtn);
        clickOn("OK");

        // is on table
        selector = this.movieTable.getSelectionModel();
        selector.select(this.movieTable.getItems().size() - 1);
        Movie movieTable = this.movieTable.getSelectionModel().getSelectedItem();
        assertEquals(testMovie.toString(), movieTable.toString());

        // remove movie
        clickOn(this.deleteBtn);
        clickOn("OK");
        ObservableList<Movie> movies = this.movieTable.getItems();
        int moviePos = movies.indexOf(testMovie);
        assertEquals(-1, moviePos);
        clickOn("OK");
    }

    private Movie searchAndSelectTotalRecall() {
        String movieSearch = "Total recall";
        this.searchField.setText(movieSearch);
        clickOn(this.searchBtn);
        ObservableList<Movie> movies = this.moviesList.getItems();
        return movies.get(1);
    }
}