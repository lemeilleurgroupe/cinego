package fr.cesi.controller;

import fr.cesi.CineGo;
import fr.cesi.config.Database;
import fr.cesi.model.DAO.CinemaDAO;
import fr.cesi.state.AdminState;
import fr.cesi.state.ClientState;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;
import org.junit.After;
import org.junit.Before;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit.ApplicationTest;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.TimeoutException;

public abstract class TestBase extends ApplicationTest {

    private Parent mainNode;
    private String pageLocation;
    private Connection connection;

    /* This operation comes from ApplicationTest and loads the GUI to test. */
    @Override
    public void start(Stage stage) throws Exception {

        // Connection to the test DATABASE
        Database.useDatabaseTest().getConnection();

        // Init admin states
        CinemaDAO cineDao = new CinemaDAO();
        AdminState.getState().setCinema(cineDao.findById(5));
        ClientState.getState().setSelectedCinema(cineDao.findById(5));

        FXMLLoader loader = new FXMLLoader();
        this.mainNode = FXMLLoader.load(CineGo.class.getResource(this.pageLocation));
        stage.setScene(new Scene(mainNode));
        stage.show();

        /* Do not forget to put the GUI in front of windows. Otherwise, the robots may interact with another
        window, the one in front of all the windows... */
        stage.toFront();
    }

    /* Just a shortcut to retrieve widgets in the GUI. */
    public <T extends Node> T find(final String query) {
        /** TestFX provides many operations to retrieve elements from the loaded GUI. */
        return lookup(query).query();
    }

    @Before
    public void setUp() {
        this.connection = Database.getInstance().getConnection();
        try {
            connection.setAutoCommit(false); // do not save database modifications
        } catch (SQLException e) {
            e.printStackTrace();
        }
        this.beforeTest();
    }

    /**
     * Elements to call before the test begins
     */
    public abstract void beforeTest();

    /* IMO, it is quite recommended to clear the ongoing events, in case of. */
    @After
    public void tearDown() throws TimeoutException {
        /* Close the window. It will be re-opened at the next test. */
        FxToolkit.hideStage();
        release(new KeyCode[]{});
        release(new MouseButton[]{});

        // rollback database
        try {
            this.connection.rollback();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public TestBase(String pageLocation) {
        this.pageLocation = pageLocation;
    }
}