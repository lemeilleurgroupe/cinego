package fr.cesi.controller.user;

import fr.cesi.controller.TestBase;
import fr.cesi.state.ClientState;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.junit.Test;

import java.sql.Date;

import static org.junit.Assert.*;

public class ConnexionClientControllerTest extends TestBase {


    private TextField txtMail;
    private PasswordField txtMdp;
    private Button btnConnexion;
    private Label lblResult;

    public ConnexionClientControllerTest() {
        super("view/user/ConnexionClient.fxml");
    }

    @Override
    public void beforeTest() {
        this.txtMail = find("#txtMail");
        this.txtMdp = find("#txtMdp");
        this.btnConnexion = find("#btnConnexion");
        this.lblResult = find("#lblResult");
    }

    @Test
    public void connectionTest() {
        clickOn(this.txtMail);
        write("already@inserted.fr");
        clickOn(this.txtMdp);
        write("123456");
        clickOn(this.btnConnexion);

        assertEquals("success", this.lblResult.getText());
        assertEquals("George", ClientState.getState().getConnectedUser().getSurname());
        assertEquals("Michael", ClientState.getState().getConnectedUser().getLastname());
        assertEquals(Date.valueOf("2018-01-18"), ClientState.getState().getConnectedUser().getBirthday());
        assertEquals("9999999999", ClientState.getState().getConnectedUser().getNumCard());
        assertEquals("already@inserted.fr", ClientState.getState().getConnectedUser().getMail());
        assertEquals("8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92", ClientState.getState().getConnectedUser().getPassword());

    }

    @Test
    public void connectionFalsePasswordTest() {
        clickOn(this.txtMail);
        write("already@inserted.fr");
        clickOn(this.txtMdp);
        write("nope");
        clickOn(this.btnConnexion);

        assertEquals("Email ou mot de passe incorrect", this.lblResult.getText());
        assertEquals(null, ClientState.getState().getConnectedUser());
    }


    @Test
    public void connectionFalseEmailTest() {
        clickOn(this.txtMail);
        write("test@azertyuiop.fr");
        clickOn(this.txtMdp);
        write("123456");
        clickOn(this.btnConnexion);

        assertEquals("Email ou mot de passe incorrect", this.lblResult.getText());
        assertEquals(null, ClientState.getState().getConnectedUser());

    }
}