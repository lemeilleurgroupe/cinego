package fr.cesi.controller.user;

import fr.cesi.controller.TestBase;
import fr.cesi.model.Cinema;
import fr.cesi.model.DAO.CinemaDAO;
import fr.cesi.model.Tarif;
import fr.cesi.state.ClientState;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Pair;
import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import static org.junit.Assert.*;

public class ChoixBilletControllerTest extends TestBase {


    private VBox tarifVBox;
    private Button moreButton;
    private Button lessButton;
    private Label counter;

    public ChoixBilletControllerTest() {
        super("view/user/ChoixBillets.fxml");
    }

    @Override
    public void beforeTest() {
        this.tarifVBox = find("#tarifVBox");
        HBox firstHBox = (HBox) tarifVBox.getChildren().get(0);
        this.lessButton = (Button) firstHBox.getChildren().get(1);
        this.moreButton = (Button) firstHBox.getChildren().get(3);
        this.counter = (Label) firstHBox.getChildren().get(2);
    }

    @Test
    public void showAllCinemaTarifs() {
        assertEquals(4, tarifVBox.getChildren().size());
    }

    @Test
    public void clickOnButtonsChangeCounter() {
        clickOn(moreButton);
        assertEquals(1, Integer.parseInt(this.counter.getText()));
        clickOn(moreButton);
        assertEquals(2, Integer.parseInt(this.counter.getText()));
        clickOn(moreButton);
        assertEquals(3, Integer.parseInt(this.counter.getText()));
        clickOn(lessButton);
        assertEquals(2, Integer.parseInt(this.counter.getText()));
        clickOn(lessButton);
        assertEquals(1, Integer.parseInt(this.counter.getText()));
        clickOn(lessButton);
        assertEquals(0, Integer.parseInt(this.counter.getText()));
    }

    @Test
    public void nbPlacePossibleCantGoUnderZero() {
        clickOn(lessButton);
        clickOn(lessButton);
        clickOn(lessButton);
        assertEquals(0, Integer.parseInt(this.counter.getText()));
    }

    @Test
    public void nbPlacePossibleCantGoLargerThanAvailablesPlaces() {
        // TODO
        // Get seance choosen
        // Get nb places availables
        // click "nb places availables" + 5 times on moreButton
        // assert if counter equals to "nb places availables"
    }

    @Test
    public void placesStoredIntoClientState() {
        clickOn(moreButton);
        clickOn(moreButton);
        HBox firstHBox = (HBox) tarifVBox.getChildren().get(2);
        Button secondMoreButton = (Button) firstHBox.getChildren().get(3);
        clickOn(secondMoreButton);
        ArrayList<Pair<Integer, Tarif>> billets = ClientState.getState().getBillets();
        // Click on continue button
        assertEquals(2, billets.size());
        assertEquals(2, java.util.Optional.ofNullable(billets.get(4).getKey()).get().intValue());
        assertEquals(1, java.util.Optional.ofNullable(billets.get(11).getKey()).get().intValue());
    }
}