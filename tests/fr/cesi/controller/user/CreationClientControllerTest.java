package fr.cesi.controller.user;

import fr.cesi.controller.TestBase;
import fr.cesi.model.Client;
import fr.cesi.model.DAO.ClientDAO;
import javafx.scene.control.*;
import org.junit.Test;

import java.sql.Date;
import java.sql.SQLException;

import static org.junit.Assert.*;

public class CreationClientControllerTest extends TestBase {

    private TextField txtNumCarte;
    private TextField txtNom;
    private TextField txtPrenom;
    private DatePicker txtDateNaissance;
    private TextField txtMail;
    private PasswordField txtMdp;
    private PasswordField txtMdpConfirm;
    private Button confirmBtn;
    private Label errorMessage;

    public CreationClientControllerTest() {
        super("view/user/CreationCompteClient.fxml");
    }

    @Override
    public void beforeTest() {
        this.txtNumCarte = find("#txtNumCarte");
        this.txtNom = find("#txtNom");
        this.txtPrenom = find("#txtPrenom");
        this.txtDateNaissance = find("#txtDateNaissance");
        this.txtMail = find("#txtMail");
        this.txtMdp = find("#txtMdp");
        this.txtMdpConfirm = find("#txtMdpConfirm");
        this.confirmBtn = find("#confirmBtn");
        this.errorMessage = find("#errorMessage");
    }

    @Test
    public void registerTest () {
        clickOn(this.txtNumCarte);
        write("1234567890");
        clickOn(this.txtNom);
        write("Doe");
        clickOn(this.txtPrenom);
        write("Michel");
        clickOn(this.txtDateNaissance);
        write("19/08/1997");
        clickOn(this.txtMail);
        write("test@test.fr");
        clickOn(this.txtMdp);
        write("test");
        clickOn(this.txtMdpConfirm);
        write("test");

        clickOn(this.confirmBtn);
        clickOn("OK");

        ClientDAO dao = new ClientDAO();
        Client client = null;
        try {
            client = dao.findByEmail("test@test.fr");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        assertNotNull(client);
        assertEquals("1234567890", client.getNumCard());
        assertEquals("Doe", client.getLastname());
        assertEquals("Michel", client.getSurname());
        assertEquals(Date.valueOf(this.txtDateNaissance.getValue()), client.getBirthday());
        assertEquals("test@test.fr", client.getMail());
        assertNotEquals("test", client.getPassword());
    }

    @Test
    public void registerWithoutCardTest () {
        clickOn(this.txtNom);
        write("Doe");
        clickOn(this.txtPrenom);
        write("Michel");
        clickOn(this.txtDateNaissance);
        write("19/08/1997");
        clickOn(this.txtMail);
        write("test@test.fr");
        clickOn(this.txtMdp);
        write("test");
        clickOn(this.txtMdpConfirm);
        write("test");

        clickOn(this.confirmBtn);
        clickOn("OK");

        ClientDAO dao = new ClientDAO();
        Client client = null;
        try {
            client = dao.findByEmail("test@test.fr");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        assertNotNull(client);
        assertEquals(null, client.getNumCard());
        assertEquals("Doe", client.getLastname());
        assertEquals("Michel", client.getSurname());
        assertEquals(Date.valueOf(this.txtDateNaissance.getValue()), client.getBirthday());
        assertEquals("test@test.fr", client.getMail());
        assertNotEquals("test", client.getPassword());
    }

    @Test
    public void registerNotAllFieldsTest () {
        clickOn(this.txtNumCarte);
        write("1234567890");
        clickOn(this.txtNom);
        write("Doe");
        clickOn(this.txtPrenom);
        write("Michel");
        clickOn(this.txtDateNaissance);
        write("19/08/1997");
        clickOn(this.txtMdp);
        write("test");
        clickOn(this.txtMdpConfirm);
        write("test");

        clickOn(this.confirmBtn);
        assertEquals("Vous devez remplir tous les champs obligatoires", this.errorMessage.getText());
    }

    @Test
    public void registerNotAnEmailTest () {
        clickOn(this.txtNumCarte);
        write("1234567890");
        clickOn(this.txtNom);
        write("Doe");
        clickOn(this.txtPrenom);
        write("Michel");
        clickOn(this.txtDateNaissance);
        write("19/08/1997");
        clickOn(this.txtMail);
        write("yoloooo");
        clickOn(this.txtMdp);
        write("test");
        clickOn(this.txtMdpConfirm);
        write("test");

        clickOn(this.confirmBtn);

        assertEquals("Erreur dans l'adresse email", this.errorMessage.getText());
    }

    @Test
    public void registerPasswordFailTest () {
        clickOn(this.txtNumCarte);
        write("1234567890");
        clickOn(this.txtNom);
        write("Doe");
        clickOn(this.txtPrenom);
        write("Michel");
        clickOn(this.txtDateNaissance);
        write("19/08/1997");
        clickOn(this.txtMail);
        write("test@test.fr");
        clickOn(this.txtMdp);
        write("test");
        clickOn(this.txtMdpConfirm);
        write("testsss");

        clickOn(this.confirmBtn);

        assertEquals("Les deux passwords ne correspondent pas", this.errorMessage.getText());
    }

    @Test
    public void registerEmailFailTest () {
        clickOn(this.txtNumCarte);
        write("1234567890");
        clickOn(this.txtNom);
        write("Doe");
        clickOn(this.txtPrenom);
        write("Michel");
        clickOn(this.txtDateNaissance);
        write("19/08/1997");
        clickOn(this.txtMail);
        write("already@inserted.fr");
        clickOn(this.txtMdp);
        write("test");
        clickOn(this.txtMdpConfirm);
        write("test");

        clickOn(this.confirmBtn);

        assertEquals("Erreur lors de l'insertion en base de donnée : Duplicate entry 'already@inserted.fr' for key 'CLIENT_Mail'", this.errorMessage.getText());
    }

    @Test
    public void registerNumCardFailTest () {
        clickOn(this.txtNumCarte);
        write("9999999999");
        clickOn(this.txtNom);
        write("Doe");
        clickOn(this.txtPrenom);
        write("Michel");
        clickOn(this.txtDateNaissance);
        write("19/08/1997");
        clickOn(this.txtMail);
        write("test@test.fr");
        clickOn(this.txtMdp);
        write("test");
        clickOn(this.txtMdpConfirm);
        write("test");

        clickOn(this.confirmBtn);

        assertEquals("Erreur lors de l'insertion en base de donnée : Duplicate entry '9999999999' for key 'CLIENT_NumCarte'", this.errorMessage.getText());
    }

    @Test
    public void registerYoloTest () {
        clickOn(this.txtNumCarte);
        write("kfsdfds");
        clickOn(this.txtNom);
        write("dfsdfds");
        clickOn(this.txtPrenom);
        write("sdfsdfds");
        clickOn(this.txtDateNaissance);
        write("dfsdfdsf");
        clickOn(this.txtMail);
        write("dfsdfsdfds");
        clickOn(this.txtMdp);
        write("dsfswfs");
        clickOn(this.txtMdpConfirm);
        write("tdfest");

        clickOn(this.confirmBtn);

        assertEquals("Vous devez remplir tous les champs obligatoires", this.errorMessage.getText());
    }
}