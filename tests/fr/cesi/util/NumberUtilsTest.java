package fr.cesi.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class NumberUtilsTest {

    @Test
    public void isNumberShouldReturnTrue() {
        String number = "1234";
        assertTrue(NumberUtils.isNumber(number));
    }

    @Test
    public void isNumberShouldReturnFalse() {
        String number = "azertyuiop";
        assertFalse(NumberUtils.isNumber(number));
    }

    @Test
    public void isNumberWithSizeShouldReturnTrue() {
        String number = "123456";
        assertTrue(NumberUtils.isNumberWithSize(number, 6));
    }


    @Test
    public void isNumberWithSizeShouldReturnFalse() {
        String number = "azerty";
        assertFalse(NumberUtils.isNumberWithSize(number, 6));

        number = "123";
        assertFalse(NumberUtils.isNumberWithSize(number, 6));

        number = "123456789";
        assertFalse(NumberUtils.isNumberWithSize(number, 6));
    }
}