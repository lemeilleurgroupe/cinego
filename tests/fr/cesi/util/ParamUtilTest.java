package fr.cesi.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class ParamUtilTest {

    @Test
    public void shouldReturnActualCinemaId() {
        int param = (int) ParamUtil.getParamUtil().getParam("cinemaId");
        assertEquals(5, param);
    }

    @Test
    public void shouldReturnAParameterNotInRootObject() {
        String param = (String) ParamUtil.getParamUtil().getParam("TMDb.url");
        assertEquals("http://api.themoviedb.org/3/", param);
    }

}