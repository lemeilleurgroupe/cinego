package fr.cesi.service;

import fr.cesi.model.DAO.TarifDAO;
import fr.cesi.model.Tarif;
import javafx.util.Pair;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.*;

public class PriceCalculatorTest {

    private HashMap<Integer, Integer> ticketsSelectedMap = new HashMap<>();
    private PriceCalculator priceCalculator = null;
    private ArrayList<Pair<Integer, Tarif>> nbByTarifList;

    @Before
    public void setUp() {
        this.ticketsSelectedMap.put(4, 2);
        this.ticketsSelectedMap.put(9, 0);
        this.ticketsSelectedMap.put(11, 3);
        this.ticketsSelectedMap.put(13, 0);

        try {
            this.nbByTarifList = PriceCalculator.convertTicketListToPricesList(this.ticketsSelectedMap);
            this.priceCalculator = new PriceCalculator(this.nbByTarifList);
        } catch (SQLException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void shouldConvertMapToList() {
        this.nbByTarifList = null;

        try {
            this.nbByTarifList = PriceCalculator.convertTicketListToPricesList(this.ticketsSelectedMap);
            assertEquals(2, this.nbByTarifList.size());
            assertEquals(2, this.nbByTarifList.get(0).getKey().intValue());
            assertEquals(3, this.nbByTarifList.get(1).getKey().intValue());

            TarifDAO dao = new TarifDAO();
            Tarif tarif1 = dao.findById(4);
            assertNotNull(tarif1);
            assertEquals(4, this.nbByTarifList.get(0).getValue().getId());
            assertEquals(tarif1.getName(), this.nbByTarifList.get(0).getValue().getName());
            assertEquals(tarif1.getPrice(), this.nbByTarifList.get(0).getValue().getPrice(), 2);
            assertEquals(5, this.nbByTarifList.get(0).getValue().getCinema().getId());

            Tarif tarif2 = dao.findById(11);
            assertNotNull(tarif2);
            assertEquals(11, this.nbByTarifList.get(1).getValue().getId());
            assertEquals(tarif2.getName(), this.nbByTarifList.get(1).getValue().getName());
            assertEquals(tarif2.getPrice(), this.nbByTarifList.get(1).getValue().getPrice(), 2);
            assertEquals(5, this.nbByTarifList.get(1).getValue().getCinema().getId());
        } catch (SQLException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void shouldCalculateTotalPrice() {
        assertEquals(41, this.priceCalculator.calculateTotalPrice(), 2);
    }

    @Test
    public void shouldCalculateNbPlacesTotal() {
        assertEquals(5, this.priceCalculator.calculateTotalTickets());
    }

}