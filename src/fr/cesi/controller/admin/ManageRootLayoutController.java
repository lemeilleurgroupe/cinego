package fr.cesi.controller.admin;

import fr.cesi.CineGo;
import fr.cesi.model.Admin;
import fr.cesi.state.AdminState;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 * Root Layout Controller
 * @author Viko
 *
 */
public class ManageRootLayoutController {
	
	private CineGo mainApp = null;
	private Admin admin;
	private String title;

    @FXML
    private Label lblAdminName;

    @FXML
    private Button btnDeconnexion;

    @FXML
    private Label lblTitle;

    @FXML
    private Button btnBack;
    
    /**
     * Initialization of the window
     */
    @FXML
    private void initialize() {      	
    	this.admin = AdminState.getState().getConnectedUser();
    	this.title = HomePageController.getManageChoosen();
    	if (admin != null)
    	    this.lblAdminName.setText(admin.getPrenom() + " " + admin.getNom());
    	this.lblTitle.setText(this.title);
    }
    
    /**
     * Occurs when the user clicks on "retour"
     * Go to the previous page (Homepage)
     */
    @FXML
    void backBtnClick() {
    	mainApp.initRootLayout();
    	mainApp.showHomePage();
    }
    
    
    /**
     * Occurs when the user clicks on "deconnexion"
     * Go to the home page with no admin connected
     */
    @FXML
    void deconnexionBtnClick() {
        AdminState.getState().setConnectedUser(null);
    	mainApp.initRootLayout();
    	mainApp.showUserLogin();
    }
    
    /**
     * set the main app for this window
     * @param cineGo
     */
    public void setMainAppAdmin(CineGo cineGo) {
		this.mainApp = cineGo;
		
	}


}
