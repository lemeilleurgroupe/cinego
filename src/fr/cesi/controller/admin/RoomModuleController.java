package fr.cesi.controller.admin;
import fr.cesi.module.moduleplansalle.application.PlanOverviewController;
import fr.cesi.module.moduleplansalle.xmlGestion.*;

import java.io.IOException;
import java.sql.SQLException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import fr.cesi.CineGo;
import fr.cesi.util.AlertUtil;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;


public class RoomModuleController {
    private CineGo mainAppAdmin;
    private PlanOverviewController planOverviewControleur;

    public void setPlanOverviewControleur(PlanOverviewController planOverviewControleur) {
		this.planOverviewControleur = planOverviewControleur;
	}

	@FXML
    private TextField roomNameField;
	
	
    public void setMainAppAdmin(CineGo mainAppAdmin) {
        this.mainAppAdmin = mainAppAdmin;
    }

    @FXML
    protected void btnRetourClick() {
        HomePageController.setManageChoosen("Gestion des salles");
        this.mainAppAdmin.showManageRootLayout();
        this.mainAppAdmin.showManageRoom();
    }
    
    /**
     * Description
     *
     * @tag1
     * @tag2
     */
    @FXML
    protected void btnImportClick() throws Exception
    {
    	this.planOverviewControleur.handleImportXML();
    }

    
    /**
     * Description
     *
     * @tag1
     * @tag2
     */
    @FXML
    protected void btnSaveClick() {
        if (roomNameField.getText().equals("")) {
            AlertUtil.sendAlert(Alert.AlertType.ERROR, "Erreur", "Vous n'avez pas donné de nom a votre salle");
            return;
        }
        System.out.println("OK");
        //Add
        try {
			this.planOverviewControleur.handleGenerateXML();
		} catch (IOException | ParserConfigurationException | SAXException | SQLException | TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    
    
    
}
