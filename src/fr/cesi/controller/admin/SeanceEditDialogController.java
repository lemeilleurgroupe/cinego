package fr.cesi.controller.admin;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Time;
import java.time.LocalDate;

import fr.cesi.CineGo;
import fr.cesi.model.Movie;
import fr.cesi.model.Room;
import fr.cesi.model.Seance;
import fr.cesi.model.SeanceCustom;
import fr.cesi.model.DAO.MovieDAO;
import fr.cesi.model.DAO.RoomDAO;
import fr.cesi.model.DAO.SeanceDAO;
import fr.cesi.util.AlertUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Seance edit dialog controller
 * @author Viko
 *
 */

public class SeanceEditDialogController {

	/**
	 * Application principale
	 */
	private CineGo mainApp = null;

	/**
	 * Date de la séance
	 */
	@FXML
	private DatePicker dtpDateModif;

	/**
	 * ComboBox des films
	 */
	@FXML
	private ComboBox<Movie> cboFilmModif;

	/**
	 * Langue du film
	 */
	@FXML
	private TextField txtLangueModif;

	@FXML
	private ComboBox<String> cboHeure;

	/**
	 * ComboBox des salles
	 */
	@FXML
	private ComboBox<Room> cboSalleModif;

	/**
	 * Bouton confirmation de modification
	 */
	@FXML
	private Button btnConfirm;

	/**
	 * Bouton annulation de modification
	 */
	@FXML
	private Button btnCancel;

	/**
	 * Fenêtre de dialogue
	 */
	private Stage dialogStage;

	/**
	 * Séance sélectionnée dans la tableView
	 */
	private SeanceCustom seanceCustom;

	/**
	 * True si l'admin confirme la modification / False si il annule
	 */
	private boolean okClicked = false;



	/**
	 * Initialise les composants de la vue
	 */
	@FXML
	private void initialize() {

		bindCboFilmModif();
		bindCboSalleModif();
		fillCboHeure();
	}

	/**
	 * Renseigne l'application principale de la vue
	 * @param cineGo application principale
	 */
	public void setMainApp(CineGo cineGo) {
		this.mainApp = cineGo;

	}

	/**
	 * Remplit les champs avec les données de la séance sélectionée
	 * @param seance
	 * @throws SQLException
	 */
	public void setSeance(SeanceCustom seanceCustom) throws SQLException {

		this.seanceCustom = seanceCustom;

		// Récupère la langue du film
		txtLangueModif.setText(seanceCustom.getLanguage());

		//Récupère l'heure de la séance
		String timeseance = seanceCustom.getTime().toString();
		String heureetminute[] = timeseance.split(":");
		String time = heureetminute[0] + ":" +  heureetminute[1];	

		cboHeure.setValue(time);

		// Récupère la date de la séance
		LocalDate date = seanceCustom.getDate().toLocalDate();
		dtpDateModif.setValue(date);

		// Récupère le film associé à la séance
		MovieDAO daomovie = new MovieDAO();
		Movie movieseance = new Movie();
		movieseance = daomovie.findById(seanceCustom.getMovieId());

		cboFilmModif.setValue(movieseance);

		// Récupère la salle associée à la séance
		RoomDAO daoroom = new RoomDAO();
		Room roomseance = new Room();
		roomseance = daoroom.findById(seanceCustom.getRoomId());

		cboSalleModif.setValue(roomseance);

	}

	/**
	 * Sets the stage of this dialog.
	 *
	 * @param dialogStage
	 */
	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}


	/**
	 * Reviens à la gestion des séances sans modifications
	 * @param event
	 */
	@FXML
	void cancelModification(ActionEvent event) {
		dialogStage.close();
	}

	/**
	 * Se déclenche quand l'utilisateur clique sur le bouton confirmation
	 * @param event
	 * @throws SQLException 
	 */
	@FXML
	void confirmModification(ActionEvent event) throws SQLException {
		
		try {

		String msgError  = "";
		// Vérifie que les données soient bonnes
		msgError = this.CheckData();

		// Si le message d'erreur est rempli
		if (msgError != "") {
			// Lève une erreur à l'admin
			AlertUtil.sendAlert(
					Alert.AlertType.WARNING,
					this.mainApp.getPrimaryStage(),
					"Données manquantes",
					msgError);
		} else {

				// Confirmation message
				if (AlertUtil.confirmationMessage(this.mainApp.getPrimaryStage(), "Êtes vous sûr de vouloir modifier cette séance ?")){
					modificationSeance();					
				}
			}
		
		} catch (Exception e) {
				e.printStackTrace();
		}
	}
	
	
	/**
	 * Modifie une seance deja existante
	 * @throws SQLException
	 */
	private void modificationSeance() throws SQLException {
		try {		
		
		//Get the seance associated to the seance_id of the customSeance
		SeanceDAO daoseance = new SeanceDAO();
		Seance seance = new Seance();
		seance = daoseance.findById(this.seanceCustom.getId());

		seance.setDate(Date.valueOf(dtpDateModif.getValue()));
		seance.setTime(Time.valueOf(cboHeure.getSelectionModel().getSelectedItem() + ":00"));
		seance.setLanguage(txtLangueModif.getText());
		seance.setMovieid(cboFilmModif.getSelectionModel().getSelectedItem().getId());
		seance.setRoomid(cboSalleModif.getSelectionModel().getSelectedItem().getId());

		// Update la séance
		daoseance.update(seance);

		okClicked = true;

		dialogStage.close();
		
		} catch (Exception e) {
			AlertUtil.sendAlert(Alert.AlertType.INFORMATION,this.mainApp.getPrimaryStage(),"Doublon séance","Une séance existe déjà dans cette salle à cette horaire");
			e.printStackTrace();
		}
	}
	

	/**
	 * Vérifie que toutes les données ont bien été rentrées
	 * @return le message d'erreur associée à la vérification
	 */
	public String CheckData() {
		String msgerror = "";
		// Données obligatoires
		if (dtpDateModif.getValue() == null) { msgerror += "\n - Date"; }
		if (cboHeure.getSelectionModel().getSelectedItem() == null) { msgerror += "\n - Heure"; }
		if (txtLangueModif.getText() == null || txtLangueModif.getText().trim().isEmpty()) { msgerror += "\n - Langue"; }
		if (cboFilmModif.getSelectionModel().getSelectedItem() == null) { msgerror += "\n - Film"; }
		if (cboSalleModif.getSelectionModel().getSelectedItem() == null) { msgerror += "\n - Salle"; }

		// On formate le message
		if (msgerror != "" ){ msgerror = "Certaines données sont manquantes : " + msgerror; }
		if (msgerror != "") { return msgerror;}

		// On retourne le message
		return msgerror;
	}

	

	/**
	 * Remplit le comboBox du choix du film
	 */
	public void bindCboFilmModif() {

		// Crée la liste des films du cinéma
		ObservableList<Movie> moviesList;
		MovieDAO dao = new MovieDAO();
		try {
			// Récuperer les films déjà en base 
			moviesList = FXCollections.observableArrayList(dao.listAll());
		} catch (SQLException e) {
			// Si erreur 
			AlertUtil.sendAlert(
					Alert.AlertType.ERROR,
					this.mainApp.getPrimaryStage(),
					"Erreur SQL",
					"Nous n'avons pas pu récupérer la liste des films."
					);
			return;
		}

		// Sinon on remplit le comboBox des films avec la liste
		this.cboFilmModif.setItems(moviesList);

	}

	/**
	 * Remplit le comboBox du choix de la Salle
	 */
	public void bindCboSalleModif() {
		// Crée la liste des salles du cinéma
		ObservableList<Room> roomList;
		RoomDAO dao = new RoomDAO();
		try {
			// Récupére les salles déjà en base 
			roomList = FXCollections.observableArrayList(dao.listAll());

		} catch (SQLException e) {
			// Si erreur
			AlertUtil.sendAlert(
					Alert.AlertType.ERROR,
					this.mainApp.getPrimaryStage(),
					"Erreur SQL",
					"Nous n'avons pas pu récupérer la liste des salles."
					);
			return;
		}

		// Sinon on remplit le comboBox avec la liste des salles
		this.cboSalleModif.setItems(roomList);

	}

	/**
	 *      
	 * @return true si l'admin a confirmé la modification, false sinon
	 */
	public boolean isOkClicked() {
		return okClicked;
	}
	
	public void fillCboHeure() {
		
		ObservableList<String> heureList= FXCollections.observableArrayList();
		heureList.add("10:00");
		heureList.add("12:00");
		heureList.add("14:00");
		heureList.add("16:00");
		heureList.add("18:00");
		heureList.add("20:00");
		heureList.add("22:00");
		this.cboHeure.setItems(heureList);
		
	}



}
