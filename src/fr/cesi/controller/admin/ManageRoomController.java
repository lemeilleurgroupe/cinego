package fr.cesi.controller.admin;

import fr.cesi.CineGo;
import fr.cesi.model.DAO.RoomDAO;
import fr.cesi.model.Room;
import fr.cesi.state.AdminState;
import fr.cesi.util.AlertUtil;
import fr.cesi.module.moduleplansalle.application.PlanOverviewController;
import fr.cesi.module.moduleplansalle.contentStatic.Utils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import fr.cesi.config.Database;

import java.io.IOException;
import java.sql.SQLException;





public class ManageRoomController {

    CineGo mainAppAdmin = null;

    private PlanOverviewController planOverviewControleur;

    public void setPlanOverviewControleur(PlanOverviewController planOverviewControleur) {
		this.planOverviewControleur = planOverviewControleur;
	}
    
    @FXML
    private TableView<Room> roomTable;

    @FXML
    private TableColumn<String, Room> roomNumberColumn;

    @FXML
    private TableColumn<String, Room> nbPlacesColumn;

    @FXML
    private TableColumn<String, Room> planColumn;

    
    @FXML
    void initialize() {
        // Get cinema's room
        RoomDAO roomDao = new RoomDAO();
        try {
            ObservableList<Room> roomsList = FXCollections.observableArrayList(roomDao.listAllByCinema(AdminState.getState().getCinema()));

            this.roomTable.setItems(roomsList);

            this.roomNumberColumn.setCellValueFactory(new PropertyValueFactory<>("num"));
            this.nbPlacesColumn.setCellValueFactory(new PropertyValueFactory<>("nbPlaces"));
        } catch (SQLException e) {
            AlertUtil.sendAlert(
                    Alert.AlertType.ERROR,
                    this.mainAppAdmin.getPrimaryStage(),
                    "Erreur SQL",
                    "Nous n'avons pas pu récupérer la liste des salles."
            );
            return;
        }

    }

    @FXML
    void newRoomAction() {
        this.mainAppAdmin.initRootLayout();
        this.mainAppAdmin.showCreationRoomPage();
    }
    
    /**
     * Description
     *
     * @tag1
     * @tag2
     */
    @FXML
    protected void btnExportClick() throws SQLException, IOException
    {	    
    	Room r = this.roomTable.getSelectionModel().getSelectedItem();
    	System.out.println(r.getNum());
    	Utils.CreateXMLAndDownload(r.getId(), Database.getInstance());
    }
    
    /**
     * Description
     *
     * @tag1
     * @tag2
     */
    @FXML
    protected void btnImportClick()
    {
    	this.mainAppAdmin.initRootLayout();
        this.mainAppAdmin.showCreationRoomPage();
    }
    
    
    public void setMainAppAdmin(CineGo mainApp) {
        this.mainAppAdmin = mainApp;
    }

}
