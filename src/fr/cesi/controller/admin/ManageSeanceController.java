package fr.cesi.controller.admin;


import java.sql.Date;
import java.sql.SQLException;
import java.sql.Time;

import fr.cesi.CineGo;
import fr.cesi.model.Movie;
import fr.cesi.model.Room;
import fr.cesi.model.Seance;
import fr.cesi.model.SeanceCustom;
import fr.cesi.model.DAO.MovieDAO;
import fr.cesi.model.DAO.RoomDAO;
import fr.cesi.model.DAO.SeanceDAO;
import fr.cesi.state.AdminState;
import fr.cesi.util.AlertUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * Seance controller
 * @author Viko
 *
 */
public class ManageSeanceController {

	/**
	 * Main application
	 */
	private CineGo mainApp = null;

	/**
	 * TableView of seances
	 */
	@FXML
	private TableView<SeanceCustom> tbvSeance;

	/**
	 * Column of the room
	 */
	@FXML
	private TableColumn<Seance, String> tvcSalle;

	/**
	 * Column of the date
	 */
	@FXML
	private TableColumn<Seance, String> tvcDate;

	/**
	 * Column of the time
	 */
	@FXML
	private TableColumn<Seance, String> tvcHeure;

	/**
	 * Column of the movie
	 */
	@FXML
	private TableColumn<Seance, String> tvcFilm;

	/**
	 * Column of the language
	 */
	@FXML
	private TableColumn<Seance, String> tvcLangue;

	/**
	 * Button of the seance deletion
	 */
	@FXML
	private Button btnDelete;

	/**
	 * ComboBox of the room's choice
	 */
	@FXML
	private ComboBox<Room> cboSelectRoom;

	/**
	 * Combobox of the movie's choice
	 */
	@FXML
	private ComboBox<Movie> cboSelectFilm;

	/**
	 * DatePicker of the date's choice
	 */
	@FXML
	private DatePicker dateSelect;

	/**
	 * Bouton for add a seance
	 */
	@FXML
	private Button btnAdd;

	/**
	 * Bouton for modify a seance
	 */
	@FXML
	private Button btnModify;

	/**
	 * Combobox for time's choice
	 */
	@FXML
	private ComboBox<String> cboHeure;
	
	/**
	 * TextField of the language
	 */
	@FXML
	private TextField txtLangue;

	/**
	 * Initialization of the window
	 */
	@FXML
	private void initialize() {
		// Combobox movie
		this.bindCboSelectFilm();
		// Combobox room
		this.bindCboSelectSalle();
		// tableview seance
		this.bindTbvSeance();
		// combobox time
		this.fillCboHeure();

	}


	/**
	 * set the main app for this view
	 * @param cineGo main application
	 */
	public void setMainAppAdmin(CineGo cineGo) {
		this.mainApp = cineGo;

	}

	/**
	 * Add a new seance to the cinema
	 */
	@FXML
	public void addNewSeance() {

		String msgError  = "";

		// Check the data
		msgError = this.CheckData();

		// If there is an error
		if (msgError != "") {			
			AlertUtil.sendAlert(
					Alert.AlertType.WARNING,
					this.mainApp.getPrimaryStage(),
					"Données manquantes",
					msgError);
		} else  {
				// If no error
				// Confirmation message
				if (AlertUtil.confirmationMessage(this.mainApp.getPrimaryStage(), "Êtes vous sûr de vouloir créer cette séance ?")){

					// Create a new seance with the datas
					Seance seance = new Seance();
					seance.setDate(Date.valueOf(dateSelect.getValue()));
					seance.setTime(Time.valueOf(cboHeure.getSelectionModel().getSelectedItem() + ":00"));
					seance.setLanguage(txtLangue.getText());
					seance.setMovieid(cboSelectFilm.getSelectionModel().getSelectedItem().getId());
					seance.setRoomid(cboSelectRoom.getSelectionModel().getSelectedItem().getId());

					SeanceDAO seanceDAO = new SeanceDAO();
					try {
						// create the seance in base
						seanceDAO.create(seance);
						seanceDAO.upadateInitNbPlaces(seance);
						AlertUtil.sendAlert(
								Alert.AlertType.INFORMATION,
								this.mainApp.getPrimaryStage(),
								"Séance ajouté",
								"Votre séance a bien été ajoutée");
						// Update the tableview
						this.bindTbvSeance();			
					} catch (SQLException e) {
						AlertUtil.sendAlert(Alert.AlertType.INFORMATION,this.mainApp.getPrimaryStage(),"Doublon séance","Une séance existe déjà dans cette salle à cette horaire");
						e.printStackTrace();
					}

				}
			}
		}
	

	/**
	 * Delete the selected seance
	 */
	@FXML
	public void deleteSeance() {
		// Get the selected seance
		SeanceCustom seance = this.tbvSeance.getSelectionModel().getSelectedItem();
		// Error : no seance selected
		if (seance == null) {
			AlertUtil.sendAlert(
					Alert.AlertType.WARNING,
					this.mainApp.getPrimaryStage(),
					"Pas de séance sélectionnée",
					"Vous devez sélectionner une séance pour la supprimer"
					);
			return;
		}

		// Confirmation message
		if (AlertUtil.confirmationMessage(this.mainApp.getPrimaryStage(), "Êtes vous sûr de vouloir supprimer cette séance ?")){
			SeanceDAO dao = new SeanceDAO();
			try {
				// Check if there is reservation for the seance
				if (dao.existingReservation(seance)) {
					AlertUtil.sendAlert(
							Alert.AlertType.ERROR,
							this.mainApp.getPrimaryStage(),
							"Reservations existantes",
							"Suppression impossible : Des réservations existent pour cette séance"
							);
				} else {
				// Delete the seance 
				dao.deleteCustom(seance);
				AlertUtil.sendAlert(
						Alert.AlertType.CONFIRMATION,
						this.mainApp.getPrimaryStage(),
						null,
						"La séance a bien été supprimé"
						);
				}
			} catch (SQLException e) {
				e.printStackTrace();
				AlertUtil.sendAlert(
						Alert.AlertType.ERROR,
						this.mainApp.getPrimaryStage(),
						"Erreur",
						"Il y a eu un problème lors de l'envois de la requête. Veuillez rééssayer"
						);
			}
		}
		
		// Update the table view
		this.bindTbvSeance();
	}


	/**
	 * Open a dialog window for modify a seance
	 * @throws SQLException
	 */
	@FXML
	public void SeanceModificationClick() throws SQLException {

		// Get the selected seance
		SeanceCustom seance = this.tbvSeance.getSelectionModel().getSelectedItem();
		// Check if the selected seance is not null
		if (seance != null) {
			// Get the result from the dialogwindow
			boolean okClicked = mainApp.showSeanceEditDialog(seance);
			// If modification, refresh the tableview
			if (okClicked) {
				bindTbvSeance();
			}
			// else error
		} else {
			// Error : no selected seance
			if (seance == null) {
				AlertUtil.sendAlert(
						Alert.AlertType.WARNING,
						this.mainApp.getPrimaryStage(),
						"Pas de séance sélectionnée",
						"Vous devez sélectionner une séance pour la modifier"
						);
			}
		}
	}

	/**
	 * Check all the datas
	 * @return error message
	 */
	public String CheckData() {
		String msgerror = "";		

		// Mandatories values
		if (dateSelect.getValue() == null) { msgerror += "\n - Date"; }
		if (cboHeure.getSelectionModel().getSelectedItem() == null) { msgerror += "\n - Heure"; }
		if (txtLangue.getText() == null || txtLangue.getText().trim().isEmpty()) { msgerror += "\n - Langue"; }
		if (cboSelectFilm.getSelectionModel().getSelectedItem() == null) { msgerror += "\n - Film"; }
		if (cboSelectRoom.getSelectionModel().getSelectedItem() == null) { msgerror += "\n - Salle"; }

		// Create the message
		if (msgerror != "" ){ msgerror = "Certaines données sont manquantes : " + msgerror; }
		if (msgerror != "") { return msgerror;}


		// Return it
		return msgerror;
	}

	

	/**
	 * Fill the combobox movie
	 */
	public void bindCboSelectFilm() {

		// Create the movie list
		ObservableList<Movie> moviesList;
		MovieDAO dao = new MovieDAO();
		try {
			// Get the movies
			moviesList = FXCollections.observableArrayList(dao.listAll());
		} catch (SQLException e) {
			// error 
			AlertUtil.sendAlert(
					Alert.AlertType.ERROR,
					this.mainApp.getPrimaryStage(),
					"Erreur SQL",
					"Nous n'avons pas pu récupérer la liste des films."
					);
			return;
		}

		// fill the combobox movie with the list
		this.cboSelectFilm.setItems(moviesList);

	}

	/**
	 * Fill the combobox room
	 */
	public void bindCboSelectSalle() {
		// Create the list of the rooms
		ObservableList<Room> roomList;
		RoomDAO dao = new RoomDAO();
		try {
			// Get all the room
			roomList = FXCollections.observableArrayList(dao.listAllByCine(AdminState.getState().getCinema()));

		} catch (SQLException e) {
			// error
			AlertUtil.sendAlert(
					Alert.AlertType.ERROR,
					this.mainApp.getPrimaryStage(),
					"Erreur SQL",
					"Nous n'avons pas pu récupérer la liste des salles."
					);
			return;
		}

		// fill the combobox room with the list
		this.cboSelectRoom.setItems(roomList);

	}
	
	/**
	 * Fill the combobox time
	 */
	public void fillCboHeure() {
	
		ObservableList<String> heureList= FXCollections.observableArrayList();
		heureList.add("10:00");
		heureList.add("12:00");
		heureList.add("14:00");
		heureList.add("16:00");
		heureList.add("18:00");
		heureList.add("20:00");
		heureList.add("22:00");
		this.cboHeure.setItems(heureList);
		
	}

	/**
	 * Bind the tableview seance
	 */
	public void bindTbvSeance() {
		// Create the cinema list
		ObservableList<SeanceCustom> seanceList;
		SeanceDAO seancedao = new SeanceDAO();
		try {
			//Get all the seance
			seanceList = FXCollections.observableArrayList(seancedao.listSeanceCustom());
		} catch (SQLException e) {
			// errr
			AlertUtil.sendAlert(
					Alert.AlertType.ERROR,
					"Erreur SQL",
					"Nous n'avons pas pu récupérer la liste des séances."
					);
			return;
		}
		// Fill the tableview
		this.tbvSeance.setItems(seanceList);      

		// Bind the column
		this.tvcSalle.setCellValueFactory(new PropertyValueFactory<>("numRoom"));
		this.tvcDate.setCellValueFactory(new PropertyValueFactory<>("date"));
		this.tvcHeure.setCellValueFactory(new PropertyValueFactory<>("time"));
		this.tvcFilm.setCellValueFactory(new PropertyValueFactory<>("movieName"));
		this.tvcLangue.setCellValueFactory(new PropertyValueFactory<>("language"));


	}

}
