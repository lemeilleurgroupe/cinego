
package fr.cesi.controller.admin;

import java.io.File;
import java.sql.SQLException;

import fr.cesi.CineGo;

import fr.cesi.model.Admin;
import fr.cesi.model.DAO.AdminDAO;
import fr.cesi.state.AdminState;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

/**
 * Admin login controller
 * @author Viko
 *
 */
public class AdminLoginController {

	private CineGo mainApp = null;
	
    @FXML
    private TextField txtLogin;
    @FXML
    private PasswordField txtMdp;    
    @FXML
    private Label lblResult;
    @FXML
    private ImageView imgHome;

   
    /**
     * Occurs when the user clicks on "Connexion"
     * @param event
     */
    @FXML
    void btnConnexion_Click(MouseEvent event) {
    	String login = txtLogin.getText();
		String password = txtMdp.getText();

		try{
            AdminDAO dao = new AdminDAO();
            Admin admin = dao.findByLoginAndPassword(login, password);
			if(admin != null){
				lblResult.setTextFill(Color.web("#99FF33"));
				lblResult.setText("Connexion réussie");
                AdminState.getState().setConnectedUser(admin);
                if (this.mainApp != null) {
                    this.mainApp.showHomePage();
                }
			}
			else {
				lblResult.setTextFill(Color.web("#FF0000"));
				lblResult.setText("Login ou mot de passe incorrect");
			}
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}

    }

    /**
     * Occurs when the user clicks on "Quitter"
     * @param event
     */
    @FXML
    void btnQuitter_Click(MouseEvent event) {
    	System.exit(0);
    }


    /**
     * Set the main application for this window
     * @param mainApp
     */
    public void setMainAppAdmin(CineGo mainApp) {
        this.mainApp = mainApp;
    }
}

