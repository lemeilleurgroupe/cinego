package fr.cesi.controller.admin;

import java.sql.SQLException;

import fr.cesi.CineGo;
import fr.cesi.model.Cinema;
import fr.cesi.model.Tarif;
import fr.cesi.model.DAO.CinemaDAO;
import fr.cesi.model.DAO.TarifDAO;

import fr.cesi.model.Tarif;
import fr.cesi.state.AdminState;
import fr.cesi.util.AlertUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.FloatStringConverter;

public class ManageCinemaController {

	@FXML
	private TableView<Tarif> tarifTable;

	@FXML
	private Button deleteTarif;

	@FXML
	private TextField tarifName;

	@FXML
	private TextField tarifValue;

	@FXML
	private TextField nbChiffres;

	@FXML
	private TableColumn<Tarif, String> tarifNameColumn;

	@FXML
	private TableColumn<Tarif, Float> tarifPriceColumn;

	private CineGo mainAppAdmin;

	// Executed after setMainApp
	void initialize() {

		// Récuperer le nombre actuellement enregistré
		this.nbChiffres.setText("" + AdminState.getState().getCinema().getNbFidelity());

		// Récupérer les tarifs
		this.loadTarifs();
	}

	/**
	 * Affiche les tarifs du cinema dans le tableau
	 */
	void loadTarifs() {
		TarifDAO dao = new TarifDAO();
		try {
			ObservableList<Tarif> tarifs = FXCollections.observableArrayList(dao.listAll(AdminState.getState().getCinema()));
			// Afficher les tarifs dans le tableau
			this.tarifTable.setItems(tarifs);
			this.tarifNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
			this.tarifPriceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));

			this.tarifTable.setEditable(true);
			this.tarifNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
			this.tarifPriceColumn.setCellFactory(TextFieldTableCell.forTableColumn(new FloatStringConverter()));

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Change le nom du tarif
	 * @param editingCell
	 */
	public void changeNameTarif(CellEditEvent editingCell) {

		try {
			Tarif tarif = this.tarifTable.getSelectionModel().getSelectedItem();
			tarif.setName(editingCell.getNewValue().toString());
			TarifDAO dao = new TarifDAO();
			dao.update(tarif);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Change le prix du tarif
	 * @param editingCell
	 */
	public void changePriceTarif(CellEditEvent editingCell) {		
		try {
			Tarif tarif = this.tarifTable.getSelectionModel().getSelectedItem();    
			double tarifValue = Double.parseDouble(editingCell.getNewValue().toString());
			tarif.setPrice((float)tarifValue);
			TarifDAO dao = new TarifDAO();
			dao.update(tarif);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Ajoute un nouveau tarif dans le tableau.
	 * @param event
	 * @throws SQLException
	 */
	@FXML
	void addTarif(ActionEvent event) throws SQLException {
		if (this.isNumber(this.tarifValue.getText(), true)) {
			double tarifValue = Double.parseDouble(this.tarifValue.getText());
			String tarifName = this.tarifName.getText();

			// Save in to database
			Tarif tarif = new Tarif();
			tarif.setName(tarifName);
			tarif.setPrice((float)tarifValue);
			tarif.setCinema(AdminState.getState().getCinema());
			TarifDAO dao = new TarifDAO();
			// Afficher les tarifs dans le tableau
			ObservableList<Tarif> tarifs = this.tarifTable.getItems();
			tarifs.add(tarif);
			this.tarifTable.setItems(tarifs);
			this.tarifNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
			this.tarifPriceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));
			try {
				dao.create(tarif);
				this.loadTarifs();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

	}

	/**
	 * Supprime le tarif sélectionné
	 */
	@FXML
	void deleteTarif() {
		Tarif tarif = this.tarifTable.getSelectionModel().getSelectedItem();
		if (tarif == null) {
			AlertUtil.sendAlert(
					Alert.AlertType.WARNING,
					"",
					"Veuillez sélectionner un tarif"
					);
			return;
		}
		if (!AlertUtil.confirmationMessage("Êtes-vous sur de vouloir supprimer ce tarif ?")) {
			return;
		}
		TarifDAO dao = new TarifDAO();
		try {
			dao.delete(tarif);
			this.loadTarifs();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sauvegarde du nombre de chiffres dans une carte de fidélité
	 * @param event
	 */
	@FXML
	void saveNbFidelity(ActionEvent event) {
		if (this.isNumber(this.nbChiffres.getText(), false)) {
			Cinema cinema = AdminState.getState().getCinema();
			cinema.setNbFidelity(Integer.parseInt(this.nbChiffres.getText()));
			CinemaDAO dao = new CinemaDAO();
			try {
				dao.update(cinema);
				AlertUtil.sendAlert(
						Alert.AlertType.INFORMATION,
						"Succès",
						"Le nombre de chiffre de la carte de fidélité a été mis à jour"
						);
			} catch (SQLException e) {
				AlertUtil.sendAlert(
						Alert.AlertType.ERROR,
						"Erreur de base de données",
						"L'enregistrement dans la base de donnée n'a pas réussi");
			}
		}
	}

	/**
	 * Vérifie que la valeur entrée est bien un chiffre
	 * @param input
	 * @return
	 */
	private boolean isNumber(String input, boolean isDouble) {
		try {
			if (!isDouble)
				Integer.parseInt(input);
			else
				Double.parseDouble(input);
			return true;
		} catch (Exception e) {
			AlertUtil.sendAlert(
					Alert.AlertType.ERROR,
					"Erreur",
					"Ce champ ne peut contenir que des chiffres"
					);
			return false;
		}
	}

	public void setMainAppAdmin(CineGo mainAppAdmin) {
		this.mainAppAdmin = mainAppAdmin;
		this.initialize();
	}
}
