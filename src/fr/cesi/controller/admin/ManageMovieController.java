package fr.cesi.controller.admin;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Date;
import java.sql.ParameterMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.cesi.util.ParamUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import fr.cesi.CineGo;
import fr.cesi.model.Movie;
import fr.cesi.model.DAO.MovieDAO;
import fr.cesi.util.AlertUtil;
import fr.cesi.util.WebServiceUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

public class ManageMovieController {

    // Table
    @FXML
    private TableView<Movie> movieTable;
    @FXML
    private TableColumn<Movie, String> movieTitleColumn;
    @FXML
    private TableColumn<Movie, String> movieReleaseDateColumn;
    @FXML
    private TableColumn<Movie, String> movieDirectorsColumn;
    @FXML
    private TableColumn<Movie, String> movieGenresColumn;
    @FXML
    private TableColumn<Movie, String> movieRuntimeColumn;

    @FXML
    private ListView<Movie> moviesList;

    @FXML
    private TextField searchField;

    private CineGo mainAppAdmin = null;

    private ObservableList<Movie> movieTableList;

    private static final String DIRECTOR_JOB_LABEL = "Director";

    @FXML
    private void initialize() {
    	
        // Récuperer les films déjà en base dans un tableau
        ObservableList<Movie> moviesList;
        MovieDAO dao = new MovieDAO();
        try {
            moviesList = FXCollections.observableArrayList(dao.listAll());
        } catch (SQLException e) {
            AlertUtil.sendAlert(
                    Alert.AlertType.ERROR,
                    this.mainAppAdmin.getPrimaryStage(),
                    "Erreur SQL",
                    "Nous n'avons pas pu récupérer la liste des films."
                    );
            return;
        }
        this.movieTableList = moviesList;
        this.movieTable.setItems(this.movieTableList);

        this.movieTitleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        this.movieReleaseDateColumn.setCellValueFactory(new PropertyValueFactory<>("releaseDate"));
        this.movieDirectorsColumn.setCellValueFactory(new PropertyValueFactory<>("director"));
        this.movieGenresColumn.setCellValueFactory(new PropertyValueFactory<>("genre"));
        this.movieRuntimeColumn.setCellValueFactory(new PropertyValueFactory<>("runtime"));

    }


    /**
     * Launch search request when user click on click button
     */
    @FXML
    public void searchButtonClick() {
        // Récupérer la liste des films
        URL url = null;
        String moviesString = "";
        try {
            String search = URLEncoder.encode(this.searchField.getText(), "UTF-8");
            url = new URL("http://api.themoviedb.org/3/search/movie?api_key=df28aa6d697eda1d264d38dbf58dcec0&language=fr&query=" + search);
            URLConnection conn = url.openConnection();
            InputStreamReader bufferedReader = new InputStreamReader(conn.getInputStream());

            int cp;
            StringBuilder json = new StringBuilder();
            while ((cp = bufferedReader.read()) != -1) {
                json.append((char) cp);
            }
            moviesString = json.toString();
            bufferedReader.close();

            JSONObject moviesJson = new JSONObject(moviesString);
            JSONArray moviesArray = moviesJson.getJSONArray("results");

            List<Movie> moviesArrayList = new ArrayList<>();
            for (int i = 0; i < moviesArray.length(); i++) {
                Movie movieObject = new Movie();
                JSONObject movie = moviesArray.getJSONObject(i);
                if (!movie.getString("release_date").equals("")) {
                    movieObject.setId(movie.getInt("id"));
                    movieObject.setTitle(movie.getString("title"));
                    movieObject.setReleaseDate(Date.valueOf(movie.getString("release_date")));
                    moviesArrayList.add(movieObject);
                }
            }
            this.moviesList.setItems(FXCollections.observableArrayList(moviesArrayList));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * User clicks on add movie button
     */
    // TODO: Refactoring
    @FXML
    public void addMovieButtonClick() {
        // Get selected movie
        Movie movie = this.moviesList.getSelectionModel().getSelectedItem();
        // Erreur : Pas de films renseignés
        if (movie == null) {
            AlertUtil.sendAlert(
                    Alert.AlertType.WARNING,
                    this.mainAppAdmin.getPrimaryStage(),
                    "Pas de film sélectionné",
                    "Vous devez sélectionner un film pour l'ajouter"
            );
            return;
        }

        // Erreur : ID non renseigné
        if (movie.getId() < 0) {
            AlertUtil.sendAlert(
                    Alert.AlertType.WARNING,
                    this.mainAppAdmin.getPrimaryStage(),
                    "Erreur de selection",
                    "le film sélectionné n'a pas d'id"
                    );
            return;
        }

        // get all info from movie
        try {
            String params = "?api_key="+ParamUtil.getParamUtil().getParam("TMDb.apikey")+"&language=" + ParamUtil.getParamUtil().getParam("TMDb.language");
            JSONObject movieJSON = WebServiceUtil.getJson(ParamUtil.getParamUtil().getParam("TMDb.url") + "movie/"+ movie.getId() + params);
            JSONObject movieCreditsJSON = WebServiceUtil.getJson(ParamUtil.getParamUtil().getParam("TMDb.url") + "movie/"+ movie.getId() +"/credits" + params);
            // fill movie object
            movie.setReleaseDate(Date.valueOf(movieJSON.getString("release_date")));
            movie.setAffiche(movieJSON.getString("poster_path"));
            movie.setOverview(movieJSON.getString("overview"));
            if (movieJSON.get("runtime") instanceof Integer)
                movie.setRuntime(movieJSON.getInt("runtime"));

            // Create string of all directors
            movie.setDirector(this.createDirectorsString(movieCreditsJSON));
            // Create string of all genres
            movie.setGenre(this.createGenresString(movieJSON));
        } catch (IOException e) {
            AlertUtil.sendAlert(
                    Alert.AlertType.ERROR,
                    "Erreur : problème de connection au webservice",
                    e.getMessage()
                    );
            return;
        } catch (JSONException e) {
            AlertUtil.sendAlert(
                    Alert.AlertType.ERROR,
                    "Erreur : problème lors de la récupération des données",
                    e.getMessage()
            );
            return;
        }

        try {
            MovieDAO dao = new MovieDAO();
            dao.create(movie);
            this.movieTableList.add(movie);
        }
        catch (MySQLIntegrityConstraintViolationException e) {
            AlertUtil.sendAlert(
                    Alert.AlertType.ERROR,
                    "Erreur lors de l'importation du film",
                    "Le film a déjà été importé"
            );
            return;
        }
        catch (SQLException e) {
            AlertUtil.sendAlert(
                    Alert.AlertType.ERROR,
                    "Erreur lors de l'importation du film",
                    e.getMessage()
            );
            return;
        }

        AlertUtil.sendAlert(
                Alert.AlertType.INFORMATION,
                "Film ajouté",
                "Votre film a bien été ajouté");
    }

    /**
     * User click on delete movie button
     */
    @FXML
    public void deleteMovieButtonClick() {
        // Get selected movie
        Movie movie = this.movieTable.getSelectionModel().getSelectedItem();
        // Erreur : Pas de films renseignés
        if (movie == null) {
            AlertUtil.sendAlert(
                    Alert.AlertType.WARNING,
                    this.mainAppAdmin.getPrimaryStage(),
                    "Pas de film sélectionné",
                    "Vous devez sélectionner un film pour l'ajouter"
            );
            return;
        }

        // Confirmation message
        if (AlertUtil.confirmationMessage("Êtes vous sûr de vouloir supprimer ce film ?")){
            MovieDAO dao = new MovieDAO();
            try {
                dao.delete(movie);
                this.movieTable.getItems().remove(movie);
            } catch (SQLException e) {
                AlertUtil.sendAlert(
                        Alert.AlertType.ERROR,
                        this.mainAppAdmin.getPrimaryStage(),
                        "Erreur",
                        "Il y a eu un problème lors de l'envois de la requête. Veuillez rééssayer"
                );
            }
        }
        AlertUtil.sendAlert(
                Alert.AlertType.CONFIRMATION,
                //this.mainAppAdmin.getPrimaryStage(),
                null,
                "Le film a bien été supprimé"
                );

        // Update table
        this.initialize();
    }

    public void setMainAppAdmin(CineGo mainAppAdmin) {
        this.mainAppAdmin = mainAppAdmin;
    }


    /**
     * Create a formated list of all directors of a movie
     * @param movieCreditsJSON
     * @return
     */
    private String createDirectorsString(JSONObject movieCreditsJSON) {
        List<String> directorsList = new ArrayList<>();

        // Créé une liste des JSONs des membres du crew
        JSONArray crewArray = movieCreditsJSON.getJSONArray("crew");

        for (int i = 0; i < crewArray.length(); i++) {
            JSONObject crewMember = crewArray.getJSONObject(i);
            // Est-ce que c'est un director?
            if (crewMember.getString("job").equals(DIRECTOR_JOB_LABEL)) {
                directorsList.add(crewMember.getString("name"));
            }
        }
        return String.join(", ", directorsList);
    }

    /**
     * Create a formated list of all directors of a movie
     * @param movieJSON
     * @return
     */
    private String createGenresString(JSONObject movieJSON) {
        List<String> genresStringList = new ArrayList<>();
        JSONArray genresArray = movieJSON.getJSONArray("genres");

        for (int i = 0; i < genresArray.length(); i++) {
            JSONObject genre = genresArray.getJSONObject(i);
            genresStringList.add(genre.getString("name"));
        }
        return String.join(", ", genresStringList);
    }
}
