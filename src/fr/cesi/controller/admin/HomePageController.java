package fr.cesi.controller.admin;

import fr.cesi.CineGo;
import fr.cesi.model.Admin;
import fr.cesi.state.AdminState;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 * Home page controller
 * @author Viko
 *
 */
public class HomePageController {
	
	private CineGo mainApp = null;
	private Admin admin;
	private static String manageChoosen;

    @FXML
    private Button btnDeconnexion;

    @FXML
    private Button btnGSalle;

    @FXML
    private Button btnGTarifs;

    @FXML
    private Button btnGSeance;

    @FXML
    private Button btnGFilm;
    
    @FXML
    private Label lblAdminName;



    /**
     * Initialization of the window
     */
    @FXML
    private void initialize() {
    	this.admin = AdminState.getState().getConnectedUser();
    	this.lblAdminName.setText(admin.getPrenom() + " " + admin.getNom());
    }


    /**
     * Get the selected management of the user
     * @return
     */
    public static String getManageChoosen() {
		return manageChoosen;
	}


    /**
     * Set the selected management of the user
     * @param manageChoosen
     */
	public static void setManageChoosen(String manageChoosen) {
		HomePageController.manageChoosen = manageChoosen;
	}


	/**
	 * Occurs when the user clicks on "Gestion des films"
	 * Open the managemovie window
	 */
	@FXML
    void manageMovieBtnClick() {
    	if (this.mainApp != null) {
            setManageChoosen("Gestion de films");
            this.mainApp.showManageRootLayout();
            this.mainApp.showManageMovie();
        }
    }

	/**
	 * Occurs when the user clicks on "Gestion des séances"
	 * Open the ManageSeance window
	 */
    @FXML
    void manageSeanceBtnClick() {

        if (this.mainApp != null) {
            setManageChoosen("Gestion des Séances");
            this.mainApp.showManageRootLayout();
            this.mainApp.showManageSeance();
        }
    }
    
	/**
	 * Occurs when the user clicks on "Gestion du cinéma"
	 * Open the ManagePrice window
	 */
    @FXML
    void managePriceBtnClick() {

        if (this.mainApp != null) {
            setManageChoosen("Gestion du cinéma");
            this.mainApp.showManageRootLayout();
            this.mainApp.showManagePrice();
        }
    }
    
    /**
	 * Occurs when the user clicks on "Gestion des salles"
	 * Open the ManageRoom window
	 */
    @FXML
    void manageRoomBtnClick() {

        if (this.mainApp != null) {
            setManageChoosen("Gestion des salles");
            this.mainApp.showManageRootLayout();
            this.mainApp.showManageRoom();
        }
    }
    

    /**
     * Occurs when the user clicks on "Deconnexion"
     * Go the home page, no admin connected
     * @param event
     */
    @FXML
    void deconnexionBtnClick(ActionEvent event) {
    	mainApp.initRootLayout();
    	mainApp.showUserLogin();

    }

    /**
     * set the main app for this window
     * @param cineGo
     */
	public void setMainAppAdmin(CineGo cineGo) {
		this.mainApp = cineGo;
		
	}

}
