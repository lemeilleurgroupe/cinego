package fr.cesi.controller.user;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;

import fr.cesi.model.Client;
import fr.cesi.model.DAO.MovieDAO;
import fr.cesi.model.DAO.TarifDAO;
import fr.cesi.CineGoClient;
import fr.cesi.model.Movie;
import fr.cesi.model.Tarif;
import fr.cesi.service.PriceCalculator;
import fr.cesi.state.ClientState;
import fr.cesi.util.AlertUtil;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;

public class RecapCommandeController extends Controller {
    @FXML
    private Label lblHeure;

    @FXML
    private Button btnAbandon;

    @FXML
    private Button btnContinuer;

    @FXML
    private Button btnRetour;

    @FXML
    private FlowPane flpPlaces;

    @FXML
    private Label lblFilm;

    @FXML
    private Label lblAdresseCine;

    @FXML
    private Label lblNomCine;

    @FXML
    private Label lblHeureSeance;

    @FXML
    private Label lblDateSeance;

    @FXML
    private ImageView imgAffiche;

    @FXML
    private GridPane tarifGrid;

    @FXML
    private Label tarifCount;

    @FXML
    private Label tarifTotal;

    /**
     * Initialization of the window
     */
    @FXML
    private void initialize() {

        // Show the reservations for the user
        this.showReservationInfos();
    }

    /**
     * Show reservation information to the user
     */
    private void showReservationInfos() {
        // Change labels
        ClientState state = ClientState.getState();
        // Cinema infos
        this.lblNomCine.setText(state.getSelectedCinema().getName());
        this.lblAdresseCine.setText(state.getSelectedCinema().getCity());
        // Seance info
        this.lblDateSeance.setText(state.getSelectedSeance().getDate().toString());
        this.lblHeureSeance.setText(state.getSelectedSeance().getTime().toString());

        // Movie info
        MovieDAO movieDao = new MovieDAO();
        try {
            Movie movie = movieDao.findById(state.getSelectedSeance().getMovieid());
            this.lblFilm.setText(movie.getTitle());
            this.imgAffiche.setImage(new Image(URL_IMAGE + movie.getAffiche()));
        } catch (SQLException e) {
            AlertUtil.sendAlert(Alert.AlertType.ERROR, "Séance invalide", "La séance n'est pas associée a un film");
            this.btnAbandonClick();
        }

        // Tarif info
        int nbLignes = 1; // Lignes dans le gridpane
        for (Pair<Integer, Tarif> nbPlaceTarif:
                ClientState.getState().getBillets()) {
            Tarif tarif = nbPlaceTarif.getValue();
            Label tarifName = new Label(tarif.getName());
            Label tarifNb = new Label("" + nbPlaceTarif.getKey());
            Label tarifPrice = new Label("" + tarif.getPrice());
            this.tarifGrid.add(tarifName, 0, nbLignes);
            this.tarifGrid.add(tarifNb, 1, nbLignes);
            this.tarifGrid.add(tarifPrice, 2, nbLignes);
            nbLignes++;
        }
        PriceCalculator pCalc = new PriceCalculator(ClientState.getState().getBillets());
        this.tarifCount.setText(String.valueOf(pCalc.calculateTotalTickets()));
        this.tarifTotal.setText(String.valueOf(pCalc.calculateTotalPrice()));
    }

    /**
     * Demande à l'utilisateur s'il veut se connecter ou entrer sa carte de fidélité, puis va a la page suivante
     * @param event
     */
    @FXML
    void btnPaiementClick(MouseEvent event) {
        boolean isChanged = false;
        // Ouvre une fenetre qui demande s'il veut se connecter
        if (ClientState.getState().getConnectedUser() == null) {
            isChanged = this.alertLoginOrRegister();
        }
        if (isChanged) return;
        if (ClientState.getState().getConnectedUser() == null) {
            // Ouvre une fenetre qui demande si on a la carte de fidélité
            this.askFidelityCard();
        }
        this.mainApp.initRootLayout();
        this.mainApp.showPaiement();
    }

    public void btnRetourClick() {
    	CineGoClient.setcurrentPage(4);
		this.mainApp.showManageRootLayout();
        this.mainApp.showChoixPlaces();
    }

    /**
     * Open dialog pane asking if user wants to connect or register an ancount
     * @return true if page change, false if cancel clicked
     */
    private boolean alertLoginOrRegister() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Que voulez vous faire ?");
        alert.setHeaderText("Voulez-vous vous connecter ou créer un compte ?");

        ButtonType loginBtn = new ButtonType("Se connecter");
        ButtonType registerBtn = new ButtonType("Créer un compte");
        ButtonType cancelBtn = new ButtonType("Continuer", ButtonBar.ButtonData.CANCEL_CLOSE);

        alert.getButtonTypes().setAll(loginBtn, registerBtn, cancelBtn);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == loginBtn){ // ... user chose "Se connecter"
            CineGoClient.setcurrentPage(5);
            this.mainApp.initRootLayout();
            this.mainApp.showConnexionClient();
            return true;
        } else if (result.get() == registerBtn) { // ... user chose "Créer un compte"
            CineGoClient.setcurrentPage(5);
            this.mainApp.initRootLayout();
            this.mainApp.showCreationCompteClient();
            return true;
        }
        return false;
    }

    /**
     * Open dialog pane asking card number
     */
    private void askFidelityCard() {
        // Create the custom dialog.
        Dialog<String> dialog = new Dialog<>();
        dialog.setTitle("Carte de fidélité");
        dialog.setHeaderText("Avez-vous une carte de fidélité ?");
        dialog.setContentText("Si oui, entrez son numero.");

        // Set the button types.
        ButtonType validateBtnType = new ButtonType("Oui", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancelBtn = new ButtonType("Non", ButtonBar.ButtonData.CANCEL_CLOSE);
        dialog.getDialogPane().getButtonTypes().addAll(validateBtnType, cancelBtn);

        // Create the fidelity card field.
        AnchorPane pane = new AnchorPane();
        TextField fidelityNumberField = new TextField();
        fidelityNumberField.setPromptText("FidelityCard");

        // Enable/Disable button depending on whether a number was entered.
        Node validateBtn = dialog.getDialogPane().lookupButton(validateBtnType);
        validateBtn.setDisable(true);

        // Do some validation.
        fidelityNumberField.textProperty().addListener((observable, oldValue, newValue) -> {
            validateBtn.setDisable(newValue.trim().isEmpty());
        });

        dialog.getDialogPane().setContent(fidelityNumberField);

        // Request focus on the field by default.
        Platform.runLater(() -> fidelityNumberField.requestFocus());

        // Convert the result to a username-password-pair when the login button is clicked.
        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == validateBtnType) {
                return fidelityNumberField.getText();
            }
            return null;
        });

        Optional<String> result = dialog.showAndWait();

        result.ifPresent(resultNumber -> {
            ClientState.getState().setFidelityCard(resultNumber);
        });
    }
}