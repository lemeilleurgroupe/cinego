package fr.cesi.controller.user;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Calendar;

import com.jfoenix.controls.JFXButton;
import fr.cesi.CineGoClient;
import fr.cesi.model.Movie;
import fr.cesi.model.Room;
import fr.cesi.model.Seance;
import fr.cesi.model.DAO.RoomDAO;
import fr.cesi.model.DAO.SeanceDAO;
import fr.cesi.state.ClientState;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;

public class ChoixSeanceController {

	@FXML
	private VBox vboxDate;

	@FXML
	private ScrollPane scrollDate;

	@FXML
	private FlowPane flowPanelHeure;

	@FXML
	private Button btnRetour;

	@FXML
	private Button btnContinuer;

	@FXML
	private Label lblHeure;

	@FXML
	private ImageView imgFilm;

	@FXML
	private Label lblHeureSelect;

	@FXML
	private Label lblDateSelect;

	@FXML
	private Label lblNomFilm;

	private Movie film;  


	private CineGoClient mainApp = null;

	public static Seance seanceSelected = null;

	private String urlImage = "https://image.tmdb.org/t/p/original";

	@FXML
	private void initialize() {
		try {		
			// Get the selected movie
			this.film = HomePageController.film;
			// Display the seance according to the movie
			displaySeance();
			Image img = new Image(urlImage + this.film.getAffiche()); 
			imgFilm.setImage(img);
			lblNomFilm.setText(this.film.getTitle());
			btnContinuer.setVisible(false);
		} catch (SQLException e1) {
			e1.getMessage();
			e1.printStackTrace();
		}		
	}

	/**
	 * Affiche la séance d'un film
	 * @throws SQLException
	 */
	private void displaySeance() throws SQLException {
		// Clear all the values
		this.vboxDate.getChildren().clear();

		SeanceDAO daos = new SeanceDAO();
		// Create a button for each differente date
		for (Date d : daos.listDateByMovie(this.film)) {
			DateFormat dfm = new SimpleDateFormat("EEEE d MMMM");	
			JFXButton button = new JFXButton(dfm.format(d));
			button.setPrefWidth(200);
			button.setPrefHeight(75);
			button.setMinHeight(75);
			button.setMaxHeight(75);
			scrollDate.setFitToWidth(true);	
			scrollDate.setStyle("-fx-background-color:transparent;");
			// When the user clicks on the date button
			button.setOnAction(new EventHandler<ActionEvent>() { 
				public void handle(ActionEvent e) {
					lblHeureSelect.setText("");
					seanceSelected = null;
					btnContinuer.setVisible(false);
					lblDateSelect.setText(button.getText());
					// Clear all the values
					flowPanelHeure.getChildren().clear(); 
					try {			
						// Get the seance associated to the movie, date, cine
						for (Seance s : daos.listAllByMovieDate(HomePageController.film, d) ){								
							LocalTime t2 = LocalTime.parse(s.getTime().toString());						
							LocalDate d2 = LocalDate.parse(d.toString());
							if (LocalTime.now().isAfter(t2) && LocalDate.now().isEqual(d2)) {
								//do nothing
							} else { 
								// Get the room associated to the seance
								Room r = new Room();
								RoomDAO daor = new RoomDAO();
								r = daor.findById(s.getRoomid());
								// Create a button for each time
								JFXButton b1 = new JFXButton();
								// Display the time of the seance, the room number and the seat left
								SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
								b1.setText(sdf.format(s.getTime()) + "\n" + "Salle " + r.getNum() + "\n" + "Places restantes : " + daos.NbPlacesRestantes(s) + "\n" + s.getLanguage());
								b1.setPrefWidth(200);
								b1.setAlignment(Pos.BASELINE_LEFT);
								// When the users click on the button
								b1.setOnAction(new EventHandler<ActionEvent>() {
									public void handle(ActionEvent e) {
										lblHeureSelect.setText(s.getTime().toString());
										// Get the seaceSelected
										seanceSelected = s;
										// Enable the continue button
										btnContinuer.setVisible(true);
									} });
								// Add the created button
								flowPanelHeure.getChildren().add(b1); 
							}
						}	
					} catch (SQLException e1) {					
						e1.printStackTrace();
					}
				}
			});
			// Add the created button 
			this.vboxDate.getChildren().add(button);
		}		
	}


	public void setMainAppAdmin(CineGoClient cineGoC) {
		this.mainApp = cineGoC;

	}	

	/**
	 * L'utilisateur clique sur "Continuer"
	 * Ajoute le film au state et va a la page suivante
	 * @param event
	 */
	@FXML
	void btnContinuerClick(MouseEvent event) {
	    if (seanceSelected != null) {
            ClientState.getState().setSelectedSeance(seanceSelected);
        }
		if (this.mainApp != null) {
			CineGoClient.setcurrentPage(3);
			this.mainApp.showManageRootLayout();
			this.mainApp.showChoixBillets();
		}
	}

	/**
	 * L'utilisateur clique sur "Continuer"
	 * Va a la page précédente
	 * @param event
	 */
	@FXML
	void btnRetourClick(MouseEvent event) {
		if (this.mainApp != null) {
			// Reset the seance
			seanceSelected = null;  
			CineGoClient.setcurrentPage(1);
			this.mainApp.showManageRootLayout();
			this.mainApp.showHomePage();
		}
	}


}
