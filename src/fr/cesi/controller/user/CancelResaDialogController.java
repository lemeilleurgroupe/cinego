package fr.cesi.controller.user;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import fr.cesi.CineGo;
import fr.cesi.CineGoClient;
import fr.cesi.model.Reservation;
import fr.cesi.model.Seance;
import fr.cesi.model.DAO.ReservationDAO;
import fr.cesi.model.DAO.SeanceDAO;
import fr.cesi.util.AlertUtil;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class CancelResaDialogController {

	/**
	 * Main application
	 */
	private CineGoClient mainApp = null;

	/**
	 * Fenètre de Dialog
	 */
	private Stage dialogStage;

	@FXML
	private Button btnAnnuler;

	@FXML
	private TextField txtNum;

	@FXML
	private Button btnRetour;

	@FXML
	private void initialize() {
		this.txtNum.setText("");
	}

	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	public void setMainApp(CineGoClient cineGoClient) {
		this.mainApp = cineGoClient;

	}

	/**
	 * Annule une réservation grace a son numero
	 */
	@FXML
	void btnAnnulerClick(MouseEvent event) throws SQLException {
		// Traitement
		// Lire le textbox du numéro de réservation
		String numResa = this.txtNum.getText();
		if (numResa.equals("")) {
			AlertUtil.sendAlert(
					Alert.AlertType.WARNING,
					this.mainApp.getPrimaryStage(),
					"Champ manquant",
					"Veuillez vérifier le numéro de votre réservation"
					);
		} else 
		{		
		Reservation resa = null;
		// Regardez si il existe une résa pour ce numéro
		ReservationDAO resaDAO = new ReservationDAO();
		SeanceDAO seanceDAO = new SeanceDAO();
		resa = resaDAO.findByNum(numResa);
		// Si non message d'erreur
		if (resa.getId() < 0) {
			AlertUtil.sendAlert(
					Alert.AlertType.WARNING,
					this.mainApp.getPrimaryStage(),
					"Pas de réservation existante pour ce numéro",
					"Veuillez vérifier le numéro de votre réservation"
					);
		} else {

			// Si oui demande de confirmation
			if (AlertUtil.confirmationMessage(this.mainApp.getPrimaryStage(), "Êtes vous sûr de vouloir annuler cette réservation?")) {
				// Vérifier si la séance est dans 24h 
				Seance seance  = seanceDAO.findById(resa.getSeanceId());
				if (seance.getId() < 0) {
					AlertUtil.sendAlert(Alert.AlertType.ERROR, "Opération impossible", "Impossible de trouver la séance associé a cette réservation");
					return;
				}
				LocalDateTime seanceDateTime = LocalDateTime.parse(seance.getDate().toString()+"T"+seance.getTime().toString());
				LocalDateTime now = LocalDateTime.now();
				LocalDateTime plus24 = now.plusDays(1);
				// Si confirmation ok : suppression de la réservation. 
				if (seanceDateTime.isAfter(plus24)) {
					resaDAO.deleteReservationPlace(resa.getId());
					resaDAO.delete(resa);
					// Message confirmant la suppression 
					AlertUtil.sendAlert(
							Alert.AlertType.CONFIRMATION,
							this.mainApp.getPrimaryStage(),
							null,
							"La réservation a bien été annulé"
							);	
					dialogStage.close();
				} else {
					AlertUtil.sendAlert(
							Alert.AlertType.WARNING,
							this.mainApp.getPrimaryStage(),
							"Erreur",
							"La séance est dans moins de 24h, la réservation ne peut pas être annulée"
							);
				}					
				return;
			} else {
				return;
			}
		}
		}
	}	


	/**
	 * Aller à la page précédente
	 * @param event
	 */
	@FXML
	void btnRetourClick(MouseEvent event) {
		dialogStage.close();
	}
}
