package fr.cesi.controller.user;

import fr.cesi.CineGoClient;
import fr.cesi.state.ClientState;
import javafx.fxml.FXML;
import javafx.scene.input.MouseEvent;

import java.util.HashMap;

/**
 * Classe parent représenatant un controlleur "Client". Il doit avoir un bouton retour et abandon
 */
public abstract class Controller {

    protected CineGoClient mainApp;

    protected String URL_IMAGE = "https://image.tmdb.org/t/p/original";

    /**
     * Bouton retour
     */
    @FXML
    protected abstract void btnRetourClick();

    /**
     * User clicks on "Abandon" button : reset state and go to homepage
     * L'utilisateur clique que le bouton "Abandonner" : le state se remet a zero et on va sur la homepage
     */
    @FXML
    public void btnAbandonClick() {
        ClientState.getState().resetReservation();
        CineGoClient.setcurrentPage(1);
        this.mainApp.showManageRootLayout();
        this.mainApp.showHomePage();
    }

    public void setMainAppAdmin(CineGoClient cineGoC) {
        this.mainApp = cineGoC;
    }

}
