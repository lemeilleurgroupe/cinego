package fr.cesi.controller.user;

import java.sql.SQLException;

import fr.cesi.CineGoClient;
import fr.cesi.model.Client;
import fr.cesi.model.DAO.ClientDAO;
import fr.cesi.state.ClientState;
import fr.cesi.util.AlertUtil;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;

public class ConnexionClientController {

    @FXML
    private TextField txtMail;

    @FXML
    private PasswordField txtMdp;

    @FXML
    private Button btnConnexion;

    @FXML
    private Button btnRetour;

    @FXML
    private Label lblResult;
    
    private CineGoClient mainApp = null;	    
    
    @FXML
	private void initialize() {
		
	}

    /**
     * Vérifie les identifiants de l'utilisateur et l'ajoute dans le state s'il est correct
     */
	public void loginUser() {
        // Récupérer les champs
        String email = this.txtMail.getText();
        String password = this.txtMdp.getText();

        // Essayer de récupérer l'utilisateur
        ClientDAO dao = new ClientDAO();
        Client client = null;
        try {
            client = dao.findByEmailAndPassword(email, password);
            if (client == null) {
                this.lblResult.setText("Email ou mot de passe incorrect");
                return;
            }
            // Changer le message
            this.lblResult.setText("success");
            // On ajoute dans le State quel utilisateur est connecté
            ClientState.getState().setConnectedUser(client);
            // Retour à la page précédente
            this.mainApp.goToPreviousPage();
        } catch (SQLException e) {
            this.lblResult.setText("Erreur SQL : " + e.getMessage());
        }
    }


    /**
     * set the main application for this view
     * @param cineGoC
     */
	public void setMainAppAdmin(CineGoClient cineGoC) {
		this.mainApp = cineGoC;
	}

    /**
     * Occurs when the user clicks on "Retour"
     * Go to the previous page
     */
    @FXML
    void btnRetourClick() {
        if (this.mainApp != null) {
            // Reset the seance
        	this.mainApp.showManageRootLayout();
            this.mainApp.goToPreviousPage();
        }
    }

}
