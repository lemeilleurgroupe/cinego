package fr.cesi.controller.user;

import fr.cesi.CineGoClient;
import fr.cesi.module.moduleplansalleuser.application.PlanOverviewController;
import fr.cesi.module.moduleplansalleuser.component.Place;
import fr.cesi.module.moduleplansalleuser.contentStatic.Utils;
import fr.cesi.service.PriceCalculator;
import fr.cesi.state.ClientState;
import fr.cesi.util.AlertUtil;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;

import java.sql.SQLException;
import java.util.ArrayList;

public class ChoixPlacesController extends Controller {

	private PlanOverviewController gridController;

	@FXML
	private void initialize() {


	}

	@Override
	@FXML
	protected void btnRetourClick() {
		CineGoClient.setcurrentPage(3);
		this.mainApp.showManageRootLayout();
		this.mainApp.showChoixBillets();
	}

	/**
	 * L'utilisateur clique sur le bouton "Continuer" : vérifie que suffisement de place ont été sélectionnées
	 * @param event
	 */
	@FXML
	void btnContinuerClick(MouseEvent event) {
	    PriceCalculator priceCalculator = new PriceCalculator(ClientState.getState().getBillets());
	    int nbPlaces = priceCalculator.calculateTotalTickets();
        ArrayList<Place> places = this.gridController.getSitToBeConvertInReserve();
        // Not enougth places
        if (places.size() < nbPlaces) {
            int diffPlace = nbPlaces - places.size();
            AlertUtil.sendAlert(Alert.AlertType.ERROR, "Erreur", "Il vous reste à choisir encore " + diffPlace + " place(s).");
            return;
        }
        // Too many places
        if (places.size() > nbPlaces) {
            int diffPlace = places.size() - nbPlaces;
            AlertUtil.sendAlert(Alert.AlertType.ERROR, "Erreur", "Vous avez réservé " + diffPlace + " places en trop.");
            return;
        }
        ClientState.getState().setSelectedPlaces(places);
		if (this.mainApp != null) {
			CineGoClient.setcurrentPage(5);
			this.mainApp.showManageRootLayout();
			this.mainApp.showRecapCommande();
		}
	}

	/**
	 * Récupère le controlleur du module
	 * @param gridController
	 */
	public void setGridController(PlanOverviewController gridController) {
		this.gridController = gridController;
	}
}
