package fr.cesi.controller.user;


import fr.cesi.CineGoClient;
import fr.cesi.state.ClientState;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

public class UserRootLayoutController {

	private CineGoClient mainApp = null;

	@FXML
	private Label txtNomClient;

	@FXML
	private Button btnCreationCompte;

	@FXML
	private Button btnConnexion;

	@FXML
	private Button btnMonCompte;

	@FXML
	private Button btnDeconnexion;	

    @FXML
    private Label lblTitre;

	private boolean isConnected = false;


	@FXML
	private void initialize() {
		displayTitle();
		displayUser();
	}


	/**
	 * Affiche les boutons de connexion, ou l'utilisateur en cours
	 */
	private void displayUser() {
		// Hide the buttons if the user is not connected
		if ( ClientState.getState().getConnectedUser() != null) {
			this.isConnected = true;
			this.txtNomClient.setText(ClientState.getState().getConnectedUser().getLastname() + " " + ClientState.getState().getConnectedUser().getSurname());

		}else {
			this.isConnected = false;
			this.txtNomClient.setText("");
		}
		this.btnMonCompte.setVisible(isConnected);
		this.btnDeconnexion.setVisible(isConnected);
	}

	/**
	 * Affiche le titre de la fenêtre en cours
	 */
	private void displayTitle() {
		switch (CineGoClient.getcurrentPage()) {
		// Home page
		case 1:
			lblTitre.setText("Films à l'affiche");
			break;
			// Seance
		case 2:
			lblTitre.setText("Sélectionner votre séance");
			break;
			//Choix billets
		case 3:
			lblTitre.setText("Sélectionner vos billets");
			break;
			// Choix places
		case 4:
			lblTitre.setText("Sélectionner vos places");
			break;
			// Recap
		case 5:
			lblTitre.setText("Votre commande");
			break;
		default:
			break;
		}
	}
	/**
	 * set the main app for this window
	 * @param cineGo
	 */
	public void setMainAppAdmin(CineGoClient cineGo) {
		this.mainApp = cineGo;

	}


	/**
	 * Occurs when the user clicks on "Connexion" button
	 * Go the connexion page for the user
	 * @param event
	 */
	@FXML
	void btnConnexionClick(MouseEvent event) {
		if (this.mainApp != null) {   
			this.mainApp.initRootLayout();
			this.mainApp.showConnexionClient();
		}
	}

	/**
	 * Occurs when the user clicks on "Créer un compte" Button
	 * Go the account creation page fo the user
	 * @param event
	 */
	@FXML
	void btnCreationClick(MouseEvent event) {
		if (this.mainApp != null) {
			this.mainApp.initRootLayout();
			this.mainApp.showCreationCompteClient();
		}
	}

	/**
	 * Occurs when the user clicks on "Deconnexion" Button
	 * Deconnecte the user
	 * @param event
	 */
	@FXML
	void btnDeconnexionClick(MouseEvent event) {
		if (ClientState.getState().getConnectedUser() !=null) {
			ClientState.getState().setConnectedUser(null);
			this.isConnected = false;
			this.btnMonCompte.setVisible(isConnected);
			this.btnDeconnexion.setVisible(isConnected);
			this.txtNomClient.setText("");
		}
	}

	/**
	 * Occurs when the user clicks on "Mon compte" Button
	 * Go the user's account's page
	 * @param event
	 */
	@FXML
	void btnMonCompteClick(MouseEvent event) {
		if (this.mainApp != null) {
			this.mainApp.initRootLayout();
			this.mainApp.showCompteClient();
		}
	}

}
