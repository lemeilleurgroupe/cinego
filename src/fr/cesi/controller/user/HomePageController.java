package fr.cesi.controller.user;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import fr.cesi.CineGoClient;
import fr.cesi.model.Cinema;
import fr.cesi.model.Movie;
import fr.cesi.model.DAO.CinemaDAO;
import fr.cesi.model.DAO.MovieDAO;
import fr.cesi.state.ClientState;
import fr.cesi.util.AlertUtil;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;

public class HomePageController {


	@FXML
	private GridPane gridFilmDetails;

	@FXML
	private GridPane gridSynopsis;

	@FXML
	private ScrollPane scrollSynopsis;

	@FXML
	private Label lblReal;

	@FXML
	private Label lblGenre;

	@FXML
	private Label lblDuree;

	@FXML
	private Label lblDateSortie;

	@FXML
	private Label lblSynopsis;

	@FXML 
	private Label lblTitreFilm;

	@FXML
	private ComboBox<Cinema> cboChoixCine;

	@FXML
	private FlowPane listFilms;

	@FXML
	private GridPane gridClient;

	@FXML
	private Label txtNomClient;

	@FXML
	private Button btnCreationCompte;

	@FXML
	private Button btnConnexion;

	@FXML
	private Button btnMonCompte;

	@FXML
	private Button btnDeconnexion;

	@FXML
	private Button btnReserver;

	@FXML
	private Button btnAnnuler;



	private CineGoClient mainApp = null;	

	private boolean isConnected = false; 

	public static Cinema cine = null;

	public static Movie film = null;


	private String urlImage = "https://image.tmdb.org/t/p/original";


	/**
	 * Initiliaze the window
	 * @throws SQLException
	 */
	@FXML
	private void initialize() throws SQLException {
		try {		
			displayHomePage();
			// Display the movies from the cine choosen
			displayFilms();	
			// manage the cinema selected 
			manageCinema();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Set a new property to the combobox, add an event when the user select a cinema
	 */
	public void manageCinema() {
		// Occurs when the users changed the value of the comboBox
		cboChoixCine.valueProperty().addListener(new ChangeListener<Cinema>() {
			@Override
			public void changed(ObservableValue<? extends Cinema> observable, Cinema oldValue, Cinema newValue) {
				// Get the new cine choosen
				HomePageController.cine = newValue;
				try {
					// Display the movies from this cine
					displayFilms();
				} catch (SQLException e) {						
					e.printStackTrace();
				}
			} 
		});	
	}

	/**
	 * Display the home page content depending of the connected user, and the selected cinema
	 */
	public void displayHomePage() {
		// Hide the films details
		gridFilmDetails.setVisible(false);
		
		this.film = null;
		// Bind the comboBox with list of cinemas (by default cineid = 5 (gaumont))
		this.bindCboSelectCinema();
		// Select the first cine of the list
		cboChoixCine.getSelectionModel().selectFirst();
		this.cine = cboChoixCine.getSelectionModel().getSelectedItem();
	}

	/**
	 * set the main application for this view
	 * @param cineGoC
	 */
	public void setMainAppAdmin(CineGoClient cineGoC) {
		this.mainApp = cineGoC;

	}

	/**
	 * Occurs when the user clicks on "Réserver places" button
	 * Go the the next page (ChoixSeance)
	 * @param event
	 */
	@FXML
	void btnReserverClick(MouseEvent event) {
		if (this.mainApp != null ){
			// If the user did not select a movie then error
			if (this.film != null) {	
				CineGoClient.setcurrentPage(2);
				this.mainApp.showManageRootLayout();
				this.mainApp.showChoixSeance();
				// Save selected cinema into state
				ClientState.getState().setSelectedCinema(this.cboChoixCine.getSelectionModel().getSelectedItem());
			} else {
				AlertUtil.sendAlert(
						Alert.AlertType.WARNING,
						this.mainApp.getPrimaryStage(),
						"Pas de film sélectionné",
						"Veuillez sélectionner un film"
						);
			}

		}
	}


	/**
	 * Open the window dialog for cancel a reservation by number
	 * @param event
	 */
	@FXML
	void btnAnnulerClick(MouseEvent event) {
		if (this.mainApp != null ){
			mainApp.showCancelResaDialog();
		}
	}

	/**
	 * Display all the movies from the cine with a seance
	 * @throws SQLException
	 */
	public void displayFilms() throws SQLException {  
		// Clear all the values
		this.listFilms.getChildren().clear();
		// Get all the movie by Cine and seance
		MovieDAO dao = new MovieDAO();
		// For each movie
		for (Movie m : dao.listAllByCine(this.cine.getId())) {  
			// Create a new image with the poster 
			ImageView img = new ImageView(urlImage + m.getAffiche());  
			img.setPreserveRatio(true);
			img.setFitHeight(295);
			img.getStyleClass().add("select-movie");
			// Occurs when the users clicks on the movie's poster
			img.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
					// Get the movie
					HomePageController.film = m;
					gridFilmDetails.setVisible(true);
					lblReal.setText(m.getDirector());					
					int heure = (m.getRuntime() / 60);
					int minutes = m.getRuntime() -( 60 * heure);					  			   
					lblDuree.setText(String.valueOf(heure) + "h" + String.valueOf(minutes));				    
					lblGenre.setText(m.getGenre());
					DateFormat dfm = new SimpleDateFormat("dd/MM/yyyy");				
					lblDateSortie.setText(dfm.format(m.getReleaseDate()));
					lblSynopsis.setText(m.getOverview());
					lblSynopsis.setWrapText(true);				
					scrollSynopsis.setFitToWidth(true);				
					lblTitreFilm.setText(m.getTitle());					
					event.consume();
				}
			});
			// Add the movie
			this.listFilms.getChildren().add(img);
		}  
	}
	

	/**
	 * Fill the comboBox with all the cinemas
	 */
	public void bindCboSelectCinema() {

		// Create the list of the cinema 
		ObservableList<Cinema> cineList;
		CinemaDAO dao = new CinemaDAO();
		try {
			// Get all the movies for this cinema
			cineList = FXCollections.observableArrayList(dao.listAll());
		} catch (SQLException e) {
			// If error
			AlertUtil.sendAlert(
					Alert.AlertType.ERROR,
					this.mainApp.getPrimaryStage(),
					"Erreur SQL",
					"Nous n'avons pas pu récupérer la liste des cinémas."
					);
			return;
		}

		// Set the combobox with the cinema list
		this.cboChoixCine.setItems(cineList);

	}



}
