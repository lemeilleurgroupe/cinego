package fr.cesi.controller.user;


import fr.cesi.CineGoClient;
import fr.cesi.controller.user.Controller;
import fr.cesi.exception.UserInputException;
import fr.cesi.model.DAO.ReservationDAO;
import fr.cesi.model.Reservation;
import fr.cesi.service.PriceCalculator;
import fr.cesi.state.ClientState;
import fr.cesi.util.AlertUtil;
import fr.cesi.util.NumberUtils;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

import java.sql.SQLException;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

public class PaiementController extends Controller {


    private static final int CARD_NB_DIGITS = 16;
    private static final int CARD_VERIF_NB_DIGITS = 3;
    private static final int CARD_DATE_NB_DIGIT = 4;
    @FXML
    private TextField cardOwnerField;

    @FXML
    private TextField cardNumberField;

    @FXML
    private TextField cardVerifField;

    @FXML
    private TextField cardMonthField;

    @FXML
    private TextField cardYearField;

    @FXML
    private Label totalPriceLabel;

    @FXML
    private Label totalReductionLabel;

    @FXML
    private Label totalToPayLabel;

    @FXML
    private Button btnAbandon;

    @FXML
    private Button btnRetour;

    private PriceCalculator pCalc = new PriceCalculator(ClientState.getState().getBillets());;

    @FXML
    public void initialize() {
        this.totalPriceLabel.setText(String.valueOf(this.pCalc.calculateTotalPrice()));
        this.totalReductionLabel.setText(String.valueOf(this.pCalc.calculateReduction()));
        this.totalToPayLabel.setText(String.valueOf(this.pCalc.calculateTotalWithReduction()));
    }

    @Override
    protected void btnRetourClick() {
        this.mainApp.initRootLayout();
        this.mainApp.showRecapCommande();
    }

    /**
     * L'uitilisateur clique sur le bouton "Payer"
     * Check les inputs et créé la réservation dans la base de données
     */
    public void btnPaiementClick() {
        try {
            this.isCorrectInputs();
        } catch (UserInputException e) {
            AlertUtil.sendAlert(Alert.AlertType.ERROR, "Carte invalide", e.getMessage());
            return;
        }
        // Créer la réservation
        String numResa = "";
        try {
            Reservation reservation = new Reservation();
            reservation.setSeanceId(ClientState.getState().getSelectedSeance().getId());
            reservation.setDateCreation(new Date());
            reservation.setTotalPrice(this.pCalc.calculateTotalWithReduction());
            if (ClientState.getState().getConnectedUser() != null)
                reservation.setClientId(ClientState.getState().getConnectedUser().getId());
            numResa += ClientState.getState().getSelectedCinema().getName().substring(0, 3).toUpperCase();
            numResa += ClientState.getState().getSelectedSeance().getMovieid();
            Calendar.getInstance().clear();
            numResa += "-" + Calendar.getInstance().getTimeInMillis();
            reservation.setNum(numResa);
            reservation.setPlaces(ClientState.getState().getSelectedPlaces());
            ReservationDAO resaDAO = new ReservationDAO();
            resaDAO.create(reservation);
        } catch (Exception e) {
            e.printStackTrace();
            AlertUtil.sendAlert(
                    Alert.AlertType.ERROR,
                    "Erreur lors de la création de la réservation",
                    "Nous n'avons pas pu créer votre réservation. Veuillez rééssayer.");
            return;
        }
        AlertUtil.sendAlert(Alert.AlertType.INFORMATION,
                "",
                "Votre réservation a bien été enregistrée. Code de réservation : " + numResa);
        this.mainApp.showManageRootLayout();
        if (ClientState.getState().getConnectedUser() != null) {
            CineGoClient.setcurrentPage(6);
            this.mainApp.showCompteClient();
        } else {
            CineGoClient.setcurrentPage(1);
            this.mainApp.showHomePage();
        }
    }

    /**
     * Check if card inputs are correct
     * @throws UserInputException if there is an error into user input
     */
    private void isCorrectInputs() throws UserInputException {
        // Remove spaces between digits (also trim)
        String cardNumber = this.cardNumberField.getText().replace(" ", "");
        if (!NumberUtils.isNumberWithSize(cardNumber, CARD_NB_DIGITS))
            throw new UserInputException("Le numero de la carte n'est pas correct. Veuillez rééssayer.");

        String verifCode = this.cardVerifField.getText().trim();
        if (!NumberUtils.isNumberWithSize(verifCode, CARD_VERIF_NB_DIGITS))
            throw new UserInputException("Le numero de vérification de la carte n'est pas correct. Veuillez rééssayer.");

        // merge date
        String dateExp = this.cardMonthField.getText() + this.cardYearField.getText();
        dateExp = dateExp.replace(" ", "");
        if (!NumberUtils.isNumberWithSize(dateExp, CARD_DATE_NB_DIGIT))
            throw new UserInputException("La date de la carte n'est pas correct. Veuillez rééssayer.");

        if (!isCardStillValid())
            throw new UserInputException("Votre carte a expiré. Veuillez rééssayer.");
    }

    /**
     * Check if cart date is not updated
     * @return false is card date is outdated
     */
    private boolean isCardStillValid() {
        // is date not outdated
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        // get actual century
        calendar.setTime(new Date());
        int century = (calendar.get(Calendar.YEAR) / 100);
        calendar.clear();
        calendar.set(Calendar.MONTH, Integer.parseInt(this.cardMonthField.getText()));
        calendar.set(Calendar.YEAR, Integer.parseInt( century + this.cardYearField.getText()));
        Date dateExpDate = calendar.getTime();
        return dateExpDate.after(new Date());
    }
}
