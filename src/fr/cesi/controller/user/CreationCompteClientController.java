package fr.cesi.controller.user;

import java.sql.Date;
import java.sql.SQLException;

import fr.cesi.util.AlertUtil;
import javafx.scene.control.*;
import org.apache.commons.validator.routines.EmailValidator;

import fr.cesi.CineGoClient;
import fr.cesi.model.Client;
import fr.cesi.model.DAO.ClientDAO;
import javafx.fxml.FXML;

public class CreationCompteClientController {

    @FXML
    private DatePicker txtDateNaissance;

    @FXML
    private TextField txtNom;

    @FXML
    private TextField txtPrenom;

    @FXML
    private TextField txtMail;

    @FXML
    private TextField txtMdp;

    @FXML
    private PasswordField txtMdpConfirm;

    @FXML
    private TextField txtNumCarte;

    @FXML
    private Label errorMessage;


    private CineGoClient mainApp = null;


    @FXML
	private void initialize() {
		
	}

    /**
     * Check inputs and register user into database
     */
	public void registerUser() {
        if (
                this.txtNom.getText().equals("") ||
                this.txtPrenom.getText().equals("") ||
                this.txtDateNaissance.getValue() == null ||
                this.txtDateNaissance.getValue().equals("") ||
                this.txtMail.getText().equals("") ||
                this.txtMdp.getText().equals("") ||
                this.txtMdpConfirm.getText().equals("")
                ) {
            this.errorMessage.setText("Vous devez remplir tous les champs obligatoires");
            return;
        }

        if (!EmailValidator.getInstance().isValid(this.txtMail.getText())) {
            this.errorMessage.setText("Erreur dans l'adresse email");
            return;
        }

        if (!this.txtMdp.getText().equals(this.txtMdpConfirm.getText())) {
            this.errorMessage.setText("Les deux passwords ne correspondent pas");
            return;
        }

        Client client = new Client();
        client.setNumCard(this.txtNumCarte.getText().equals("") ? null : this.txtNumCarte.getText());
        client.setSurname(this.txtPrenom.getText());
        client.setLastname(this.txtNom.getText());
        client.setBirthday(Date.valueOf(this.txtDateNaissance.getValue()));
        client.setMail(this.txtMail.getText());
        client.setPassword(this.txtMdp.getText());
        ClientDAO dao = new ClientDAO();
        try {
            dao.create(client);
            AlertUtil.sendAlert(Alert.AlertType.INFORMATION, "Félicitation", "Votre compte a bien été créé");
            this.mainApp.goToPreviousPage();
        } catch (SQLException e) {
            this.errorMessage.setText("Erreur lors de l'insertion en base de donnée : "  + e.getMessage());
        }
    }

    /**
     * set the main application for this view
     * @param cineGoC
     */
	public void setMainAppAdmin(CineGoClient cineGoC) {
		this.mainApp = cineGoC;
	}

    /**
     * Occurs when the user clicks on "Retour"
     * Go to the previous page
     */
    @FXML
    void btnRetourClick() {
    	 this.mainApp.goToPreviousPage();
    }

}
