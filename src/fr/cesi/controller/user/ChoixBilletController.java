package fr.cesi.controller.user;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.jfoenix.controls.JFXButton;
import fr.cesi.CineGoClient;
import fr.cesi.model.DAO.MovieDAO;
import fr.cesi.model.DAO.TarifDAO;
import fr.cesi.model.Movie;
import fr.cesi.model.Tarif;
import fr.cesi.service.PriceCalculator;
import fr.cesi.state.ClientState;
import fr.cesi.util.AlertUtil;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class ChoixBilletController extends Controller {

	@FXML
	private Label lblDate;

	@FXML
	private Label lblHeure;

	@FXML
	private Label lblNomFilm;

	@FXML
	private Button btnAbandon;

	@FXML
	private Button btnContinuer;

	@FXML
	private Button btnRetour;

	@FXML
	private VBox tarifVBox;

	@FXML
	private ImageView imgAffiche;

	private Map<Integer, Integer> counterValues = new HashMap<>();

	public ChoixBilletController() {
		super();
	}

	@FXML
	private void initialize() {
		getSeanceInfos();
		// Get cinema's tarifs
		TarifDAO tarifDao = new TarifDAO();
		try {
			System.out.println(ClientState.getState().getSelectedCinema());
			List<Tarif> tarifs = tarifDao.listAll(ClientState.getState().getSelectedCinema());
			for (Tarif tarif:
				tarifs) {
				// Add tarif into counter value
				counterValues.put(tarif.getId(), 0);
				this.showTarif(tarif);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Récupère les informations de la séance dans le State et les affiche
	 */
	private void getSeanceInfos() {
		try {		
			ClientState state = ClientState.getState();
			// Seance info
			this.lblDate.setText(state.getSelectedSeance().getDate().toString());
			this.lblHeure.setText(state.getSelectedSeance().getTime().toString());
			MovieDAO movieDao = new MovieDAO();
			Movie movie = movieDao.findById(state.getSelectedSeance().getMovieid());
			this.lblNomFilm.setText(movie.getTitle());
			this.imgAffiche.setImage(new Image(URL_IMAGE + movie.getAffiche()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * L'utilisateur clique sur le bouton "Continuer"
	 * @param event
	 */
	@FXML
	void btnContinuerClick(MouseEvent event) {
		if (!this.isTicketSelected()) {
			AlertUtil.sendAlert(Alert.AlertType.ERROR, "Pas de billets selectionnés", "Veuillez sélectionner au moins un ticket");
			return;
		}
		try {
			ClientState.getState().setBillets(PriceCalculator.convertTicketListToPricesList(this.counterValues));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (this.mainApp != null) {
			CineGoClient.setcurrentPage(4);
			this.mainApp.showManageRootLayout();
			this.mainApp.showChoixPlaces();
		}
	}

	/**
	 * L'utilisateur clique sur le bouton retour
	 */
	public void btnRetourClick() {
		try {
			ClientState.getState().setBillets(PriceCalculator.convertTicketListToPricesList(this.counterValues));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		CineGoClient.setcurrentPage(2);
		this.mainApp.showManageRootLayout();
		this.mainApp.showChoixSeance();
	}

	/**
	 * Add onr tarif into interface
	 * @param tarif
	 */
	private void showTarif(Tarif tarif) {
		// Create HBox
		GridPane tarifFrame = new GridPane();
		tarifFrame.setId("" + tarif.getId()); // Set unique ID

		tarifFrame.setStyle("-fx-padding: 10;");

		ColumnConstraints colLabel = new ColumnConstraints();
		colLabel.setPercentWidth(30);
		ColumnConstraints colSelector = new ColumnConstraints();
		colLabel.setPrefWidth(50);
		tarifFrame.getColumnConstraints().addAll(colLabel, colSelector, colSelector, colSelector);

		// Create label
		Label tarifNameAndPrice = new Label(tarif.getName() + " : " + tarif.getPrice() + " €");
		tarifFrame.add(tarifNameAndPrice, 0, 1);

		// Create -1 btn
		JFXButton lessButton = new JFXButton("-");
		tarifFrame.add(lessButton, 1, 1);

		// Create counter button
		Label nbBillets = new Label("0");
		tarifFrame.add(nbBillets, 2, 1);

		// Create +1 button
		JFXButton moreButton = new JFXButton("+");
		tarifFrame.add(moreButton, 3, 1);

		// Add to vbox
		this.tarifVBox.getChildren().add(tarifFrame);

		// Add action after adding hbox
		lessButton.setOnAction(event -> clickOnLess(tarif.getId()));
		moreButton.setOnAction(event -> clickOnMore(tarif.getId()));
	}

	/**
	 * L'utilisateur clique sur le bouton +
	 * @param id ID du tarif
	 * @return null
	 */
	private EventHandler<ActionEvent> clickOnMore(int id) {
		GridPane gridpane = (GridPane) this.tarifVBox.lookup("#" + id);
		Label counter = (Label) gridpane.getChildren().get(2); // counter
		// Add 1 to counter
		this.counterValues.put(id, this.counterValues.get(id) + 1);
		counter.setText(counterValues.get(id).toString());
		return null;
	}

	/**
	 * L'utilisateur clique sur le bouton +
	 * @param id ID du tarif
	 * @return null
	 */
	private EventHandler<ActionEvent> clickOnLess(int id) {
		if (this.counterValues.get(id) > 0) {
			GridPane gridpane = (GridPane) this.tarifVBox.lookup("#" + id);
			Label counter = (Label) gridpane.getChildren().get(2); // counter
			// Remove 1 to counter
			this.counterValues.put(id, this.counterValues.get(id) - 1);
			counter.setText(counterValues.get(id).toString());
		}
		return null;
	}

	/**
	 * Vérifie que l'utilisateur a sélectionné un ticket
	 * @return true if minimum one ticket is selected
	 */
	private boolean isTicketSelected() {
		for (Integer nbTickets:
			this.counterValues.values()) {
			if (nbTickets > 0) {
				return true;
			}
		}
		return false;
	}
}


