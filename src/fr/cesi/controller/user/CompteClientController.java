package fr.cesi.controller.user;

import java.sql.SQLException;
import java.time.LocalDateTime;

import fr.cesi.CineGoClient;
import fr.cesi.model.Client;
import fr.cesi.model.Reservation;
import fr.cesi.model.ReservationCustom;
import fr.cesi.model.Seance;
import fr.cesi.model.SeanceCustom;
import fr.cesi.model.DAO.CinemaDAO;
import fr.cesi.model.DAO.ReservationDAO;
import fr.cesi.model.DAO.SeanceDAO;
import fr.cesi.state.ClientState;
import fr.cesi.util.AlertUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;

public class CompteClientController {

	@FXML
	private TableView<ReservationCustom> tbvReservations;

	@FXML
	private TableColumn<Reservation, String> tvcDate;

	@FXML
	private TableColumn<Reservation, String> tvcHeure;

	@FXML
	private TableColumn<Reservation, String> tvcSalle;

	@FXML
	private TableColumn<Reservation,String> tvcFilm;

	@FXML
	private Button btnAnnulation;

	@FXML
	private Button btnConsulter;

	@FXML
	private GridPane gridInfosComp;

	@FXML
	private Label txtCinema;

	@FXML
	private Label txtNbPlaces;

	@FXML
	private Label txtPlaces;

	@FXML
	private Label txtDateResa;

	@FXML
	private Label txtPrixTotal;

	@FXML
	private Button btnRetour;

	private CineGoClient mainApp = null;

	private Client client;

	/**
	 * Initialization of the window
	 */
	@FXML
	private void initialize() {
		gridInfosComp.setVisible(false);
		this.client = ClientState.getState().getConnectedUser();
		this.bindTbvReservation();
	}

	/**
	 * L'utilisateur clique sur "Consulter détails"
	 * Récupère la réservatoin sélectionnée, et affiche des messages a ce propos
	 * @param event
	 */
	@FXML
	void btnConsulterClick(MouseEvent event) {
		// Get the reservation
		ReservationCustom reservationCustom = this.tbvReservations.getSelectionModel().getSelectedItem();
		// If the selected reservation is null
		if (reservationCustom == null) {
			gridInfosComp.setVisible(false);
			AlertUtil.sendAlert(
					Alert.AlertType.WARNING,
					this.mainApp.getPrimaryStage(),
					"Pas de réservation sélectionnée",
					"Vous devez sélectionner une réservation pour consulter les détails"
					);
			return;			
		} else {
			// display the details
			gridInfosComp.setVisible(true);
			displayDetails(reservationCustom);
		}

	}
	/**
	 * set the main application for this view
	 * @param cineGoC
	 */
	public void setMainAppAdmin(CineGoClient cineGoC) {
		this.mainApp = cineGoC;

	}

	/**
	 * Récupére toutes les réservations des utilisateurs, et l'ajoute dans le tableau
	 * Get all the reservations for the connected user, and bind the tableview
	 */
	public void bindTbvReservation() {
		// Crée la liste des séances du cinéma
		ObservableList<ReservationCustom> reservationList;
		ReservationDAO reservationdao = new ReservationDAO();
		try {
			// Récupére les séances déjà en base avec le numéro de la salle et le nom du film associé
			reservationList = FXCollections.observableArrayList(reservationdao.listReservationCustom());
		} catch (SQLException e) {
			// Si erreur
			e.printStackTrace();
			AlertUtil.sendAlert(
					Alert.AlertType.ERROR,
					"Erreur SQL",
					"Nous n'avons pas pu récupérer la liste des réservations."
					);
			return;
		}
		// Sinon on remplit la tableView la liste des séances
		this.tbvReservations.setItems(reservationList);      

		// On associe chaque colonne à sa propriétés correspondantes
		this.tvcSalle.setCellValueFactory(new PropertyValueFactory<>("numRoom"));
		this.tvcDate.setCellValueFactory(new PropertyValueFactory<>("dateCrea"));
		this.tvcHeure.setCellValueFactory(new PropertyValueFactory<>("time"));
		this.tvcFilm.setCellValueFactory(new PropertyValueFactory<>("movieName"));


	}
	/**
	 * Affiche les détails de la réservation délectionnées
	 * @param reservationCustom
	 */
	public void displayDetails(ReservationCustom reservationCustom) {

		try {
			CinemaDAO cineDao = new CinemaDAO();
			ReservationDAO resaDAO = new ReservationDAO();

			txtCinema.setText(cineDao.getCinemaByRoom(reservationCustom.getRoomId()).getName());
			txtNbPlaces.setText(Integer.toString(resaDAO.getNbPlacesByReservation(reservationCustom.getId())) );
			txtDateResa.setText(reservationCustom.getDateCrea().toString());
			txtPlaces.setText(resaDAO.findById(reservationCustom.getId()).getPlaces().toString()
					.replace("[", "")
					.replace("]", ""));
			txtPrixTotal.setText(Double.toString(reservationCustom.getTotalPrice()));

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Occurs when the user clicks on "Retour"
	 * Go to the previous page
	 * @param event
	 */
	@FXML
	void btnRetourClick(MouseEvent event) {
		 this.mainApp.goToPreviousPage();
	}

	/**
	 * Cancel a reservation by id
	 * @param event
	 */
	@FXML
	void btnAnnulerResaClick(MouseEvent event) {
		// Get the reservation
		ReservationCustom reservationCustom = this.tbvReservations.getSelectionModel().getSelectedItem();
		// If the selected reservation is null
		if (reservationCustom == null) {
			gridInfosComp.setVisible(false);
			AlertUtil.sendAlert(
					Alert.AlertType.WARNING,
					this.mainApp.getPrimaryStage(),
					"Pas de réservation sélectionnée",
					"Vous devez sélectionner une réservation pour l'annuler"
					);
			return;			
		} else {		
			annulerReservation(reservationCustom);
		}

	}

	/**
	 * Cancel the reservation selected by the user
	 * @param resaCustom
	 */
	public void annulerReservation(ReservationCustom resaCustom) {
		try {
			ReservationDAO resaDAO = new ReservationDAO();
			SeanceDAO seanceDAO = new SeanceDAO();
			Reservation resa = resaDAO.findById(resaCustom.getId());
			// demande de confirmation
			if (AlertUtil.confirmationMessage(this.mainApp.getPrimaryStage(), "Êtes vous sûr de vouloir annuler cette réservation?")) {
				// Vérifier si la séance est dans 24h 
				Seance seance  = seanceDAO.findById(resa.getSeanceId());
				LocalDateTime seanceDateTime = LocalDateTime.parse(seance.getDate().toString()+"T"+seance.getTime().toString());
				LocalDateTime now = LocalDateTime.now();
				LocalDateTime plus24 = now.plusDays(1);
				// Si confirmation ok : suppression de la réservation. 
				if (seanceDateTime.isAfter(plus24)) {
					resaDAO.deleteReservationPlace(resa.getId());
					resaDAO.delete(resa);
					// Message confirmant la suppression 
					AlertUtil.sendAlert(
							Alert.AlertType.CONFIRMATION,
							this.mainApp.getPrimaryStage(),
							null,
							"La réservation a bien été annulé"
							);	
					// refresh tableview
					this.bindTbvReservation();
				} else {
					AlertUtil.sendAlert(
							Alert.AlertType.WARNING,
							this.mainApp.getPrimaryStage(),
							"Erreur",
							"La séance est dans moins de 24h, la réservation ne peut pas être annulée"
							);
				}					
				return;				
			} 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}


