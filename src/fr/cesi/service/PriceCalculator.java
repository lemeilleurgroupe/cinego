package fr.cesi.service;

import fr.cesi.model.DAO.TarifDAO;
import fr.cesi.model.Tarif;
import javafx.util.Pair;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

public class PriceCalculator {

    private ArrayList<Pair<Integer, Tarif>> prices;

    public PriceCalculator(ArrayList<Pair<Integer, Tarif>> prices) {
        this.prices = prices;
    }

    private final double REDUCTION_LESS = 2.5;
    private final double REDUCTION_MORE = 1.5;
    private final int CHANGE_REDUC = 3;

    /**
     * Calculate total price
     * @return
     */
    public double calculateTotalPrice() {
        double result = 0.00d;
        for (Pair<Integer, Tarif> nbTicketsPricePair:
                this.prices) {
            result += nbTicketsPricePair.getKey() * nbTicketsPricePair.getValue().getPrice();
        }
        return result;
    }

    /**
     * calculate the adapted reduction
     * @return
     */
    public double calculateReduction() {
        double reduc = REDUCTION_LESS;
        if (this.calculateTotalTickets() > CHANGE_REDUC)
            reduc = REDUCTION_MORE;
        return this.calculateTotalPrice() * reduc * 0.01;
    }

    /**
     * Calculate total price with the adapted reduction
     * @return
     */
    public double calculateTotalWithReduction() {
        return this.calculateTotalPrice() - this.calculateReduction();
    }

    /**
     * Calculate order's number of tickets
     * @return
     */
    public int calculateTotalTickets() {
        int result = 0;
        for (Pair<Integer, Tarif> nbTicketsPricePair:
                this.prices) {
            result += nbTicketsPricePair.getKey();
        }
        return result;
    }

    /**
     * Convert an hashmap composed by tarifIds and nbTickets to a list of number of tickets by tarifs object
     * @param ticketList
     * @return
     * @throws SQLException
     */
    public static ArrayList<Pair<Integer, Tarif>> convertTicketListToPricesList(Map<Integer, Integer> ticketList) throws SQLException {
        ArrayList<Pair<Integer, Tarif>> formatedList = new ArrayList<>();

        TarifDAO tarifDAO = new TarifDAO();
        for (Integer tarifId: ticketList.keySet()) {
            int nbPlaces = ticketList.get(tarifId);
            if (nbPlaces > 0) {
                try {
                    formatedList.add(new Pair<>(nbPlaces, tarifDAO.findById(tarifId)));
                } catch (SQLException e) {
                    throw e;
                }
            }
        }
        return formatedList;
    }
}
