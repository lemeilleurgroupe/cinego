package fr.cesi.model;

import fr.cesi.module.moduleplansalleuser.component.Place;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Reservation {

	private int id = -1;
	private String num = null;
	private Date dateCreation = null;
	private double totalPrice = -1;
	private int clientId = -1;
	private int seanceId = -1;
	private List<Place> places;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public Date getDateCreation() {
			return dateCreation;
		}
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	public double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public int getClientId() {
		return clientId;
	}
	public void setClientId(int clientId) {
		this.clientId = clientId;
	}
	public int getSeanceId() {
		return seanceId;
	}
	public void setSeanceId(int seanceId) {
		this.seanceId = seanceId;
	}

	public void setPlaces(List<Place> places) {
		this.places = places;
	}

	public List<Place> getPlaces() {
		return places;
	}
}
