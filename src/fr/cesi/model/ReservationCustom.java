package fr.cesi.model;

import java.sql.Date;
import java.sql.Time;

public class ReservationCustom {
	
	private int id = -1;
	private Date date = null;
	private Time time = null;
	private String movieName = null;
	private int numRoom = -1;
	private int roomId = -1;
	private Date dateCrea = null;
	private double totalPrice = -1;
	
	public int getId() {
		return id;
	}
	public Date getDateCrea() {
		return dateCrea;
	}
	public void setDateCrea(Date dateCrea) {
		this.dateCrea = dateCrea;
	}
	public double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Time getTime() {
		return time;
	}
	public void setTime(Time time) {
		this.time = time;
	}
	public String getMovieName() {
		return movieName;
	}
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}
	public int getNumRoom() {
		return numRoom;
	}
	public void setNumRoom(int numRoom) {
		this.numRoom = numRoom;
	}

	public int getRoomId() {
		return roomId;
	}
	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}

}
