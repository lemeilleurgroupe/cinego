package fr.cesi.model;

import java.sql.Date;
import java.sql.Time;


public class Seance {
	
	private int id = -1;
	private Date date = null;
	private Time time = null;
	private String language = null;
	private int movieid = -1;
	private int roomid = -1;

	public Seance() {
		
	}
	

	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date Date) {
		this.date = Date;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public int getMovieid() {
		return movieid;
	}

	public void setMovieid(int movieid) {
		this.movieid = movieid;
	}

	public int getRoomid() {
		return roomid;
	}

	public void setRoomid(int roomid) {
		this.roomid = roomid;
	}




}
