package fr.cesi.model;



public class Room {
	
		private int id = -1;
	    private Integer nbPlaces = null;	   
		private Integer nbRang = null;
	    private Integer nbColonne = null;
	    private Integer num = null;
	    private int cineId = -1;

	    public Room() {
	    }
	    public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public Integer getNbPlaces() {
			return nbPlaces;
		}

		public void setNbPlaces(Integer nbPlaces) {
			this.nbPlaces = nbPlaces;
		}

		public Integer getNbRang() {
			return nbRang;
		}

		public void setNbRang(Integer nbRang) {
			this.nbRang = nbRang;
		}

		public Integer getNbColonne() {
			return nbColonne;
		}

		public void setNbColonne(Integer nbColonne) {
			this.nbColonne = nbColonne;
		}

		public Integer getNum() {
			return num;
		}

		public void setNum(Integer num) {
			this.num = num;
		}

		public Integer getCineId() {
			return cineId;
		}

		public void setCineId(Integer cineId) {
			this.cineId = cineId;
		}
		
		
		 @Override
		    public String toString() {
		        return "Salle numéro : " + num.toString();
		    }

}
