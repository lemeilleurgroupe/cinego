package fr.cesi.model.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.cesi.config.Database;
import fr.cesi.model.Cinema;
import fr.cesi.model.Tarif;

public class TarifDAO implements DAO<Tarif> {
    private Connection conn;

    public TarifDAO() {
        this.conn = Database.getInstance().getConnection();
    }

    @Override
    public List<Tarif> listAll() throws SQLException {
        return null;
    }

    @Override
    public Tarif findById(int id) throws SQLException {
        PreparedStatement stmt = conn.prepareStatement(
                "SELECT * FROM TARIF WHERE TARIF_ID=?"
        );
        stmt.setString(1, ""+id);
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            Tarif tarif = new Tarif();
            tarif.setId(rs.getInt("TARIF_ID"));
            tarif.setName(rs.getString("TARIF_Nom"));
            tarif.setPrice(rs.getFloat("TARIF_Prix"));
            CinemaDAO cinemaDAO = new CinemaDAO();
            tarif.setCinema(cinemaDAO.findById(rs.getInt("CINE_ID")));
            return tarif;
        }
        return null;
    }

    @Override
    public void create(Tarif tarif) throws SQLException {
        PreparedStatement stmt = conn.prepareStatement(
                "INSERT INTO TARIF(TARIF_Nom, TARIF_Prix, CINE_ID) VALUES (?, ?, ?)"
        );
        stmt.setString(1, tarif.getName());
        stmt.setFloat(2, tarif.getPrice());
        stmt.setInt(3, tarif.getCinema().getId());
        stmt.execute();
    }

    @Override
    public void update(Tarif tarif) throws SQLException {
    	   String query = "UPDATE `TARIF` SET `TARIF_Nom` = ?, `TARIF_Prix` = ? WHERE `TARIF`.`TARIF_ID` = " + tarif.getId();
           PreparedStatement stmt = this.conn.prepareStatement(query);
           stmt.setString(1, tarif.getName());
           stmt.setFloat(2, tarif.getPrice());       

           stmt.execute();
    }

    @Override
    public void delete(Tarif tarif) throws SQLException {
        PreparedStatement stmt = conn.prepareStatement("DELETE FROM TARIF WHERE TARIF_ID = ?");
        stmt.setInt(1, tarif.getId());
        stmt.execute();
    }

    public List<Tarif> listAll(Cinema cinema) throws SQLException {
        PreparedStatement stmt = conn.prepareStatement(
                "SELECT * FROM TARIF WHERE CINE_ID = ?");
        stmt.setInt(1, cinema.getId());
        ResultSet rs = stmt.executeQuery();
        List<Tarif> tarifs = new ArrayList<>();
        CinemaDAO cineDAO = new CinemaDAO();
        while (rs.next()) {
            Tarif tarif = new Tarif();
            tarif.setId(rs.getInt("TARIF_ID"));
            tarif.setName(rs.getString("TARIF_Nom"));
            tarif.setPrice(rs.getFloat("TARIF_Prix"));
            tarif.setCinema(cineDAO.findById(rs.getInt("CINE_ID")));
            tarifs.add(tarif);
        }
        return tarifs;
    }
}
