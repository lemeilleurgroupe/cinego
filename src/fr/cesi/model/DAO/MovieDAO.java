package fr.cesi.model.DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.cesi.config.Database;
import fr.cesi.model.Movie;

public class MovieDAO implements DAO<Movie> {

    private Connection conn;

    public MovieDAO() {
        this.conn = Database.getInstance().getConnection();
    }

    @Override
    public List<Movie> listAll() throws SQLException {
        List<Movie> moviesList = new ArrayList<>();

        String query = "SELECT * FROM FILM";
        Statement stmt = this.conn.createStatement();

        ResultSet rs = stmt.executeQuery(query);

        fillMovie(moviesList, rs);

        return moviesList;
    }

    private void fillMovie(List<Movie> moviesList, ResultSet rs) throws SQLException {
        while (rs.next()) {
            Movie movie = new Movie();
            movie.setId(rs.getInt("FILM_ID"));
            movie.setTitle(rs.getString("FILM_Titre"));
            movie.setDirector(rs.getString("FILM_Real"));
            movie.setAffiche(rs.getString("FILM_Affiche"));
            movie.setOverview(rs.getString("FILM_Synopsis"));
            movie.setReleaseDate(rs.getDate("FILM_DateSortie"));
            movie.setRuntime(rs.getInt("FILM_Duree"));
            movie.setGenre(rs.getString("FILM_Genre"));

            moviesList.add(movie);
        }
    }


    public List<Movie> listAllByCine(int cineID) throws SQLException {
        List<Movie> moviesList = new ArrayList<>();

        String query = "SELECT FILM.FILM_ID, FILM.FILM_Titre, FILM.FILM_Real, FILM.FILM_Affiche, FILM.FILM_Synopsis, FILM.FILM_DateSortie, FILM.FILM_Duree, FILM.FILM_Genre FROM FILM, SEANCE, SALLE, CINEMA "
        		+ "WHERE FILM.FILM_ID = SEANCE.FILM_ID AND SEANCE.SALLE_ID = SALLE.SALLE_ID AND SALLE.CINE_ID = " + cineID +  " AND SEANCE.SEANCE_Date >= CURRENT_DATE group by FILM.FILM_ID";
        Statement stmt = this.conn.createStatement();

        ResultSet rs = stmt.executeQuery(query);

        fillMovie(moviesList, rs);

        return moviesList;
    }

    @Override
    public Movie findById(int id) throws SQLException {
    	

        String query = "SELECT * FROM FILM WHERE FILM_ID = " + id;
        Statement stmt = this.conn.createStatement();

        ResultSet rs = stmt.executeQuery(query);
        
        Movie movie = new Movie();
           
        while (rs.next()) {
            movie.setId(rs.getInt("FILM_ID"));
            movie.setTitle(rs.getString("FILM_Titre"));
            movie.setDirector(rs.getString("FILM_Real"));
            movie.setAffiche(rs.getString("FILM_Affiche"));
            movie.setOverview(rs.getString("FILM_Synopsis"));
            movie.setReleaseDate(rs.getDate("FILM_DateSortie"));
            movie.setRuntime(rs.getInt("FILM_Duree"));
            movie.setGenre(rs.getString("FILM_Genre"));
        }  
            
            return movie;
    }

    @Override
    public void create(Movie movie) throws SQLException {
        String query = "INSERT INTO " +
                "FILM (FILM_ID, FILM_Titre, FILM_Real, FILM_Affiche, FILM_Synopsis, FILM_DateSortie, FILM_Duree, FILM_Genre) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement stmt = this.conn.prepareStatement(query);
        stmt.setInt(1, movie.getId());
        stmt.setString(2, movie.getTitle());
        stmt.setString(3, movie.getDirector());
        stmt.setString(4, movie.getAffiche());
        stmt.setString(5, movie.getOverview());
        stmt.setDate(6, (Date) movie.getReleaseDate());
        stmt.setInt(7, movie.getRuntime());
        stmt.setString(8, movie.getGenre());
        stmt.execute();
    }

    @Override
    public void update(Movie object) {

    }

    @Override
    public void delete(Movie movie) throws SQLException {
        String query = "DELETE FROM FILM WHERE FILM_ID = ?";
        PreparedStatement stmt = this.conn.prepareStatement(query);
        stmt.setInt(1, movie.getId());
        stmt.execute();
    }
}
