package fr.cesi.model.DAO;

import java.sql.SQLException;
import java.util.List;

public interface DAO<T> {

    /**
     * Liste tous les objets appartenant a cette entitée en base de données
     * @return a List
     * @throws SQLException
     */
    List<T> listAll() throws SQLException;

    /**
     * Récupére un objet grace a son ID en base de données
     * @param id id de l'ibjet
     * @return
     * @throws SQLException
     */
    T findById(int id) throws SQLException;

    /**
     * Créé une nouvelle entrée dans la base de données
     * @param object
     * @throws SQLException
     */
    void create(T object) throws SQLException;

    /**
     * Met à jour l'objet dans la base de données
     * @param object L'objet a mettre à jour
     * @throws SQLException
     */
    void update(T object) throws SQLException;

    /**
     * Supprime l'objet dans la base de données
     * @param object L'objet a supprimer
     * @throws SQLException
     */
    void delete(T object) throws SQLException;
}
