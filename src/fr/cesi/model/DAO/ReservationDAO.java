package fr.cesi.model.DAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import fr.cesi.config.Database;
import fr.cesi.model.Reservation;
import fr.cesi.model.ReservationCustom;
import fr.cesi.model.SeanceCustom;
import fr.cesi.module.moduleplansalleuser.component.Place;
import fr.cesi.state.AdminState;
import fr.cesi.state.ClientState;
import fr.cesi.util.DateUtil;

public class ReservationDAO implements DAO<Reservation> {

	private Connection conn = Database.getInstance().getConnection();

	@Override
	public List<Reservation> listAll() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Reservation findById(int id) throws SQLException {
		String query = "SELECT * FROM RESERVATION WHERE RES_ID = ?";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		stmt.setInt(1, id);
		ResultSet rsResa = stmt.executeQuery();

		Reservation resa = new Reservation();

		query = "SELECT P.PLACE_ID, P.PLACE_Colonne, P.PLACE_Ligne FROM PLACE P, RESERVATION_PLACE RP WHERE P.PLACE_ID = RP.PLACE_ID AND RES_ID = ?";
		stmt = this.conn.prepareStatement(query);
		stmt.setInt(1, id);
		ResultSet rsPlaces = stmt.executeQuery();
		if (rsPlaces == null) {
			return getReservation(rsResa, resa);
		}
		ArrayList<Place> places = new ArrayList<>();
		while(rsPlaces.next()) {
			Place place = new Place(
					rsPlaces.getInt(1),
					rsPlaces.getInt(2),
					rsPlaces.getInt(3)
			);
			places.add(place);

		}
		resa.setPlaces(places);
		return getReservation(rsResa, resa);
	}

	public Reservation findByNum(String num) throws SQLException {
		String query = "SELECT * FROM RESERVATION WHERE RES_Num LIKE ?";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		stmt.setString(1, num);
		ResultSet rs = stmt.executeQuery();

		Reservation resa = new Reservation();

		return getReservation(rs, resa);
	}

	@Override
	public void create(Reservation resa) throws SQLException {
		String query = "INSERT INTO RESERVATION(RES_Num, RES_DateCrea, RES_PrixFinal, CLIENT_ID, SEANCE_ID)" +
				"VALUES (?, ?, ?, ?, ?)";
		PreparedStatement stmt = this.conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		stmt.setString(1, resa.getNum());
		stmt.setDate(2, DateUtil.toSqlDate(resa.getDateCreation()));
        stmt.setDouble(3, resa.getTotalPrice());
        if (resa.getClientId() > 0)
            stmt.setInt(4, resa.getClientId());
        else
            stmt.setNull(4, Types.INTEGER);
        stmt.setInt(5, resa.getSeanceId());
        stmt.executeUpdate();
		ResultSet rs = stmt.getGeneratedKeys();
		int resaId = -1;
		if (rs.next()){
			resaId = rs.getInt(1);
		}
		if (resaId < 0)
			throw new SQLException("Cannot retrieve last reservation ID.");
        try {
			for (Place place:
				resa.getPlaces()) {
				query = "INSERT INTO RESERVATION_PLACE(PLACE_ID, RES_ID)" +
						"VALUES (?, ?)";
				stmt = this.conn.prepareStatement(query);
				stmt.setInt(1, place.getID());
				stmt.setInt(2, resaId);
				stmt.execute();
			}
		} catch (SQLException e) {
        	// Remove reservation if something happened
			this.deleteReservationPlace(resaId);
			throw e;
		}
	}

		@Override
	public void update(Reservation object) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(Reservation resa) throws SQLException {
		String query = "DELETE FROM RESERVATION WHERE RESERVATION.RES_ID = ?";
		PreparedStatement stmt = this.conn.prepareStatement(query);
	    stmt.setInt(1, resa.getId());
	    stmt.execute();
	}

	public List<ReservationCustom> listReservationCustom() throws SQLException {

		List<ReservationCustom>reservationList = new ArrayList<>();

		String query = "" +
				"SELECT SEANCE.SEANCE_Date, SEANCE.SEANCE_Heure, SALLE.SALLE_Num, FILM.FILM_Titre, RESERVATION.RES_ID, SALLE.SALLE_ID, RESERVATION.RES_DateCrea, RESERVATION.RES_PrixFinal " +
				"FROM SEANCE, SALLE, FILM, CLIENT, RESERVATION " +
				"WHERE CLIENT.CLIENT_ID = RESERVATION.CLIENT_ID " +
				"AND RESERVATION.SEANCE_ID = SEANCE.SEANCE_ID " +
				"AND FILM.FILM_ID = SEANCE.FILM_ID " + 
				"AND SEANCE.SALLE_ID = SALLE.SALLE_ID " + 
				"AND CLIENT.CLIENT_ID = " + ClientState.getState().getConnectedUser().getId();

		Statement stmt = this.conn.createStatement();

		ResultSet rs = stmt.executeQuery(query);

		while (rs.next()) {
			ReservationCustom reservation = new ReservationCustom();
			reservation.setId(rs.getInt("RES_ID"));
			reservation.setDate(rs.getDate("SEANCE_Date"));
			reservation.setTime(rs.getTime("SEANCE_Heure"));        	
			reservation.setMovieName(rs.getString("FILM_Titre"));
			reservation.setNumRoom(rs.getInt("SALLE_Num"));        
			reservation.setRoomId(rs.getInt("SALLE_ID"));
			reservation.setDateCrea(rs.getDate("RES_DateCrea"));
			reservation.setTotalPrice(rs.getDouble("RES_PrixFinal"));

			reservationList.add(reservation);

		}

		return reservationList;
	}
	
	public void deleteReservationPlace(int resaID) throws SQLException {
		String query = "DELETE FROM RESERVATION_PLACE WHERE RESERVATION_PLACE.RES_ID = ?";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		stmt.setInt(1, resaID);
		stmt.execute();
	}

	public int getNbPlacesByReservation(int resaID) throws SQLException {

		String query = "SELECT * FROM RESERVATION_PLACE WHERE RESERVATION_PLACE.RES_ID = " + resaID;
		Statement stmt = this.conn.createStatement();
		ResultSet rs = stmt.executeQuery(query);
		return getRowCount(rs);

	}
	/**
	 * Retourne le nombre de lignes d'une requete
	 *
	 * @param resultSet
	 * @return
	 */
	private int getRowCount(ResultSet resultSet) {
		if (resultSet == null) {
			return 0;
		}
		try {
			resultSet.last();
			return resultSet.getRow();
		} catch (SQLException exp) {
			exp.printStackTrace();
		} finally {
			try {
				resultSet.beforeFirst();
			} catch (SQLException exp) {
				exp.printStackTrace();
			}
		}
		return 0;
	}

	/**
	 * Fill reservation object from ResultSet
	 * @param rs
	 * @param resa
	 * @return
	 * @throws SQLException
	 */
	private Reservation getReservation(ResultSet rs, Reservation resa) throws SQLException {
		if (!rs.next())
			return null;

		do {
			resa.setId(rs.getInt("RES_ID"));
			resa.setNum(rs.getString("RES_Num"));
			resa.setDateCreation(rs.getDate("RES_DateCrea"));
			resa.setTotalPrice(rs.getDouble("RES_PrixFinal"));
			resa.setClientId(rs.getInt("CLIENT_ID"));
			resa.setSeanceId(rs.getInt("SEANCE_ID"));
		} while(rs.next());

		return resa;
	}

}
