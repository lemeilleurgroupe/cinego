package fr.cesi.model.DAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import fr.cesi.config.Database;
import fr.cesi.model.Cinema;
import fr.cesi.model.Room;

public class RoomDAO implements DAO<Room> {
	
	 private Connection conn;

	    public RoomDAO() {
	        this.conn = Database.getInstance().getConnection();
	    }

	    @Override
	    public List<Room> listAll() throws SQLException {
	        List<Room> roomList = new ArrayList<>();

	        String query = "SELECT * FROM SALLE";
	        Statement stmt = this.conn.createStatement();

	        ResultSet rs = stmt.executeQuery(query);

			createRoomsList(roomList, rs);

			return roomList;
	    }

	public List<Room> listAllByCinema(Cinema cinema) throws SQLException {
		List<Room> roomList = new ArrayList<>();

		String query = "SELECT * FROM SALLE WHERE CINE_ID = ?";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		stmt.setInt(1, cinema.getId());
		ResultSet rs = stmt.executeQuery();

		createRoomsList(roomList, rs);

		return roomList;
	}

	private void createRoomsList(List<Room> roomList, ResultSet rs) throws SQLException {
		while (rs.next()) {
			Room room = new Room();
			room.setId(rs.getInt("SALLE_ID"));
			room.setNbPlaces(rs.getInt("SALLE_NbPlaces"));
			room.setNbRang(rs.getInt("SALLE_NbRang"));
			room.setNbColonne(rs.getInt("SALLE_NbColonne"));
			room.setNum(rs.getInt("SALLE_Num"));
			room.setCineId(rs.getInt("CINE_ID"));

			roomList.add(room);
		}
	}

	public List<Room> listAllByCine(Cinema cine) throws SQLException {
	        List<Room> roomList = new ArrayList<>();

	        String query = "SELECT * FROM SALLE WHERE SALLE.CINE_ID = " + cine.getId();
	        Statement stmt = this.conn.createStatement();

	        ResultSet rs = stmt.executeQuery(query);

		createRoomsList(roomList, rs);

		return roomList;
	    }

	

	@Override
	public void create(Room object) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Room object) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Room object) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Room findById(int id) throws SQLException {		 
	       
	        	String query = "SELECT * FROM SALLE WHERE SALLE_ID = " + id;
	        	Statement stmt = this.conn.createStatement();
		       
		        
		        ResultSet rs = stmt.executeQuery(query);
		        
		        Room room = new Room();
		       
		        while (rs.next()) {
		       
		        room.setId(rs.getInt("SALLE_ID"));
		        room.setNbPlaces(rs.getInt("SALLE_NbPlaces"));
		        room.setNbRang(rs.getInt("SALLE_NbRang"));
		        room.setNbColonne(rs.getInt("SALLE_NbColonne"));
		        room.setNum(rs.getInt("SALLE_Num"));
		        room.setCineId(rs.getInt("CINE_ID"));           
		        }

		        return room;		
	       
	     
	}
	
	
	

}
