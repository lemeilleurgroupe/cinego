package fr.cesi.model.DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import fr.cesi.CineGo;
import fr.cesi.config.Database;
import fr.cesi.controller.user.HomePageController;
import fr.cesi.model.Movie;
import fr.cesi.model.Room;
import fr.cesi.model.Seance;
import fr.cesi.model.SeanceCustom;
import fr.cesi.state.AdminState;

public class SeanceDAO implements DAO<Seance> {

    private Connection conn;

    public SeanceDAO() {
        this.conn = Database.getInstance().getConnection();
    }

    @Override
    public List<Seance> listAll() throws SQLException {
        List<Seance> seanceList = new ArrayList<>();

        String query = "SELECT * FROM SEANCE";
        Statement stmt = this.conn.createStatement();

        ResultSet rs = stmt.executeQuery(query);

        while (rs.next()) {
            Seance seance = new Seance();
            seance.setId(rs.getInt("SEANCE_ID"));
            seance.setDate(rs.getDate("SEANCE_Date"));
            seance.setTime(rs.getTime("SEANCE_Heure"));
            seance.setLanguage(rs.getString("SEANCE_Langue"));
            seance.setMovieid(rs.getInt("FILM_ID"));
            seance.setRoomid(rs.getInt("SALLE_ID"));

            seanceList.add(seance);
        }

        return seanceList;

    }

    public List<Seance> listAllByMovieDate(Movie film, Date d) throws SQLException {
        List<Seance> seanceList = new ArrayList<>();

        String query = "SELECT DISTINCT SEANCE.SEANCE_ID, SEANCE.SEANCE_Date, SEANCE.SEANCE_Heure, SEANCE.SEANCE_Langue, SEANCE.FILM_ID, SEANCE.SALLE_ID FROM SEANCE, FILM, SALLE, CINEMA WHERE FILM.FILM_ID = " + film.getId() + " AND FILM.FILM_ID = SEANCE.FILM_ID AND SEANCE.SALLE_ID = SALLE.SALLE_ID AND SALLE.CINE_ID = " + HomePageController.cine.getId() + " AND SEANCE.SEANCE_DATE = " + "\"" + d + "\"";
        Statement stmt = this.conn.createStatement();

        ResultSet rs = stmt.executeQuery(query);

        while (rs.next()) {
            Seance seance = new Seance();
            seance.setId(rs.getInt("SEANCE_ID"));
            seance.setDate(rs.getDate("SEANCE_Date"));
            seance.setTime(rs.getTime("SEANCE_Heure"));
            seance.setLanguage(rs.getString("SEANCE_Langue"));
            seance.setMovieid(rs.getInt("FILM_ID"));
            seance.setRoomid(rs.getInt("SALLE_ID"));

            seanceList.add(seance);
        }

        return seanceList;

    }

    public List<Date> listDateByMovie(Movie film) throws SQLException {
        List<Date> dateList = new ArrayList<>();

        String query = "SELECT SEANCE.SEANCE_Date FROM SEANCE, FILM, SALLE, CINEMA WHERE FILM.FILM_ID = " + film.getId() + " AND FILM.FILM_ID = SEANCE.FILM_ID AND SEANCE.SALLE_ID = SALLE.SALLE_ID AND SALLE.CINE_ID = " + HomePageController.cine.getId() + " AND SEANCE.SEANCE_Date >= CURRENT_DATE GROUP BY SEANCE.SEANCE_Date";
        Statement stmt = this.conn.createStatement();

        ResultSet rs = stmt.executeQuery(query);

        while (rs.next()) {
            dateList.add(rs.getDate("SEANCE_Date"));
        }

        return dateList;

    }

    public List<Time> listHeureByMovieDate(Movie film, Date date) throws SQLException {
        List<Time> timeList = new ArrayList<>();

        String query = "SELECT SEANCE.SEANCE_Heure FROM SEANCE WHERE FILM_ID = " + film.getId() + " AND SEANCE_DATE = " + "\"" + date + "\"";
        Statement stmt = this.conn.createStatement();

        ResultSet rs = stmt.executeQuery(query);

        while (rs.next()) {
            timeList.add(rs.getTime("SEANCE_Heure"));
        }

        return timeList;

    }

    /*public Seance getSeanceByDateTimeMovie*/

    public List<SeanceCustom> listSeanceCustom() throws SQLException {

        List<SeanceCustom> seanceList = new ArrayList<>();

        String query = "" +
                "SELECT DISTINCT `SEANCE_ID`, `SEANCE_Date`, `SEANCE_Heure`, `SEANCE_Langue`, `SALLE_Num`, `FILM_Titre`, FILM.`FILM_ID`, SALLE.`SALLE_ID` " +
                "FROM SEANCE, SALLE, FILM, CINEMA " +
                "WHERE SEANCE.SALLE_ID = SALLE.SALLE_ID " +
                "AND SEANCE.FILM_ID = FILM.FILM_ID " +
                "AND SALLE.CINE_ID = " + AdminState.getState().getCinema().getId();

        Statement stmt = this.conn.createStatement();

        ResultSet rs = stmt.executeQuery(query);

        while (rs.next()) {
            SeanceCustom seance = new SeanceCustom();
            seance.setId(rs.getInt("SEANCE_ID"));
            seance.setDate(rs.getDate("SEANCE_Date"));
            seance.setTime(rs.getTime("SEANCE_Heure"));
            seance.setLanguage(rs.getString("SEANCE_Langue"));
            seance.setMovieName(rs.getString("FILM_Titre"));
            seance.setNumRoom(rs.getInt("SALLE_Num"));
            seance.setMovieId(rs.getInt("FILM_ID"));
            seance.setRoomId(rs.getInt("SALLE_ID"));

            seanceList.add(seance);

        }

        return seanceList;
    }

    @Override
    public Seance findById(int id) throws SQLException {
        String query = "SELECT * FROM SEANCE WHERE SEANCE_ID = " + id;
        Statement stmt = this.conn.createStatement();


        ResultSet rs = stmt.executeQuery(query);

        Seance seance = new Seance();

        while (rs.next()) {

            seance.setId(rs.getInt("SEANCE_ID"));
            seance.setDate(rs.getDate("SEANCE_Date"));
            seance.setTime(rs.getTime("SEANCE_Heure"));
            seance.setLanguage(rs.getString("SEANCE_Langue"));
            seance.setMovieid(rs.getInt("FILM_ID"));
            seance.setRoomid(rs.getInt("SALLE_ID"));
        }

        return seance;

    }

    @Override
    public void create(Seance seance) throws SQLException {

        String query = "INSERT INTO " +
                "SEANCE (SEANCE_Date, SEANCE_Heure, SEANCE_Langue, FILM_ID, SALLE_ID) " +
                "VALUES (?, ?, ?, ?, ?)";
        PreparedStatement stmt = this.conn.prepareStatement(query);
        stmt.setDate(1, seance.getDate());
        stmt.setTime(2, seance.getTime());
        stmt.setString(3, seance.getLanguage());
        stmt.setInt(4, seance.getMovieid());
        stmt.setInt(5, seance.getRoomid());

        stmt.execute();
    }
    
   
    public void upadateInitNbPlaces (Seance seance) throws SQLException
    {
    	String query = "UPDATE `SEANCE` "
    				 + "SET `SEANCE_PlaceRestante` = (SELECT `SALLE_NbPlaces`"
    				 					  	  + "FROM SALLE "
    				 					      + "WHERE SALLE_ID = " + seance.getRoomid() + ")"
    				 + "WHERE `SEANCE`.`SEANCE_ID` = " + seance.getId();
        PreparedStatement stmt = this.conn.prepareStatement(query);     

        stmt.execute();
    }

    @Override
    public void update(Seance seance) throws SQLException {
        String query = "UPDATE `SEANCE` SET `SEANCE_Date` = ?, `SEANCE_Heure` = ?, `SEANCE_Langue` = ?, `FILM_ID` = ?, `SALLE_ID` = ? WHERE `SEANCE`.`SEANCE_ID` = " + seance.getId();
        PreparedStatement stmt = this.conn.prepareStatement(query);
        stmt.setDate(1, seance.getDate());
        stmt.setTime(2, seance.getTime());
        stmt.setString(3, seance.getLanguage());
        stmt.setInt(4, seance.getMovieid());
        stmt.setInt(5, seance.getRoomid());

        stmt.execute();

    }

    @Override
    public void delete(Seance seance) throws SQLException {
        String query = "DELETE FROM SEANCE WHERE SEANCE_ID = ?";
        PreparedStatement stmt = this.conn.prepareStatement(query);
        stmt.setInt(1, seance.getId());
        stmt.execute();

    }

    /**
     * Supprime une seance depuis la tableView des seances
     *
     * @param seance
     * @throws SQLException
     */
    public void deleteCustom(SeanceCustom seance) throws SQLException {
        String query = "DELETE FROM SEANCE WHERE SEANCE_ID = ?";
        PreparedStatement stmt = this.conn.prepareStatement(query);
        stmt.setInt(1, seance.getId());
        stmt.execute();

    }

    /**
     * Verifie si il existe des reservations existantes pour cette seance
     *
     * @param seance
     * @return
     * @throws SQLException
     */
    public Boolean existingReservation(SeanceCustom seance) throws SQLException {
        String query = "SELECT * FROM RESERVATION WHERE SEANCE_ID = " + seance.getId();        
        Statement stmt = this.conn.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        // Si il n y a pas de reservations => retourne false
        if (getRowCount(rs) == 0) {
            return false;
        } else {
            return true;
        }
    }


    public int NbPlacesRestantes(Seance seance) throws SQLException {
        String query = "SELECT * FROM RESERVATION WHERE SEANCE_ID = " + seance.getId();
        ;
        Statement stmt = this.conn.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        int nbResaBySeance = getRowCount(rs);
        Room r = new Room();
        RoomDAO dao = new RoomDAO();

        r = dao.findById(seance.getRoomid());
        return r.getNbPlaces() - nbResaBySeance;

    }

    /**
     * Retourne le nombre de lignes d'une requete
     *
     * @param resultSet
     * @return
     */
    private int getRowCount(ResultSet resultSet) {
        if (resultSet == null) {
            return 0;
        }
        try {
            resultSet.last();
            return resultSet.getRow();
        } catch (SQLException exp) {
            exp.printStackTrace();
        } finally {
            try {
                resultSet.beforeFirst();
            } catch (SQLException exp) {
                exp.printStackTrace();
            }
        }
        return 0;
    }


}
