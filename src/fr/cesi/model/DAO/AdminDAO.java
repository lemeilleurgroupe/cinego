package fr.cesi.model.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;

import fr.cesi.config.Database;
import fr.cesi.model.Admin;

public class AdminDAO implements DAO<Admin>{

    private final Connection databaseConnection;

    public AdminDAO() {
        this.databaseConnection = Database.getInstance().getConnection();
    }

    @Override
    public List<Admin> listAll() {
        return null;
    }

    @Override
    public Admin findById(int id) {
        return null;
    }

    @Override
    public void create(Admin object) {

    }

    @Override
    public void update(Admin object) {

    }

    @Override
    public void delete(Admin object) {

    }

    /**
     * Return user if login and password is used
     * @param login
     * @param password
     * @return
     * @throws SQLException
     */
    public Admin findByLoginAndPassword(String login, String password) throws SQLException {

        // Hash password
        String hashedPassword = DigestUtils.sha256Hex(password);

        Admin admin = null;
        String query = "SELECT * FROM ADMIN WHERE ADM_Login = ? AND ADM_Mdp = ?";
        PreparedStatement stmt = this.databaseConnection.prepareStatement(query);
        stmt.setString(1, login);
        stmt.setString(2, hashedPassword);
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            admin = new Admin();
            admin.setId(rs.getInt(1));
            admin.setEmail(rs.getString("ADM_Login"));
            admin.setNom(rs.getString("ADM_Nom"));
            admin.setPrenom(rs.getString("ADM_Prenom"));
            admin.setPassword(rs.getString("ADM_Mdp"));
        }
        return admin;
    }
}