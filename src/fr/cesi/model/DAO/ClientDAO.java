package fr.cesi.model.DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;

import fr.cesi.config.Database;
import fr.cesi.model.Client;

public class ClientDAO implements DAO<Client>{
    private Connection conn = Database.getInstance().getConnection();

    @Override
    public List<Client> listAll() throws SQLException {
        return null;
    }

    @Override
    public Client findById(int id) throws SQLException {
        return null;
    }

    public Client findByEmail(String email) throws SQLException {
        PreparedStatement stmt = this.conn.prepareStatement(
                "SELECT * FROM CLIENT WHERE CLIENT_Mail=?"
        );
        stmt.setString(1, email);
        ResultSet rs = stmt.executeQuery();
        Client client = fillClient(rs);
        if (client != null) return client;
        return null;
    }

    @Override
    public void create(Client client) throws SQLException {
        PreparedStatement stmt = this.conn.prepareStatement(
                "INSERT INTO CLIENT(CLIENT_Prenom, CLIENT_Nom, CLIENT_DateNaissance, CLIENT_NumCarte, CLIENT_Mail, CLIENT_Mdp) " +
                    "VALUES(?, ?, ?, ?, ?, ?)"
        );
        stmt.setString(1, client.getSurname());
        stmt.setString(2, client.getLastname());
        stmt.setDate(3, (Date) client.getBirthday());
        stmt.setString(4, client.getNumCard());
        stmt.setString(5, client.getMail());
        stmt.setString(6, DigestUtils.sha256Hex(client.getPassword()));
        stmt.execute();
    }

    @Override
    public void update(Client object) throws SQLException {

    }

    @Override
    public void delete(Client object) throws SQLException {

    }

    public Client findByEmailAndPassword(String email, String password) throws SQLException {
        String hashedPassword = DigestUtils.sha256Hex(password);
        PreparedStatement stmt = this.conn.prepareStatement(
                "SELECT * FROM CLIENT WHERE CLIENT_Mail=? AND CLIENT_Mdp=?"
        );
        stmt.setString(1, email);
        stmt.setString(2, hashedPassword);
        ResultSet rs = stmt.executeQuery();
        Client client = fillClient(rs);
        if (client != null) return client;
        return null;
    }

    private Client fillClient(ResultSet rs) throws SQLException {
        if (rs.next()) {
            Client client = new Client();
            client.setId(rs.getInt("CLIENT_ID"));
            client.setSurname(rs.getString("CLIENT_Prenom"));
            client.setLastname(rs.getString("CLIENT_Nom"));
            client.setMail(rs.getString("CLIENT_Mail"));
            client.setBirthday(rs.getDate("CLIENT_DateNaissance"));
            client.setNumCard(rs.getString("CLIENT_NumCarte"));
            client.setPassword(rs.getString("CLIENT_Mdp"));
            return client;
        }
        return null;
    }
}
