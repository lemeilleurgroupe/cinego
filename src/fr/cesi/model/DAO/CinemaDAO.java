package fr.cesi.model.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.cesi.config.Database;
import fr.cesi.model.Cinema;

public class CinemaDAO implements DAO<Cinema> {

	Connection conn = Database.getInstance().getConnection();

	@Override
	public List<Cinema> listAll() throws SQLException {
		List<Cinema> cineList = new ArrayList<>();

		String query = "SELECT * FROM CINEMA";
		Statement stmt = this.conn.createStatement();

		ResultSet rs = stmt.executeQuery(query);

		while (rs.next()) {
			Cinema cine = new Cinema();
			cine.setId(rs.getInt("CINE_ID"));
			cine.setName(rs.getString("CINE_Nom"));
			cine.setCity(rs.getString("CINE_Ville"));
			cine.setNbFidelity(rs.getInt("CINE_NbChiffreFidelite"));              

			cineList.add(cine);
		}

		return cineList;
	}

	@Override
	public Cinema findById(int id) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement("SELECT * FROM CINEMA WHERE CINE_ID = ?");
		stmt.setInt(1, id);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			Cinema cinema = new Cinema();
			cinema.setId(rs.getInt("CINE_ID"));
			cinema.setName(rs.getString("CINE_Nom"));
			cinema.setCity(rs.getString("CINE_Ville"));
			cinema.setNbFidelity(rs.getInt("CINE_NbChiffreFidelite"));
			return cinema;
		}
		return null;
	}

	@Override
	public void create(Cinema cinema) throws SQLException {

	}

	@Override
	public void update(Cinema cinema) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(
				"UPDATE CINEMA SET CINE_Nom = ?, CINE_Ville = ?, CINE_NbChiffreFidelite = ? " +
				"WHERE CINE_ID = ?");
		stmt.setString(1, cinema.getName());
		stmt.setString(2, cinema.getCity());
		stmt.setInt(3, cinema.getNbFidelity());
		stmt.setInt(4, cinema.getId());
		stmt.execute();
	}

	@Override
	public void delete(Cinema object) throws SQLException {

	}

	public Cinema getCinemaByRoom(int roomId) throws SQLException {
		PreparedStatement stmt;

		stmt = conn.prepareStatement("SELECT * FROM CINEMA, SALLE WHERE  SALLE.CINE_ID = CINEMA.CINE_ID AND SALLE.SALLE_ID = ? ");
		stmt.setInt(1, roomId);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			Cinema cinema = new Cinema();
			cinema.setId(rs.getInt("CINE_ID"));
			cinema.setName(rs.getString("CINE_Nom"));
			cinema.setCity(rs.getString("CINE_Ville"));
			cinema.setNbFidelity(rs.getInt("CINE_NbChiffreFidelite"));
			return cinema;
		}
		return null;	          


		
	}
}
