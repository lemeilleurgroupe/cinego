package fr.cesi.module.moduleplansalle.application;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import javafx.application.Application;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import fr.cesi.module.moduleplansalle.xmlGestion.LecteurXML;

public class Parcourir extends Application {
	
	private String pathXMLConcerned;
	private GridPane gp;
	private String nameFileImport;
	public String getNameFileImport() {
		return nameFileImport;
	}

	public void setNameFileImport(String nameFileImport) {
		this.nameFileImport = nameFileImport;
	}

	public GridPane getGp() {
		return gp;
	}

	public void setGp(GridPane gp) {
		this.gp = gp;
	}

	public String getPathXMLConcerned() {
		return pathXMLConcerned;
	}

	public void setPathXMLConcerned(String pathXMLConcerned) {
		this.pathXMLConcerned = pathXMLConcerned;
	}	
	
	/**
	 * Constructeur de la classe Parcourir
	 * @param GridPane gp
	 */
	public Parcourir(GridPane gp){
		this.gp = gp;		
		this.gp.getChildren().clear();
	}
	/**
	 * Ouvre un explorateur de fichier
	 */
	public void start() throws Exception {
		
		Stage newWindow = new Stage();
		final FileChooser fileChooser = new FileChooser();
		configuringFileChooser(fileChooser);		            
                File file = fileChooser.showOpenDialog(newWindow);
                if (file != null) {
                    openFile(file);
                    List<File> files = Arrays.asList(file);
                    try {
						printLog(files);
					} catch (ParserConfigurationException | SAXException | IOException e) {
						e.printStackTrace();
					}
                }
	    }
	/**
	 * Param�trage de la fen�tre d'explorateur de fichier (Title, extensions autoris�es, default path de recherche)
	 * @param FileChooser filechooser
	 */
	private void configuringFileChooser(FileChooser fileChooser) {
		 
	      // Set title for FileChooser
	      fileChooser.setTitle("Select XML file to import");
	 
	      // Set Initial Directory
	      fileChooser.setInitialDirectory(new File("C:/"));
	 
	      // Add Extension Filters
	      fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("XML", "*.xml")); //
	              
	  }
	 
	/**
	 * Stock le fichier choisit par l'utilisateur
	 * @param List<File>files
	 */
	private void printLog(List<File>files) throws ParserConfigurationException, SAXException, IOException {
			if(files == null || files.isEmpty())
			{
				return;
			}
			
			for(File file : files)
			{
				System.out.println(file.getAbsolutePath() + "\n");
				this.pathXMLConcerned = file.getAbsolutePath();
				this.CreateXMLFile(this.pathXMLConcerned);
			}
			
		}
	/**
	 * Envoie le fichier XMl pour analyse via le lecteurXML
	 * @param String path
	 */
	public  void CreateXMLFile(String path) throws ParserConfigurationException, SAXException, IOException
	{
		LecteurXML lXml = new LecteurXML(path,this.gp);
		lXml.initSAX();
	}
	
	
	public String ExtractNameFileFromUri(String path)
	{
		String valueOfReturn = "";
		String[] fields = path.split("\\");
		String[] subString = fields[fields.length-1].split(".");		
		valueOfReturn = subString[0];
		
		System.out.println("ExctractNameFileFromUri : " + valueOfReturn);		
		this.nameFileImport = valueOfReturn;		
		return valueOfReturn;
		
	}
	private void openFile(File file) {
			//Appel du saxParseur sur le fichier pass� en argument
	}
		
	public static void main(String[] args) {
			Application.launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {}	
}


