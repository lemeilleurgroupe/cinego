package fr.cesi.module.moduleplansalle.application;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;

import fr.cesi.module.moduleplansalle.component.Sit;
import fr.cesi.module.moduleplansalle.component.SitHandi;
import fr.cesi.module.moduleplansalle.contentStatic.Utils;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import fr.cesi.module.moduleplansalle.xmlGestion.ParseurXML;

public class PlanOverviewController {
	@FXML
	private SplitPane splitpan;
	@FXML
	private AnchorPane pan_left;
	@FXML
	private AnchorPane pan_right;
	@FXML
	private GridPane gridpan;
	public GridPane getGridpan() {
		return gridpan;
	}

	public void setGridpan(GridPane gridpan) {
		this.gridpan = gridpan;
	}

	@FXML
	private Button btn_sit;
	@FXML
	private Button btn_handi;
	@FXML
	private Button couloir;
	@FXML
	private Button empty_zone;
	@FXML
	private Button btn_xml;	
	@FXML
	private Button btn_import;	
	@FXML
	private Button btn_clearnode;
	
	
	private int[] colAndRow;

	private ArrayList<Node> allNodes = new ArrayList<Node>();

	/**
	 * Constructeur PlanOverviewController
	 * Initialisation du tableau de coordonn�es [0;0]
	 */
	public PlanOverviewController() {
		colAndRow = new int [2];
		//col
		colAndRow[0] = 0;
		//row
		colAndRow[1] = 0;		
		
	}

	/**
	 * Initializes the controller class. This method is automatically called after
	 * the fxml file has been loaded.
	 * Initialisation du panel gauche avec 4 boutons (Sit / SitHandi / Corridor / Blank)
	 */
	@FXML
	private void initialize() 
	{
		Utils.init_pan_left(btn_sit, btn_handi, couloir, empty_zone);		
	}
	
	public void setMainApp(MainApp mainApp) {
	}

	/**
	 * Event sur click du bouton Sit
	 * Gestion du click droit ou gauche
	 * Sur click gauche ajout d'un Sit au GridPane(Plan salle)
	 * Sur click droit ajout de plusieurs sieges Sit (Choix du nombre via Pop-up)
	 */
	@FXML
	private void handleBtnClickSit(MouseEvent event) {
		MouseButton button = event.getButton();		
		if(button == MouseButton.PRIMARY )
		{				
			Utils.ImplementGridPane("chaise", this.gridpan, this.colAndRow, this.allNodes, 1);
			
		}
		else
		{
			if(button == MouseButton.SECONDARY)
			{				
				TextInputDialog dialog = new TextInputDialog("1");
				dialog.setTitle("How many ?");
				
				dialog.setContentText("Please enter a number :");

				// Traditional way to get the response value.
				Optional<String> result = dialog.showAndWait();
				
				if (result.isPresent())
				{					
					Utils.ImplementGridPane("chaise", this.gridpan, this.colAndRow, this.allNodes, Integer.parseInt(result.get()));
				}
			}
		}
	}
	/**
	 * Event sur click du bouton SitHandi
	 * Gestion du click droit ou gauche
	 * Sur click gauche ajout d'un SitHandi au GridPane(Plan salle)
	 * Sur click droit ajout de plusieurs sieges SitHandi (Choix du nombre via Pop-up)
	 */
	@FXML
	private void handleBtnClickSit_Handi(MouseEvent event) {
		MouseButton button = event.getButton();
		
		if(button == MouseButton.PRIMARY)
		{						
			Utils.ImplementGridPane("chaise_handicap", this.gridpan, this.colAndRow, this.allNodes,1);
		}
		else
		{
			if(button == MouseButton.SECONDARY)
			{				
				TextInputDialog dialog = new TextInputDialog("1");
				dialog.setTitle("How many ?");
				
				dialog.setContentText("Please enter a number :");

				// Traditional way to get the response value.
				Optional<String> result = dialog.showAndWait();
				
				if (result.isPresent())
				{					
					Utils.ImplementGridPane("chaise_handicap", this.gridpan, this.colAndRow, this.allNodes, Integer.parseInt(result.get()));
				}			
			}
		}
	}
	/**
	 * Event sur click du bouton EmptyZone
	 * Gestion du click droit ou gauche
	 * Sur click gauche ajout d'un EmptyZone au GridPane(Plan salle)
	 * Sur click droit ajout de plusieurs sieges EmptyZone (Choix du nombre via Pop-up)
	 */
	@FXML
	private void handleBtnClickEmpty_Zone(MouseEvent event) {
		
		MouseButton button = event.getButton();
		if(button == MouseButton.PRIMARY)
		{			
			Utils.ImplementGridPane("empty_zone", this.gridpan, this.colAndRow, this.allNodes, 1);
		}
		else
		{
			if(button == MouseButton.SECONDARY)
			{				
				TextInputDialog dialog = new TextInputDialog("1");
				dialog.setTitle("How many ?");
				
				dialog.setContentText("Please enter a number :");

				// Traditional way to get the response value.
				Optional<String> result = dialog.showAndWait();
				if (result.isPresent())
				{					
					Utils.ImplementGridPane("empty_zone", this.gridpan, this.colAndRow, this.allNodes, Integer.parseInt(result.get()));
				}				
			}
		}
	}
	
	/**
	 * Event sur click du bouton Corridor
	 * Sur click gauche ajout d'un EmptyZone au GridPane(Plan salle)
	 */
	@FXML
	private void handleBtnClick_Couloir(MouseEvent event)
	{		
		Utils.ImplementGridPane("couloir", this.gridpan, this.colAndRow, this.allNodes, 1);
	}
	
	/**
	 * En mode creation d'un plan de salle, edition des Elements ins�r�s.
	 * Permet de supprimer le dernier �l�ment ins�rer.
	 * L'op�ration peut etre r�p�t�e jusqu'au dernier �l�ment ins�r�
	 */
	@FXML
	private void handleBtnClick_DeleteLastNode(MouseEvent event)
	{		
		ObservableList<Node> childrens = gridpan.getChildren();	
			
		childrens.remove(childrens.size()-1);		 
		
		
		//G�rer erreur OutBoundException
			if(this.colAndRow[0] == 0 && this.colAndRow[1] > 0)
			{
				this.colAndRow[0] = 34;
				this.colAndRow[1]--;
			}
			else
			{
				this.colAndRow[0]--;
			}
	}
	
	/**
	 * Bouton permettant de g�rer l'import d'un fichier XML depuis le systeme de fichier.
	 */
	@FXML
	public void handleImportXML() throws Exception
	{
		Parcourir p = new Parcourir(this.gridpan);
		p.start();
	}
	
	/**
	 * Bouton permettant de g�rer l'enregistrement d'un fichier XML une fois le mode cr�ation termin�
	 */
	@FXML
	public void handleGenerateXML() throws IOException, ParserConfigurationException, SAXException, SQLException, TransformerException {
		ParseurXML px = new ParseurXML(allNodes);	
			px.Generation();				
	}	
}
