package fr.cesi.module.moduleplansalle.application;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class Database {

    private static final String DBMS = "mysql";
    private static final String USERNAME = "app";
    private static final String PASSWORD = "KW7wIwcEClLdOiB2";
    private static final String SERVER_NAME = "37.59.36.201";
    private static final String PORT = "3306";
    private static final String DB_NAME = "ProjetJAVA";

    private Connection conn = null;

    private static Database ourInstance = new Database();

    public static Database getInstance() {
        return ourInstance;
    }

    public Database() {
        Properties connectionProps = new Properties();
        connectionProps.put("user", USERNAME);
        connectionProps.put("password", PASSWORD);

        try {
            this.conn = DriverManager.getConnection(
                "jdbc:" + DBMS + "://" +
                        SERVER_NAME +
                        ":" + PORT + "/" + DB_NAME,
            connectionProps);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("Connected to database");
    }

    public Connection getConnection() {
        return conn;
    }
}
