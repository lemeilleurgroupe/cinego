package fr.cesi.module.moduleplansalle.xmlGestion;

import java.util.Arrays;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import fr.cesi.module.moduleplansalle.application.Database;
import fr.cesi.module.moduleplansalle.contentStatic.Utils;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;

public class AnalyzeXMLHandler extends DefaultHandler {
	
	public String node;	
	private Database db = Database.getInstance();
	public String type;
	public int rang;
	public int colonne;
	private GridPane gp;	
	
	/**
	 * Getteur GridPane : gp
	 */
	public GridPane getGp() {
		return gp;
	}
	/**
	 * Setteur GridPane : gp
	 *
	 * @tag1
	 * @tag2
	 */
	public void setGp(GridPane gp) {
		this.gp = gp;
	}
	
	/**
	 * Constructeur de classe : AnalyzeXMLHandler
	 *
	 * @param Gridpane
	 */
	public AnalyzeXMLHandler(GridPane gp) {
		super();
		this.gp = gp;	
	}
	
	/**
	 * D�tection du d�but de l'analyse du document
	 */
	public void startDocument() throws SAXException{
		System.out.println("D�but de l'analyse");
	}
	
	/**
	 * D�tection de fin d'analyse du document
	 * Ouverture d'une pop-up : statut de l'import
	 */
	public void endDocument() throws SAXException{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Success Import..");
		// Header Text: null
		alert.setHeaderText(null);
		alert.setContentText("Sauvegarde du plan et des places r�alis�e avec succ�s !");
		alert.showAndWait();
	}
	
	/**
	 * D�tection de la lecture d'un noeud xml
	 *
	 * @param String namespaceURI
	 * @param String lname
	 * @param String qname
	 * @param Attributes attrs
	 */	
	public void startElement(String namespaceURI, String lname, String qname, Attributes attrs) throws SAXException
	{		 
		//Variable contenant le nom du noeud qui a cr�� l'�v�nement
		node = qname;
		type = node;	
	}
	
	/**
	 * D�tection de fin de lecture d'un noeud
	 *
	 * @param String uri
	 * @param String localName
	 * @param String qName
	 */
	public void endElement (String uri, String localName, String qName) throws SAXException
	{
		System.out.println("Fin de l'�l�ment " + qName);
	}
	
	/**
	 * Lecture de la valeur pour l'element lu. D�tection du type de noeud (Sit / SitHandi / Blank / Corridor)
	 * Association des noeuds lus aux composants Place
	 * Association de l'image + dimensionnement
	 * Ajout au GridPane
	 *
	 * @param Char[] data contient tout notre fichier.
	 * @param int start Pour r�cuperer la valeur, nous devons nous servir des limites en parametre
	 * @param int end correspond � la longueur de la chaine
	 */
	public void characters (char[] data, int start, int end)
	{
		//La variable data contient tout notre fichier.
		//Pour r�cup�rer la valeur, nous devons nous servir des limites en param�tre
		//"start" correspond � l'indice o� commence la valeur recherch�e
		//"end" correspond � la longueur de la cha�ne
		
		String str = new String(data,start, end);
		List<String> items = Arrays.asList(str.split(";"));
		
		if(items.size()>1)
		{
			switch (node) {
			case "Sit":
				Utils.ImplementGridPane("chaise", gp, Integer.parseInt(items.get(0)), Integer.parseInt(items.get(1)), db);				
				break;
				
			case "SitHandi":
				Utils.ImplementGridPane("chaise_handicap", gp, Integer.parseInt(items.get(0)), Integer.parseInt(items.get(1)), db);				
				break;
				
			case "Blank" :
				Utils.ImplementGridPane("empty_zone", gp, Integer.parseInt(items.get(0)), Integer.parseInt(items.get(1)), db);				
				break;
				
			case "Corridor":
				Utils.ImplementGridPane("couloir", gp, Integer.parseInt(items.get(0)), Integer.parseInt(items.get(1)), db);				
				break;
			default:
				break;
			}
			
		}
	}


	
}