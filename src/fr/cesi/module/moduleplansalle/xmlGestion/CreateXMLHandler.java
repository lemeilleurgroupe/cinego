package fr.cesi.module.moduleplansalle.xmlGestion;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.mysql.jdbc.PreparedStatement;

import fr.cesi.config.Database;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class CreateXMLHandler extends DefaultHandler {
	private ParseurXML pXML;
	public String node;
	public Database db = Database.getInstance();	
	public String type;
	public String rang;
	public String colonne;
	private int NbPlaces;
	
	
	public int getNbPlaces() {
		return NbPlaces;
	}
	
	public void setNbPlaces(int nbPlaces) {
		NbPlaces = nbPlaces;
	}
	public int getLastInsertIdSalle() {
		return lastInsertIdSalle;
	}
	
	public void setLastInsertIdSalle(int lastInsertIdSalle) {
		this.lastInsertIdSalle = lastInsertIdSalle;
	}
	
	public int getLastInsertIdXML() {
		return lastInsertIdXML;
	}
	
	
	public void setLastInsertIdXML(int lastInsertIdXML) {
		this.lastInsertIdXML = lastInsertIdXML;
	}

	private int lastInsertIdSalle;
	private int lastInsertIdXML;
	
	
	public ParseurXML getpXML() {
		return pXML;
	}
	
	public void setpXML(ParseurXML pXML) {
		this.pXML = pXML;
	}
	
	/**
	 * Constructeur class CreateXMLHandler extends DefaultHandler
	 *
	 * @param ParseurXML pXML
	 * @param int idSalle
	 * @param int idXML
	 */
	public CreateXMLHandler(ParseurXML pXML,int idSalle, int idXML) {
		super();
		this.pXML = pXML;
		this.lastInsertIdSalle = idSalle;
		this.lastInsertIdXML = idXML;
		this.NbPlaces = 0;
	}
	
	/**
	 * D�tection du d�but de l'analyse du document
	 */
	public void startDocument() throws SAXException{}
	
	/**
	 * D�tection de fin d'analyse du document
	 * Mise � jour du nombre de place associ� � la salle cr��e
	 */
	public void endDocument() throws SAXException{
		String SqlReq = "UPDATE SALLE SET SALLE_NbPlaces = ? WHERE SALLE_ID = ?";
		try {
			PreparedStatement stat = (PreparedStatement) db.getConnection().prepareStatement(SqlReq);
			stat.setInt(1, this.NbPlaces);
			stat.setInt(2, this.lastInsertIdSalle);
			stat.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Success..");
		alert.setHeaderText(null);
		alert.setContentText("Plan et Places sauvegard�s avec succ�s !");
		alert.showAndWait();
	}
	
	/**
	 * D�tection de la lecture d'un noeud xml
	 *
	 * @param String namespaceURI
	 * @param String lname
	 * @param String qname
	 * @param Attributes attrs
	 */	
	public void startElement(String namespaceURI, String lname, String qname, Attributes attrs) throws SAXException
	{		 
		//Variable contenant le nom du noeud qui a cr�� l'�v�nement
		node = qname;
		type = node;	
	}
	
	/**
	 * D�tection de fin de lecture d'un noeud
	 *
	 * @param String uri
	 * @param String localName
	 * @param String qName
	 */
	public void endElement (String uri, String localName, String qName) throws SAXException	{}
	
	/**
	 * Lecture de la valeur pour l'element lu. D�tection du type de noeud (Sit / SitHandi / Blank / Corridor)
	 * Association des noeuds lus aux composants Place
	 * Mise en base de donn�e
	 *
	 * @param Char[] data La variable data contient tout notre fichier.
	 * @param int start Pour r�cuperer la valeur, nous devons nous servir des limites en param�tre
	 * @param int end correspond � la longueur de la chaine
	 */
	public void characters (char[] data, int start, int end)
	{
		//La variable data contient tout notre fichier.
		//Pour r�cuperer la valeur, nous devons nous servir des limites en param�tre
		//"start" correspond � l'indice o� commence la valeur recherch�e
		//"end" correspond � la longueur de la chaine	
	
		if(node == "Sit" || node == "SitHandi" || node == "Blank" || node == "Corridor")
		{		
			String str = new String(data, start, end);
			List<String> items = Arrays.asList(str.split(";"));
			
			if(items.size() > 1)
			{
				colonne = items.get(0);
				rang = items.get(1);				
				String requete = "INSERT INTO PLACE (`PLACE_Colonne`,`PLACE_Ligne`,`PLACE_Type`,`SALLE_ID`,`SEANCE_ID`,`XMLFile_ID`) " + "VALUES (" + rang + ", " + colonne + ", '" + type + "',"+this.lastInsertIdSalle+", 1,"+this.lastInsertIdXML+")";				
				
				if(type == "Sit" || type == "SitHandi")
				{
					System.out.println("Place count + 1");
					this.NbPlaces++;
				}
				try {
					Statement stat = db.getConnection().createStatement();
					stat.executeUpdate(requete);
					stat.close();
				} catch (SQLException e) {e.printStackTrace();}
		
			}
		}	
			
	}


	
} 