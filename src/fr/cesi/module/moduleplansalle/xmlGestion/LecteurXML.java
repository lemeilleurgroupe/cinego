package fr.cesi.module.moduleplansalle.xmlGestion;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import javafx.scene.layout.GridPane;

public class LecteurXML {
	private String pathXML;
	private GridPane gp;
	
	/**
	 * Getteur GripPane
	 * @return Gridpane gp
	 */
	public GridPane getGp() {
		return gp;
	}
	
	/**
	 * Setteur GridPane
	 * @param GridPane gp
	 */
	public void setGp(GridPane gp) {
		this.gp = gp;
	}
	
	/**
	 * Getteur : Path fichier XML
	 * @return String pathXML
	 */
	public String getPathXML() {
		return pathXML;
	}
	
	/**
	 * Setteur : PathXML
	 *
	 * @param String pathXML
	 */
	public void setPathXML(String pathXML) {
		this.pathXML = pathXML;
	}
	
	/**
	 * Constructueur de classe Lecteur XML
	 * Classe ayant pour role de :
	 * 		- lire un XML import� 
	 * 		- g�n�rer un gridpan � partir du xml lu	 * 
	 *
	 * @param String pathXML
	 * @param GridPane gp
	 */
	public LecteurXML(String pathXML, GridPane gp) {
		this.pathXML = pathXML;
		this.gp = gp;		
	}
	
	/**
	 * Initialise un objet SAXParserFactory
	 * Appel de la methode parse qui donne acc�s � l'analyse xml noeud par noeud
	 */
	public void initSAX() throws ParserConfigurationException, SAXException, IOException
	{
		SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = factory.newSAXParser();        
        parser.parse(this.pathXML, new AnalyzeXMLHandler(this.gp));
	}
	
}
