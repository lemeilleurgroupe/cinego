package fr.cesi.module.moduleplansalle.xmlGestion;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Optional;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.TransformerException;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.xml.sax.SAXException;
import fr.cesi.module.moduleplansalle.application.Database;
import fr.cesi.module.moduleplansalle.component.Blank;
import fr.cesi.module.moduleplansalle.component.Corridor;
import fr.cesi.module.moduleplansalle.component.Sit;
import fr.cesi.module.moduleplansalle.component.SitHandi;
import javafx.scene.Node;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.GridPane;
import fr.cesi.module.moduleplansalle.xmlGestion.CreateXMLHandler;

public class ParseurXML {
	
	private Database db = new Database();	
	private GridPane gp;
	private String xmlGenerate;
	private ArrayList<Node> allNodes;	
	private int lastInsertIdXML;
	private int lastInsertIdSalle;
	
	public int getLastInsertId() {
		return lastInsertIdSalle;
	}
	
	public void setLastInsertId(int lastInsertId) {
		this.lastInsertIdSalle = lastInsertId;
	}
	
	public GridPane getGp(){
		return gp;
	}
	
	public void setGp(GridPane gp) {
		this.gp = gp;
	}
	
	public String getXmlGenerate() {
		return xmlGenerate;
	}
	
	public void setXmlGenerate(String xmlGenerate) {
		this.xmlGenerate = xmlGenerate;
	}	
	
	/**
	 * Constructueur de classe ParseurXML
	 * Classe ayant pour role de :
	 * 		- Cr�er un document XML a partir d'un GridPane implement�
	 * 		- G�rer l'import dans la BDD 
	 *
	 * @param ArrayList<Node> allNodes
	 */
	public ParseurXML(ArrayList<Node> allNodes)
	{				
		//this.gp = gp;
		this.allNodes = allNodes;
	}
	
	/**
	 * Cr�ation d'un document XML
	 * Le document stocker se trouve par d�faut dans le folder xml/ du projet
	 */
	public void Generation() throws TransformerException, IOException, ParserConfigurationException, SAXException, SQLException {
		
		Document doc = new Document();
		
		Element root = new Element("PlanSalle");
		Element cinema = new Element("GoMonPate"); //ALRDD
		root.addContent(cinema);
		Element salle = new Element("Salle");
		//Attribute nameSale = new Attribute("name","PepitoMiCorazone"); //ALRDD
	    //salle.setAttribute(nameSale);		
		cinema.addContent(salle);		
 		
		for(Node node : allNodes)
		{			
			if(node instanceof Sit)
			{				
				Element sit = new Element("Sit");
				sit.addContent(((Sit) node).xmlInput());
				salle.addContent(sit);				
			}
			if(node instanceof SitHandi)
			{
				Element child = new Element("SitHandi");
				child.addContent(((SitHandi) node).xmlInput());
				salle.addContent(child);	
			}
			if(node instanceof Corridor)
			{
				Element child = new Element("Corridor");
				child.addContent(((Corridor) node).xmlInput());
				salle.addContent(child);	
			}
			if(node instanceof Blank)
			{
				Element child = new Element("Blank");
				child.addContent(((Blank) node).xmlInput());
				salle.addContent(child);	
			}			
		}
		
		doc.setRootElement(root);
		
		XMLOutputter outter=new XMLOutputter();
		outter.setFormat(Format.getPrettyFormat());	
		
		TextInputDialog inDialog = new TextInputDialog();
		inDialog.setTitle("Save As..");
		inDialog.setHeaderText("Nom du fichier XML cr��");
		inDialog.setContentText("Nom :");
		Optional<String> textIn = inDialog.showAndWait();
		
		if (textIn.isPresent()) {
			outter.output(doc, new FileWriter(new File("xml/" + textIn.get().replace(" ", "") + ".xml")));
		}			
		//Creation d'une occurence XML
		this.pathXMLIntoBDD(textIn.get().replace(" ", ""));
		//Creation d'une nouvelle Salle
		this.CreateNewRoom();
		//Push en BDD des places
		this.importBDD(textIn.get().replace(" ", ""));		
	}
	
	/**
	 * Import en base de donn�e du fichier xml cr��
	 *
	 * @param String nameXML
	 */
	public void importBDD(String nameXML) throws ParserConfigurationException, SAXException, IOException
	{
		 SAXParserFactory factory = SAXParserFactory.newInstance();
	     SAXParser parser = factory.newSAXParser();         
	     parser.parse("xml/"+ nameXML +".xml", new CreateXMLHandler(this,this.searchlastInsertIdSalle(),this.lastInsertIdXML));			
	}
	
	/**
	 * Recherche en base de donn�e le dernier XML_ID inser� en base
	 *
	 * @return int lastInsertID
	 */
	public int searchlastInsertIdXML()
	{		
		try {
			Statement stat0 = db.getConnection().createStatement();
			ResultSet rs = stat0.executeQuery("SELECT MAX(XMLFile_ID) FROM XMLFile");
			while(rs.next())
			{
			System.out.println(rs.getString(1));
			this.lastInsertIdXML = Integer.valueOf(rs.getString(1));
			System.out.println(this.lastInsertIdXML);
			}
			
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}		
		return this.lastInsertIdXML;
	}
	
	/**
	 * Recherche en base de donn�e le dernier IDSalle inser� en base
	 *
	 * @return int lastInsertIDSalle
	 */
	public int searchlastInsertIdSalle()
	{		
		try {
			Statement stat0 = db.getConnection().createStatement();
			ResultSet rs = stat0.executeQuery("SELECT MAX(SALLE_ID) FROM SALLE");
			while(rs.next())
			{		
				this.lastInsertIdSalle = Integer.valueOf(rs.getString(1));			
			}			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}		
		return this.lastInsertIdSalle;
	}
	
	/**
	 * Insert en base de donn�e le nom du fichier xml pass� en param�tre
	 * @param String nameFile
	 */
	public void pathXMLIntoBDD(String nameFile) throws SQLException
	{
		this.xmlGenerate = "xml/"+ nameFile+".xml'";
		String requete = "INSERT INTO XMLFile(`XMLFile_LINK`) VALUES ('xml/" + nameFile + ".xml')";		
		try {
			Statement stat = db.getConnection().createStatement();
			stat.executeUpdate(requete);
			this.searchlastInsertIdXML();
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Creation d'une nouvelle salle en base de donn�e
	 */
	public void CreateNewRoom() throws SQLException
	{			
		// create a Statement from the connection
		Statement statement = db.getConnection().createStatement();
		System.out.println("INSERT INTO SALLE VALUES("+100+","+20+","+10+","+99+","+5+","+this.searchlastInsertIdXML()+")");
		// insert the data
		statement.executeUpdate("INSERT INTO SALLE(`SALLE_NbPlaces`,`SALLE_NbRang`,`SALLE_NbColonne`,`SALLE_Num`,`CINE_ID`,`XMLFile_ID`) VALUES("+0+","+0+","+0+","+99+","+5+","+this.searchlastInsertIdXML()+")");
	}
}