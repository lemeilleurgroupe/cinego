package fr.cesi.module.moduleplansalle.contentStatic;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import com.mysql.jdbc.PreparedStatement;
import fr.cesi.module.moduleplansalle.application.Database;
import fr.cesi.module.moduleplansalle.component.Blank;
import fr.cesi.module.moduleplansalle.component.Corridor;
import fr.cesi.module.moduleplansalle.component.Sit;
import fr.cesi.module.moduleplansalle.component.SitHandi;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;

public class Utils {
	
	/**
	 * Initilise le panneau gauche pour la frame cr�ationd d'un plan de salle
	 *	Ajout des 4 boutons Images pour impl�menter le gridpan
	 * @param Button btnSit
	 * @param Button btnHandi
	 * @param Button btnCouloir
	 * @param Button btnEmptyZone
	 */
	public static void init_pan_left(Button btnSit,Button btnHandi,Button btnCouloir,Button btnEmptyZone)
	{
		ImageView imgView = new ImageView();
		File file = new File("Images/chaise.png");
		Image img = new Image(file.toURI().toString(), 50, 50, false, false);
		imgView.setImage(img);
		btnSit.setGraphic(imgView);

		ImageView imgV = new ImageView();
		File file1 = new File("Images/chaise_handicap.png");
		Image img1 = new Image(file1.toURI().toString(), 50, 50, false, false);
		imgV.setImage(img1);
		btnHandi.setGraphic(imgV);

		ImageView imgV1 = new ImageView();
		File file2 = new File("Images/couloir.png");
		Image img2 = new Image(file2.toURI().toString(), 50, 50, false, false);
		imgV1.setImage(img2);
		btnCouloir.setGraphic(imgV1);

		ImageView imgV2 = new ImageView();
		File file3 = new File("Images/empty_zone.png");
		Image img3 = new Image(file3.toURI().toString(), 50, 50, false, false);
		imgV2.setImage(img3);
		btnEmptyZone.setGraphic(imgV2);
	}
	
	/**
	 * Implemente une liste de Node ArrayList<Node> en fonction d'une type de composant
	 *
	 * @param String pictName
	 * @param GridPane gp
	 * @param int[] colAndRow
	 * @param ArrayList<Node> allNodes
	 * @param int nb
	 */
	public static void ImplementGridPane(String pictName, GridPane gp, int[] colAndRow, ArrayList<Node> allNodes, int nb)
	{		 
		for(int i = 0 ; i < nb ; i++)
		{
			ImageView imgView = new ImageView();
			File file = new File("images/" + pictName + ".png");
			Image img = null;
			
			switch (pictName) {
				case "chaise":
					img = new Image(file.toURI().toString(), 23, 23, false, false);
					Sit s = new Sit(colAndRow[0], colAndRow[1]);					
					allNodes.add(s);
				break;
					
				case "chaise_handicap":
					img = new Image(file.toURI().toString(), 23, 23, false, false);
					SitHandi sh = new SitHandi(colAndRow[0], colAndRow[1]);
					allNodes.add(sh);
				break;
					
				case "empty_zone":
					img = new Image(file.toURI().toString(), 23, 23, false, false);
					Blank b = new Blank(colAndRow[0], colAndRow[1]);
					allNodes.add(b);
				break;
					
				case "couloir":
					img = new Image(file.toURI().toString(), 15, 20, false, false);
					Corridor c = new Corridor(colAndRow[0], colAndRow[1]);
					allNodes.add(c);
				break;
			}
			
			imgView.setImage(img);
			gp.add(imgView, colAndRow[0] , colAndRow[1]);
			
			
			if(colAndRow[0] == 34)
			{
				colAndRow[0] = 0;
				colAndRow[1]++;
			}
			else
			{
				colAndRow[0]++;
			}
		}
	}
	
	/**
	 * Lecture du gridpan et insertion de chaque node en base 
	 *
	 * @param String pictName
	 * @param GridPane gp
	 * @param int item0
	 * @param int item1
	 * @param Database db
	 */
	public static void ImplementGridPane(String pictName, GridPane gp, int item0, int item1,  Database db)
	{
		ImageView imgView = new ImageView();
		File file = new File("images/" + pictName + ".png");
		Image img = null;
		String requete;
		Statement stat;
		switch (pictName) {
		
			case "chaise":
				img = new Image(file.toURI().toString(), 23, 23, false, false);
				imgView.setImage(img);
				gp.add(imgView, item0, item1);
				requete = "INSERT INTO PLACE (`PLACE_Colonne`,`PLACE_Ligne`,`PLACE_Type`,`SALLE_ID`,`SEANCE_ID`,`XMLFile_ID`) " + "VALUES (" + item1 + ", " + item0 + ",'Sit', 11, 1, 1)";
				
				try {
					stat = db.getConnection().createStatement();
					stat.executeUpdate(requete);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
				
			case "chaise_handicap":
				img = new Image(file.toURI().toString(), 23, 23, false, false);
				imgView.setImage(img);
				gp.add(imgView, item0, item1);
				requete = "INSERT INTO PLACE (`PLACE_Colonne`,`PLACE_Ligne`,`PLACE_Type`,`SALLE_ID`,`SEANCE_ID`,`XMLFile_ID`) " + "VALUES (" + item1 + ", " + item0 + ",'SitHandi', 11, 1, 1)";
				
				try {
					stat = db.getConnection().createStatement();
					stat.executeUpdate(requete);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				break;
			case "empty_zone":
				img = new Image(file.toURI().toString(), 23, 23, false, false);
				imgView.setImage(img);
				gp.add(imgView, item0, item1);
				requete = "INSERT INTO PLACE (`PLACE_Colonne`,`PLACE_Ligne`,`PLACE_Type`,`SALLE_ID`,`SEANCE_ID`,`XMLFile_ID`) " + "VALUES (" + item1 + ", " + item0 + ",'Blank', 11, 1, 1)";
				try {
					stat = db.getConnection().createStatement();
					stat.executeUpdate(requete);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				break;
			case "couloir":
				img = new Image(file.toURI().toString(), 15, 20, false, false);
				imgView.setImage(img);
				gp.add(imgView, item0, item1);
				requete = "INSERT INTO PLACE (`PLACE_Colonne`,`PLACE_Ligne`,`PLACE_Type`,`SALLE_ID`,`SEANCE_ID`,`XMLFile_ID`) " + "VALUES (" + item1 + ", " + item0 + ",'Corridor', 11, 1, 1)";
				try {
					stat = db.getConnection().createStatement();
					stat.executeUpdate(requete);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				break;
		}
	}
	
	/**
	 * Construit un fichier xml � partir d'un numero de salle
	 * Fichier stocker dans le folder projet xml/
	 *
	 * @param int numSalle
	 * @param Database db 
	 */
	public static void CreateXMLAndDownload(int numSalle,fr.cesi.config.Database database) throws SQLException, IOException
	{		
		
		int XMLID = 0;
		Statement stat0 = database.getConnection().createStatement();
		ResultSet rs = stat0.executeQuery("SELECT SALLE.XMLFile_ID FROM SALLE WHERE SALLE.SALLE_ID = " + numSalle);
		while(rs.next())
		{
			XMLID = rs.getInt(1);
		}
		System.out.println(XMLID);
		String reqSQL = "SELECT Place_Ligne, PLACE_Colonne, PLACE_Type, PLACE_ID FROM PLACE WHERE XMLFile_ID = ? order by PLACE_ID";
		PreparedStatement stmt = (PreparedStatement) database.getConnection().prepareStatement(reqSQL);
		stmt.setInt(1,XMLID);
		
		ResultSet rs1 = stmt.executeQuery();		
		HashMap<String,String> content = new HashMap<String,String>();
		while(rs1.next())
		{
			String tmpStr = rs1.getString("PLACE_Type");
			String tmp = rs1.getInt("Place_Ligne") +";"+ rs1.getInt("PLACE_Colonne");
			content.put(tmp, tmpStr);
		}
		
		Document doc = new Document();
		
		Element root = new Element("PlanSalle");
		Element cinema = new Element("GoMonPate");
		root.addContent(cinema);
		Element salle = new Element("Salle");	
		cinema.addContent(salle);		
		String laValue;
		String laKey;
		for (Map.Entry<String, String> entry : content.entrySet()) 
		{
			laValue = entry.getValue();
			laKey = entry.getKey();			
			switch (laValue) {
			case "Sit":
				Element sit = new Element("Sit");
				sit.addContent(laKey);
				salle.addContent(sit);	
				break;
			case "SitHandi":
				Element child0 = new Element("SitHandi");
				child0.addContent(laKey);
				salle.addContent(child0);	
				break;
			case "Blank":
				Element child = new Element("Blank");
				child.addContent(laKey);
				salle.addContent(child);
				break;
			case "Corridor":
				Element child1 = new Element("Corridor");
				child1.addContent(laKey);
				salle.addContent(child1);
				break;
			
			}		
		}
		
		doc.setRootElement(root);
		
		XMLOutputter outter=new XMLOutputter();
		outter.setFormat(Format.getPrettyFormat());	
		
		TextInputDialog inDialog = new TextInputDialog();
		inDialog.setTitle("Save As..");
		inDialog.setHeaderText("Nom du fichier XML cr��");
		inDialog.setContentText("Nom :");
		Optional<String> textIn = inDialog.showAndWait();
		
		if (textIn.isPresent()) {
			outter.output(doc, new FileWriter(new File("xml/" + textIn.get().replace(" ", "") + ".xml")));
		}		
	}
}
