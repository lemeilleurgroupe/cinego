package fr.cesi.module.moduleplansalleuser.contentStatic;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import fr.cesi.config.Database;
import fr.cesi.module.moduleplansalleuser.bddObject.Seance;
import fr.cesi.module.moduleplansalleuser.component.Blank;
import fr.cesi.module.moduleplansalleuser.component.Place;
import fr.cesi.module.moduleplansalleuser.component.Corridor;
import fr.cesi.module.moduleplansalleuser.component.Sit;
import fr.cesi.module.moduleplansalleuser.component.SitHandi;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;

public class Utils {		
	
	/**
	 *  Implementation du gridpane pour choix de place / analyse des places d�ja r�serv�es (affichage des places gris�s)
	 * @param cmp
	 * @param gp
	 * @param isReserved
	 */
	public static void ImplementGridPane(Place cmp, GridPane gp, Boolean isReserved)
	{
		Sit chaise;
		SitHandi chaiseHandi;
		Corridor crdr;
		Blank blk;
		ImageView imgView = new ImageView();
		File file;
		Image img = null;
		
		if(isReserved)
		{
			if(cmp instanceof Sit)
			{
				chaise = (Sit) cmp;	
				file = new File("images/chaise.png");
				img = new Image(file.toURI().toString(), 18, 18, false, false);
				//imgView.setImage(img);
				chaise.setGraphic(new ImageView(img));
				chaise.getStyleClass().add("buttonClassReserved");
				chaise.setDisable(true);
				gp.add(chaise, chaise.getPosX() , chaise.getPosY());
			}
			
			if(cmp instanceof SitHandi)
			{
				chaiseHandi = (SitHandi) cmp;
				file = new File("images/chaise_handicap.png");
				chaiseHandi.getStyleClass().add("buttonClassReserved");
				chaiseHandi.setDisable(true);
				img = new Image(file.toURI().toString(), 18, 18, false, false);
				//imgView.setImage(img);
				chaiseHandi.setGraphic(new ImageView(img));
				gp.add(chaiseHandi, chaiseHandi.getPosX(), chaiseHandi.getPosY());
			}
		}
		else
		{			
			if(cmp instanceof Sit)
			{
				chaise = (Sit) cmp;		
				file = new File("images/chaise.png");
				chaise.getStyleClass().add("buttonClassNotReserved");
				img = new Image(file.toURI().toString(), 18, 18, false, false);
				//imgView.setImage(img);
				
				//chaise.setImgView(imgView);
				chaise.setGraphic(new ImageView(img));			
				gp.add(chaise, chaise.getPosX() , chaise.getPosY());
			}
			if(cmp instanceof SitHandi)
			{
				chaiseHandi = (SitHandi) cmp;
				file = new File("images/chaise_handicap.png");
				chaiseHandi.getStyleClass().add("buttonClassNotReserved");
				img = new Image(file.toURI().toString(), 18, 18, false, false);
				//imgView.setImage(img);
				chaiseHandi.setGraphic(new ImageView(img));				
				gp.add(chaiseHandi, chaiseHandi.getPosX(), chaiseHandi.getPosY());
			}
			if(cmp instanceof Corridor)
			{
				crdr = (Corridor) cmp;
				file = new File("images/couloir.png");
				img = new Image(file.toURI().toString(), 10, 15, false, false);
				imgView.setImage(img);
				gp.add(imgView,crdr.getPosX(),crdr.getPosY());
			}
			if(cmp instanceof Blank)
			{
				blk = (Blank)cmp;
				file = new File("images/empty_zone.png");
				img = new Image(file.toURI().toString(), 18, 18, false, false);
				imgView.setImage(img);
				gp.add(imgView, blk.getPosX(), blk.getPosY());
			}
		}		
	}
	/**
	 * Mise en collection des si�ges choisies pour reservation, parcours du gridpane 
	 * @param gp
	 * @return ArrayList<Place> sitToBeConvertInReserve
	 */
	public static ArrayList<Place> GetComponentSelectedForReserved(GridPane gp)
	{
		ArrayList<Place> sitToBeConvertInReserve = new ArrayList<Place>();
		ObservableList<Node> child = gp.getChildren();
		
		for(int i = 0 ; i < child.size();i++)
		{
			if(child.get(i) instanceof Sit)
			{
				if(((Sit)child.get(i)).getColored())
				{
					sitToBeConvertInReserve.add((Sit)child.get(i));
				}
			}
			if(child.get(i) instanceof SitHandi)
			{
				if(((SitHandi)child.get(i)).getColored())
				{
					sitToBeConvertInReserve.add((SitHandi)child.get(i));
				}
			}
		}	
		return sitToBeConvertInReserve;
	}

	/**
	 * Implementation du gridpane en mode creation, ajout d'un node en fonction du nom pictName
	 * @param pictName
	 * @param gp
	 * @param colAndRow
	 * @param allNodes
	 * @param nb
	 */
	public static void ImplementGridPane(String pictName, GridPane gp, int[] colAndRow, ArrayList<Node> allNodes, int nb)
	{		
		for(int i = 0 ; i < nb ; i++)
		{
			ImageView imgView = new ImageView();
			File file = new File("images/" + pictName + ".png");
			Image img = null;
			
			switch (pictName) {
				case "chaise":
					img = new Image(file.toURI().toString(), 23, 23, false, false);
					Sit s = new Sit((Integer) null,colAndRow[0], colAndRow[1]);					
					allNodes.add(s);
				break;
					
				case "chaise_handicap":
					img = new Image(file.toURI().toString(), 23, 23, false, false);
					SitHandi sh = new SitHandi((Integer) null,colAndRow[0], colAndRow[1]);
					allNodes.add(sh);
				break;
					
				case "empty_zone":
					img = new Image(file.toURI().toString(), 23, 23, false, false);
					Blank b = new Blank((Integer) null,colAndRow[0], colAndRow[1]);
					allNodes.add(b);
				break;
					
				case "couloir":
					img = new Image(file.toURI().toString(), 15, 20, false, false);
					Corridor c = new Corridor((Integer) null,colAndRow[0], colAndRow[1]);
					allNodes.add(c);
				break;
			}
			
			imgView.setImage(img);
			
			gp.add(imgView, colAndRow[0] , colAndRow[1]);			
			
			if(colAndRow[0] == 34)
			{
				colAndRow[0] = 0;
				colAndRow[1]++;
			}
			else
			{
				colAndRow[0]++;
			}
		}
	}
	/**
	 * ajout d'un node du gridpane en base de donn�e selon le param�tre pictName
	 * @param pictName
	 * @param gp
	 * @param item0
	 * @param item1
	 * @param db
	 */
	public static void ImplementGridPane(String pictName, GridPane gp, int item0, int item1,  Database db)
	{
		ImageView imgView = new ImageView();
		File file = new File("images/" + pictName + ".png");
		Image img = null;
		String requete;
		Statement stat;
		switch (pictName) {
		
			case "chaise":
				img = new Image(file.toURI().toString(), 23, 23, false, false);
				imgView.setImage(img);
				gp.add(imgView, item0, item1);
				requete = "INSERT INTO PLACE (`PLACE_Colonne`,`PLACE_Ligne`,`PLACE_Type`,`SALLE_ID`,`SEANCE_ID`,`XMLFile_ID`) " + "VALUES (" + item1 + ", " + item0 + ",'Sit', 11, 1, 1)";
				
				try {
					stat = db.getConnection().createStatement();
					stat.executeUpdate(requete);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				break;
				
			case "chaise_handicap":
				img = new Image(file.toURI().toString(), 23, 23, false, false);
				imgView.setImage(img);
				gp.add(imgView, item0, item1);				
				requete = "INSERT INTO PLACE (`PLACE_Colonne`,`PLACE_Ligne`,`PLACE_Type`,`SALLE_ID`,`SEANCE_ID`,`XMLFile_ID`) " + "VALUES (" + item1 + ", " + item0 + ",'SitHandi', 11, 1, 1)";
				
				try {
					stat = db.getConnection().createStatement();
					stat.executeUpdate(requete);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				break;
			case "empty_zone":
				img = new Image(file.toURI().toString(), 23, 23, false, false);
				imgView.setImage(img);
				gp.add(imgView, item0, item1);
				requete = "INSERT INTO PLACE (`PLACE_Colonne`,`PLACE_Ligne`,`PLACE_Type`,`SALLE_ID`,`SEANCE_ID`,`XMLFile_ID`) " + "VALUES (" + item1 + ", " + item0 + ",'Blank', 11, 1, 1)";
				try {
					stat = db.getConnection().createStatement();
					stat.executeUpdate(requete);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				break;
			case "couloir":
				img = new Image(file.toURI().toString(), 15, 20, false, false);
				imgView.setImage(img);
				gp.add(imgView, item0, item1);
				requete = "INSERT INTO PLACE (`PLACE_Colonne`,`PLACE_Ligne`,`PLACE_Type`,`SALLE_ID`,`SEANCE_ID`,`XMLFile_ID`) " + "VALUES (" + item1 + ", " + item0 + ",'Corridor', 11, 1, 1)";
				try {
					stat = db.getConnection().createStatement();
					stat.executeUpdate(requete);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				break;
		}
	}

	/**
	 * Generation du gridpan pour choix de place d'un utilisateur. Analyse des places r�serv�s et reservable. Impl�mentation du gridpan
	 * @param gp
	 * @param s
	 * @param dbase
	 * @throws SQLException
	 */
	public static void GenerateGridPlan(GridPane gp, Seance s, Database dbase) throws SQLException
	{
			//R�cup�rer toute les places de la salle ou la seance sera diffus�
			ArrayList<Place>toutesLesPlacesDeLaSalle = new ArrayList<Place>();
			String lesPlacesDeLaSalleAssocieeALaSeance = "SELECT PLACE.PLACE_ID, PLACE.PLACE_Ligne, PLACE.PLACE_Colonne, PLACE.PLACE_Type FROM PLACE, SEANCE, SALLE WHERE SEANCE.SALLE_ID = SALLE.SALLE_ID AND SALLE.SALLE_ID = PLACE.SALLE_ID AND SEANCE.SEANCE_ID = ?";
			java.sql.PreparedStatement stat = dbase.getConnection().prepareStatement(lesPlacesDeLaSalleAssocieeALaSeance);
			stat.setInt(1, s.getId());
			ResultSet rs = stat.executeQuery();
			Sit chaise;
			SitHandi chaiseHandi;
			Blank blk;
			Corridor crdr;
			String placetype;
			while(rs.next())
			{
				placetype = rs.getString("PLACE_Type");
				switch (placetype) {
				case "Sit":									
					chaise = new Sit(rs.getInt("PLACE_ID"),rs.getInt("PLACE_Ligne"),rs.getInt("PLACE_Colonne"));
					toutesLesPlacesDeLaSalle.add(chaise);
					break;
				case "SitHandi":
					chaiseHandi = new SitHandi(rs.getInt("PLACE_ID"),rs.getInt("PLACE_Ligne"),rs.getInt("PLACE_Colonne"));
					toutesLesPlacesDeLaSalle.add(chaiseHandi);
					break;
				case "Blank":		
					blk = new Blank(rs.getInt("PLACE_ID"),rs.getInt("PLACE_Ligne"),rs.getInt("PLACE_Colonne"));
					toutesLesPlacesDeLaSalle.add(blk);
					break;
				case "Corridor":
					crdr = new Corridor(rs.getInt("PLACE_ID"),rs.getInt("PLACE_Ligne"),rs.getInt("PLACE_Colonne"));
					toutesLesPlacesDeLaSalle.add(crdr);
					break;
				}				
			}
			
			stat.close();
			rs.close();
			
			//R�cuperer les r�servations associ� � cette seance + les places 
			ArrayList<Integer>idsPlacesReservees = new ArrayList<Integer>();
			String lesPlacesAssocieesAUneResa = "SELECT RESERVATION_PLACE.PLACE_ID FROM RESERVATION_PLACE,RESERVATION WHERE RESERVATION_PLACE.RES_ID = RESERVATION.RES_ID AND RESERVATION.SEANCE_ID = ?";
			java.sql.PreparedStatement stat1 = dbase.getConnection().prepareStatement(lesPlacesAssocieesAUneResa);
			stat1.setInt(1, s.getId());
			ResultSet rs1 = stat1.executeQuery();
			while(rs1.next())
			{
				System.out.println(rs1.getInt("RESERVATION_PLACE.PLACE_ID"));
				idsPlacesReservees.add(rs1.getInt("RESERVATION_PLACE.PLACE_ID"));
			}
			stat1.close();
			rs1.close();
			
			for(int i = 0 ; i < toutesLesPlacesDeLaSalle.size(); i ++)
			{
				
				//Si l'id de la place lu est contenu dans la liste des ids de la seance alors cette place est reserv� sinon elle est reservable
				if(idsPlacesReservees.contains(toutesLesPlacesDeLaSalle.get(i).getID()))
				{
					ImplementGridPane(toutesLesPlacesDeLaSalle.get(i), gp, true);
				}
				else
				{
					ImplementGridPane(toutesLesPlacesDeLaSalle.get(i), gp,false);
				}
			}
	}

	

}
