package fr.cesi.module.moduleplansalleuser.component;

public class SitHandi extends Place {
	
	private Boolean isReserved; 
	public Boolean getIsReserved() {
		return isReserved;
	}
	public void setIsReserved(Boolean isReserved) {
		this.isReserved = isReserved;
	}
	public SitHandi(int ID,int posx, int posy)
	{
		super(ID,posx,posy);
	}

}
