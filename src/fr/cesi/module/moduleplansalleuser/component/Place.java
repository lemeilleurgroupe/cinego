package fr.cesi.module.moduleplansalleuser.component;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class Place extends Button{

	private int ID;
	private Boolean colored;
	public Boolean getColored() {
		return colored;
	}
	public void setColored(Boolean colored) {
		this.colored = colored;
	}
	private int posX;
	public int getPosX() {
		return posX;
	}
	public void setPosX(int posX) {
		this.posX = posX;
	}
	public int getPosY() {
		return posY;
	}
	public void setPosY(int posY) {
		this.posY = posY;
	}
	private int posY;
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}	
	
	/**
	 * Constructeur classe Place extend button. Initialise l'attribut colored � false
	 * @param int ID
	 * @param int posx
	 * @param int posy
	 */
	public Place(int ID, int posx, int posy)
	{
		this.colored = false;
		this.ID = ID;
		this.posX = posx;
		this.posY = posy;
		this.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
               if(colored) {
            	   setStyle("-fx-background-color:transparent;");
            	   colored = false;
               }
               else
               {
            	   setStyle("-fx-background-color:#E8CC06;");
            	   colored = true;
               }
            }
        });
	}	
	
	/**
	 * Concatenation de la posx avec la posy
	 * @return String posX;PosY
	 */
	public String xmlInput() {
		return this.posX + ";" + this.posY;
	}
	
	/**
	 * R�dfinition de la m�thode toString 
	 * Concatenation de la posx avec la posy
	 * @return String posX-PosY
	 */
	@Override
	public String toString()
	{
		return this.getPosX() + "-"+ this.getPosY();
	}
}
