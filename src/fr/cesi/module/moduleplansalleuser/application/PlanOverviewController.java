package fr.cesi.module.moduleplansalleuser.application;

import java.sql.SQLException;
import java.util.ArrayList;

import fr.cesi.config.Database;
import fr.cesi.module.moduleplansalleuser.bddObject.Seance;
import fr.cesi.module.moduleplansalleuser.component.Place;
import fr.cesi.module.moduleplansalleuser.contentStatic.Utils;
import fr.cesi.state.ClientState;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;

public class PlanOverviewController {
	@FXML
	private SplitPane splitpan;
	@FXML
	private AnchorPane pan_left;
	@FXML
	private AnchorPane pan_right;
	@FXML
	private GridPane gridpan;
	
	@FXML
	private Button btn_reserved;
	
	@FXML
	private Button btn_loadSeance;
	private int[] colAndRow;
	private Seance s;
	
	private ArrayList<Place> sitToBeConvertInReserve = new ArrayList<Place>();

	public ArrayList<Place> getSitToBeConvertInReserve() {
		return Utils.GetComponentSelectedForReserved(this.gridpan);
	}

	public void setSitToBeConvertInReserve(ArrayList<Place> sitToBeConvertInReserve) {
		this.sitToBeConvertInReserve = sitToBeConvertInReserve;
	}

	/**
	 * Constructeur PlanOverviewController
	 * Initialisation du tableau de coordonn�es [0;0]
	 */
	public PlanOverviewController(/*Seance s*/) 
	{		
		//this.s = s;		
		colAndRow = new int [2];
		//col
		colAndRow[0] = 0;
		//row
		colAndRow[1] = 0;		
	}

	/**
	 * Initializes the controller class. This method is automatically called after
	 * the fxml file has been loaded.
	 * Initialisation du panel gauche avec 4 boutons (Sit / SitHandi / Corridor / Blank)
	 */
	@FXML
	private void initialize() throws SQLException{		
		Seance s1 = new Seance(ClientState.getState().getSelectedSeance().getId(), null, null);
		Utils.GenerateGridPlan(gridpan, s1, Database.getInstance());
	}
	
	public void setMainApp(MainApp mainApp) {}

	@FXML
	private void onClickBtnReserved(){
		sitToBeConvertInReserve = Utils.GetComponentSelectedForReserved(this.gridpan);
	}
}
