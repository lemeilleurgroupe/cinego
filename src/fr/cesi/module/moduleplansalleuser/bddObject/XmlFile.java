package fr.cesi.module.moduleplansalleuser.bddObject;

public class XmlFile {
	private int id;
	private String path;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	
	/**
	 * Constucteur class XmlFile
	 * @param int id
	 * @param String path
	 */
	public XmlFile(int id, String path)
	{
		this.id = id;
		this.path = path;
	}
}
