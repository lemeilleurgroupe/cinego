package fr.cesi.module.moduleplansalleuser.bddObject;

import java.util.Date;

public class Seance {
	
	private int id;
	private Date date;
	private Salle s;
	public Salle getS() {
		return s;
	}
	public void setS(Salle s) {
		this.s = s;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	/**
	 *	Constructeur de la classe Seance
	 * @param int id
	 * @param Date date
	 * @Salle s
	 */
	public Seance(int id, Date date, Salle s) {
		this.id = id;
		this.date = date;
		this.s = s;
	}
	
	
	
	
}
