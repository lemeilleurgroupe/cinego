package fr.cesi.module.moduleplansalleuser.bddObject;

public class Salle {
	private int id;
	private int nbPlaces;
	private XmlFile xFile;
	public XmlFile getxFile() {
		return xFile;
	}
	public void setxFile(XmlFile xFile) {
		this.xFile = xFile;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNbPlaces() {
		return nbPlaces;
	}
	public void setNbPlaces(int nbPlaces) {
		this.nbPlaces = nbPlaces;
	}
	
	/**
	 * Constructeur classe Salle 
	 * @param int id
	 * @param int nbPlaces
	 * @XmlFile fx
	 */
	public Salle(int id, int nbPlaces, XmlFile fx)
	{
		this.id = id;
		this.nbPlaces = nbPlaces;
		this.xFile = fx;
	}
	
	
}
