package fr.cesi;

import java.io.IOException;
import java.sql.SQLException;

import fr.cesi.controller.admin.*;
import fr.cesi.model.Admin;
import fr.cesi.model.Cinema;
import fr.cesi.model.SeanceCustom;

import fr.cesi.model.DAO.CinemaDAO;

import fr.cesi.state.AdminState;
import fr.cesi.util.ParamUtil;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

@SuppressWarnings("restriction")
public class  CineGo extends Application {

	/**
	 * Cinema of the administrator
	 */
    private static final int CINEMA_ID = (int) ParamUtil.getParamUtil().getParam("cinemaId");
    /**
     * Primary stage
     */
    private Stage primaryStage;
    /**
     * Root layout
     */
    private BorderPane rootLayout;
    /**
     * ManageRootLayout
     */
    private BorderPane manageRootLayout;


    /**
     * Main method, launch the application
     */
    public CineGo() {
    	// Choose cinema (change to each cinema)
        CinemaDAO cineDao = new CinemaDAO();
        try {
            AdminState.getState().setCinema(cineDao.findById(CINEMA_ID));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("CineGo");
        this.primaryStage.setResizable(false);

        this.initRootLayout();

        this.showUserLogin();
    }

    /**
     * Initialisation of the root layout
     */
    public void initRootLayout() {
        try {
            // Load root layout from fxml file
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(CineGo.class.getResource("view/admin/RootLayout.fxml"));
            this.rootLayout = loader.load();

            // Show scene
            Scene scene = new Scene(this.rootLayout);
            this.primaryStage.setScene(scene);
            this.primaryStage.show();      
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Show the user login interface
     */
    public void showUserLogin() {
        try {
            // Load FXML
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(CineGo.class.getResource("view/admin/ConnexionAdmin.fxml"));
            GridPane personOverview = loader.load();

            // Set person overview into center of rootLayout
            this.rootLayout.setCenter(personOverview);
            
            AdminLoginController controller = loader.getController();
            controller.setMainAppAdmin(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Show the home page interface
     */
    public void showHomePage() {
        try {
            // Load FXML
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(CineGo.class.getResource("view/admin/HomePage.fxml"));
            BorderPane homePage = loader.load();

            // Set person overview into center of rootLayout
            this.rootLayout.setCenter(homePage);

            HomePageController controller = loader.getController();
            controller.setMainAppAdmin(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Show the manage root layout interface
     */
    public void showManageRootLayout() {
    	try {
            // Load root layout from fxml file
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(CineGo.class.getResource("view/admin/ManageRootLayout.fxml"));
            this.manageRootLayout = loader.load();

            // Show scene
            Scene scene = new Scene(this.manageRootLayout);
            this.primaryStage.setScene(scene);
            this.primaryStage.show();

            ManageRootLayoutController controller = loader.getController();
            controller.setMainAppAdmin(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    	
    }
    
    /**
     * Show the manage movie interface
     */
    public void showManageMovie() {
        try {
            // Load FXML
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(CineGo.class.getResource("view/admin/ManageMovie.fxml"));
            GridPane ManageMovie = loader.load();

            // Set person overview into center of rootLayout
            this.manageRootLayout.setCenter(ManageMovie);

            ManageMovieController controller = loader.getController();
            controller.setMainAppAdmin(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Show the manage seance interface
     */
    public void showManageSeance() {
        try {
            // Load FXML
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(CineGo.class.getResource("view/admin/ManageSeance.fxml"));
            GridPane ManageSeance = loader.load();

            // Set person overview into center of rootLayout
            this.manageRootLayout.setCenter(ManageSeance);

           ManageSeanceController controller = loader.getController();
           controller.setMainAppAdmin(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Show the seance edit dialog interface
     */
    public boolean showSeanceEditDialog(SeanceCustom seance) throws SQLException {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(CineGo.class.getResource("view/admin/SeanceEditDialog.fxml"));
            GridPane page = (GridPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Modifier séance");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            SeanceEditDialogController controller = loader.getController();
            controller.setMainApp(this);
            controller.setDialogStage(dialogStage);
            controller.setSeance(seance);
            
         
            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
           
        }
    }
    
    /**
     * Show the manage price interface
     */
    public void showManagePrice() {
        try {
            // Load FXML
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(CineGo.class.getResource("view/admin/ManagePrice.fxml"));
            GridPane manageCinema = loader.load();

            // Set person overview into center of rootLayout
            this.manageRootLayout.setCenter(manageCinema);

            ManageCinemaController controller = loader.getController();
            controller.setMainAppAdmin(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Show the manage room interface
     */
    public void showManageRoom() {
        try {
            // Load FXML
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(CineGo.class.getResource("view/admin/ManageRoom.fxml"));
            GridPane manageRoom = loader.load();

            //Set the controller
            ManageRoomController controller = loader.getController();
            controller.setMainAppAdmin(this);

            // Set person overview into center of rootLayout
            this.manageRootLayout.setCenter(manageRoom);



        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get the primary stage of the main application
     * @return the primary stage
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public void showCreationRoomPage() {
        try {
            // Load person overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(CineGo.class.getResource("view/admin/RoomModuleFrame.fxml"));
            GridPane roomPane = loader.load();

            // Set person overview into the center of root layout.
            this.rootLayout.setCenter(roomPane);


            //Set the controller
            RoomModuleController controller = loader.getController();
            controller.setMainAppAdmin(this);

            loader = new FXMLLoader();
			loader.setLocation(CineGo.class.getResource("module/moduleplansalle/application/PlanOverview.fxml"));
			AnchorPane newLoadedPane = loader.load();
			roomPane.getChildren().add(newLoadedPane);
            controller.setPlanOverviewControleur(loader.getController());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
