package fr.cesi;

import java.io.IOException;

import fr.cesi.config.Database;
import fr.cesi.controller.user.*;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;


public class CineGoClient extends Application{

	private Stage primaryStage;
	private BorderPane rootLayout;
	private BorderPane userRootLayout;
	private static int currentPage = 1;

	public CineGoClient() {
		// Connection to the DATABASE
		Database.getInstance().getConnection();
	}

	/**
	 * Get the selected management of the user
	 * @return
	 */
	public static int getcurrentPage() {
		return currentPage;
	}


	/**
	 * Set the selected management of the user
	 * @param currentPage
	 */
	public static void setcurrentPage(int currentPage) {
		CineGoClient.currentPage = currentPage;
	}


	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("CineGo");
		this.primaryStage.setResizable(false);

		this.initRootLayout();
		this.showManageRootLayout();
		this.showHomePage();
	}

	/**
     * Initiliazation of the root layout
     */
	public void initRootLayout() {
		try {
			// Load root layout from fxml file
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(CineGo.class.getResource("view/user/RootLayout.fxml"));
			this.rootLayout = loader.load();

			// Show scene
			Scene scene = new Scene(this.rootLayout);
			this.primaryStage.setScene(scene);
			this.primaryStage.show();      
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
     * Show the manage root layout interface
     */
	public void showManageRootLayout() {
		try {
			// Load root layout from fxml file
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(CineGo.class.getResource("view/user/UserRootLayout.fxml"));
			this.userRootLayout = loader.load();

			// Show scene
			Scene scene = new Scene(this.userRootLayout);
			this.primaryStage.setScene(scene);
			this.primaryStage.show();

			UserRootLayoutController controller = loader.getController();
			controller.setMainAppAdmin(this);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	/**
     * Show the  home page interface
     */
	public void showHomePage() {
		try {
			// Load FXML
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(CineGo.class.getResource("view/user/HomePage.fxml"));
			GridPane homePage = loader.load();

			// Set homePage into center of rootLayout
			this.userRootLayout.setCenter(homePage);

			HomePageController controller = loader.getController();
			controller.setMainAppAdmin(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Show login page.
	 */
	public void showConnexionClient() {
		try {
			// Load FXML
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(CineGo.class.getResource("view/user/ConnexionClient.fxml"));
			GridPane connexionClt = loader.load();

			// Set person overview into center of rootLayout
			this.rootLayout.setCenter(connexionClt);

			ConnexionClientController controller = loader.getController();
			controller.setMainAppAdmin(this);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Show register page.
	 */
	public void showCreationCompteClient() {
		try {
			// Load FXML
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(CineGo.class.getResource("view/user/CreationCompteClient.fxml"));
			GridPane creationClt = loader.load();

			// Set person overview into center of rootLayout
			this.rootLayout.setCenter(creationClt);

			CreationCompteClientController controller = loader.getController();
			controller.setMainAppAdmin(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
     * Show the client account interface
     */
	public void showCompteClient() {
		try {
			// Load FXML
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(CineGo.class.getResource("view/user/CompteClient.fxml"));
			TabPane compteClt = loader.load();

			// Set person overview into center of rootLayout
			this.rootLayout.setCenter(compteClt);

			CompteClientController controller = loader.getController();
			controller.setMainAppAdmin(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
     * Show the seance choice interface
     */
	public void showChoixSeance() {
		try {
			// Load FXML
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(CineGo.class.getResource("view/user/ChoixSeance.fxml"));
			GridPane choixSeance = loader.load();

			// Set person overview into center of rootLayout
			this.userRootLayout.setCenter(choixSeance);

			ChoixSeanceController controller = loader.getController();
			controller.setMainAppAdmin(this);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
     * Show the tickets interface
     */
	public void showChoixBillets() {
		try {
			// Load FXML
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(CineGo.class.getResource("view/user/ChoixBillets.fxml"));
			GridPane choixBillets = loader.load();

			// Set person overview into center of rootLayout
			this.userRootLayout.setCenter(choixBillets);

			ChoixBilletController controller = loader.getController();
			controller.setMainAppAdmin(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
     * Show the seats interface
     */
	public void showChoixPlaces() {
		try {
			// Load FXML
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(CineGo.class.getResource("view/user/ChoixPlaces.fxml"));
			GridPane choixPlaces = loader.load();

			// Set person overview into center of rootLayout
			this.userRootLayout.setCenter(choixPlaces);

			ChoixPlacesController controller = loader.getController();
			controller.setMainAppAdmin(this);

			loader = new FXMLLoader();
			loader.setLocation(CineGo.class.getResource("module/moduleplansalleuser/application/PlanOverview.fxml"));
			AnchorPane newLoadedPane = loader.load();
            choixPlaces.getChildren().add(newLoadedPane);

			controller.setGridController(loader.getController());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
     * Show the recap interface
     */
	public void showRecapCommande() {
		try {
			// Load FXML
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(CineGo.class.getResource("view/user/RecapCommande.fxml"));
			GridPane RecapCommande = loader.load();

			// Set person overview into center of rootLayout
			this.userRootLayout.setCenter(RecapCommande);

			RecapCommandeController controller = loader.getController();
			controller.setMainAppAdmin(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
     * Show the paiement interface
     */
	public void showPaiement() {
		try {
			// Load FXML
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(CineGo.class.getResource("view/user/Paiement.fxml"));
			AnchorPane paiement = loader.load();

			// Set person overview into center of rootLayout
			this.rootLayout.setCenter(paiement);

			Controller controller = loader.getController();
			controller.setMainAppAdmin(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
     * Show the cancel reservation dialog interface
     */
	public void showCancelResaDialog() {
		try {
			// Load the fxml file and create a new stage for the popup dialog.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(CineGo.class.getResource("view/user/CancelResaDialog.fxml"));
			GridPane page = (GridPane) loader.load();

			// Create the dialog Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Annuler réservation");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			// Set the person into the controller.
			CancelResaDialogController controller = loader.getController();
			controller.setMainApp(this);
			controller.setDialogStage(dialogStage);

			// Show the dialog and wait until the user closes it
			dialogStage.showAndWait();

		} catch (IOException e) {
			e.printStackTrace();

		}
	}

	/**
	 * Get the primary stage of the main application
	 * @return the primary stage
	 */
	public Stage getPrimaryStage() {
		return primaryStage;
	}
	
	/**
	 * Go to the previous page
	 */
	public void goToPreviousPage() {		 
		switch (CineGoClient.getcurrentPage()) {
		// Home page
		case 1:
			this.showManageRootLayout();
           this.showHomePage();
			break;
			// Seance
		case 2:
			this.showManageRootLayout();
           this.showChoixSeance();
			break;
			//Choix billets
		case 3:
			this.showManageRootLayout();
           this.showChoixBillets();
			break;
			// Choix places
		case 4:
			this.showManageRootLayout();
           this.showChoixPlaces();
			break;
			// Recap
		case 5:
			this.showManageRootLayout();
           	this.showRecapCommande();
			break;
		case 6:
			this.showManageRootLayout();
			this.showCompteClient();
		default:
			break;
		}
		 
	}


}
