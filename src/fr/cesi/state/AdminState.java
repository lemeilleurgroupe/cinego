package fr.cesi.state;

import fr.cesi.model.Admin;
import fr.cesi.model.Cinema;

/**
 * Le State représente l'état de l'application a un instant T.
 * On y stocke des variables qui doivent êtres utilisées dans plusieurs pages
 * Comme par exemple l'utilisateur connecté, les informations de la réservation...
 */
public class AdminState {
    private static AdminState ourState = new AdminState();

    /**
     * Admin actuellement connecté
     */
    private Admin connectedUser;

    /**
     * Cinéma associé à l'admin
     */
    private Cinema cinema;

    public static AdminState getState() {
        return ourState;
    }

    private AdminState() {
        this.connectedUser = null;
    }

    public Admin getConnectedUser() {
        return connectedUser;
    }

    public void setConnectedUser(Admin connectedUser) {
        this.connectedUser = connectedUser;
    }

    public Cinema getCinema() {
        return cinema;
    }

    public void setCinema(Cinema cinema) {
        this.cinema = cinema;
    }
}
