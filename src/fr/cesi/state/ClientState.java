package fr.cesi.state;

import fr.cesi.model.Cinema;
import fr.cesi.model.Client;
import fr.cesi.model.Seance;
import fr.cesi.model.Tarif;
import fr.cesi.module.moduleplansalleuser.component.Place;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Le State représente l'état de l'application a un instant T.
 * On y stocke des variables qui doivent êtres utilisées dans plusieurs pages
 * Comme par exemple l'utilisateur connecté, les informations de la réservation...
 */
public class ClientState {
    private static ClientState ourState = new ClientState();
    /**
     * Client actuellement connecté
     * Null si personne n'est connecté
     */
    private Client connectedUser;
    /**
     * Cinéma dans lequel la séance a été sélectionnée
     */
    private Cinema selectedCinema;
    /**
     * Liste des billets commandé.
     * Le premier élément de la pair représente le nombre de billet d'un type choisi
     * Le second représente le tarif sélectionné
     * (ex : 4 tarifs étudiant)
     */
    private ArrayList<Pair<Integer, Tarif>> billets;
    /**
     * Séance choisie par l'utilisateur
     */
    private Seance selectedSeance;
    /**
     * Carte de fidélité du client, peut etre Null
     */
    private String fidelityCard;
    /**
     * Liste des places sélectionnée
     */
    private ArrayList<Place> selectedPlaces;

    public static ClientState getState() {
        return ourState;
    }

    private ClientState() {
        this.connectedUser = null;
        this.selectedCinema = null;
        this.billets = null;
    }

    public Client getConnectedUser() {
        return connectedUser;
    }

    public void setConnectedUser(Client connectedUser) {
        this.connectedUser = connectedUser;
    }

    public Cinema getSelectedCinema() {
        return selectedCinema;
    }

    public void setSelectedCinema(Cinema selectedCinema) {
        this.selectedCinema = selectedCinema;
    }

    public ArrayList<Pair<Integer, Tarif>> getBillets() {
        return billets;
    }

    public void setBillets(ArrayList<Pair<Integer, Tarif>> billets) {
        this.billets = billets;
    }

    /**
     * Reset all reservation fields
     */
    public void resetReservation() {
        this.selectedCinema = null;
        this.billets = null;
    }

    public void setSelectedSeance(Seance selectedSeance) {
        this.selectedSeance = selectedSeance;
    }

    public Seance getSelectedSeance() {
        return selectedSeance;
    }

    public void setFidelityCard(String fidelityCard) {
        this.fidelityCard = fidelityCard;
    }

    public String getFidelityCard() {
        return fidelityCard;
    }

    public void setSelectedPlaces(ArrayList<Place> selectedPlaces) {
        this.selectedPlaces = selectedPlaces;
    }

    public ArrayList<Place> getSelectedPlaces() {
        return selectedPlaces;
    }
}
