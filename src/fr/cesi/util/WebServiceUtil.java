package fr.cesi.util;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONObject;

public class WebServiceUtil {

    /**
     * Récupère le JSON que renvoit une URL
     * @param wsUrl URL du JSON a récupérer
     * @return L'objet JSON
     * @throws IOException
     */
    public static JSONObject getJson(String wsUrl) throws IOException {
        URL url = new URL(wsUrl);
        URLConnection conn = url.openConnection();
        InputStreamReader bufferedReader = new InputStreamReader(conn.getInputStream());

        int cp;
        StringBuilder json = new StringBuilder();
        while ((cp = bufferedReader.read()) != -1) {
            json.append((char) cp);
        }
        String result = json.toString();
        bufferedReader.close();

        return new JSONObject(result);
    }

}
