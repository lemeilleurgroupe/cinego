package fr.cesi.util;

public class NumberUtils {

    /**
     * Check if the number string is a number
     * @param number String to check if it's a number
     * @return true if number is a number
     */
    public static boolean isNumber(String number) {
        try {
            Long.parseLong(number);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * Check if a String is a number and check his size
     * @param number String to check if it's a number
     * @param size length to check
     * @return true if the string is a number with "size" characters
     */
    public static boolean isNumberWithSize(String number, int size) {
        // Is number
        if (!NumberUtils.isNumber(number))
            return false;

        // is "size" digits
        return number.toCharArray().length == size;
    }

}
