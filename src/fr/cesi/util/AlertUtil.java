package fr.cesi.util;

import java.util.Optional;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import javafx.stage.Window;

public class AlertUtil {

    /**
     * Envoit une alerte à l'utilisateur
     * @param alertType Type de l'alerte. Voir l'enum AlertType
     * @param origin Origine de l'alerte. Page sur laquelle elle va apparaitre
     * @param headerText Texte court et efficace expliquant le sujet de l'alerte
     * @param contentText Explication plus précide de l'alerte
     */
    public static void sendAlert(Alert.AlertType alertType, Window origin, String headerText, String contentText) {
        Alert alert = new Alert(alertType);
        alert.initOwner(origin);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);
        alert.showAndWait();
    }

    /**
     * Envoit une alerte à l'utilisateur sans origine (peut etre utile)
     * @param alertType Type de l'alerte. Voir l'enum AlertType
     * @param headerText Texte court et efficace expliquant le sujet de l'alerte
     * @param contentText Explication plus précide de l'alerte
     */
    public static void sendAlert(Alert.AlertType alertType, String headerText, String contentText) {
        sendAlert(alertType, null, headerText, contentText);
    }

    /**
     * Evoit un message de confirmation à l'utilisateur
     * @param originStage Page d'origine
     * @param message Question a poser à l'utilisateur
     * @return true si l'utilisateur confirme, false s'il annule
     */
    public static boolean confirmationMessage(Stage originStage, String message) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setContentText(message);

        Optional<ButtonType> result = alert.showAndWait();
        return result.get() == ButtonType.OK;
    }

    /**
     * Agit comme un message de confirmation, mais propose le choix entre oui ou non.
     * @param message Question a poser à l'utilisateur
     * @return true si l'utilisateur dit oui, false s'il dit non
     */
    public static boolean yesNo(String message) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Que voulez vous faire ?");
        alert.setHeaderText(message);

        ButtonType buttonTypeOne = new ButtonType("Oui");
        ButtonType buttonTypeTwo = new ButtonType("Non");

        alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo);

        Optional<ButtonType> result = alert.showAndWait();
        return result.get() == buttonTypeOne;
    }

    /**
     * Envoit un message de cionfirmation sans ajouter de state
     * @param message Question a poser à l'utilisateur
     * @return true si l'utilisateur confirme, false s'il annule
     */
    public static boolean confirmationMessage(String message) {
        return confirmationMessage(null, message);
    }
}
