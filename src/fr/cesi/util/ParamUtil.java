package fr.cesi.util;

import jdk.nashorn.internal.parser.JSONParser;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Arrays;

public class ParamUtil {

    private final static File CONFIG_FILE = new File("src/fr/cesi/config/config.json");

    private static ParamUtil instance = new ParamUtil();
    private JSONObject config = null;

    private ParamUtil() {
        try {
            String json = new String(Files.readAllBytes(CONFIG_FILE.toPath()));
                config = new JSONObject(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ParamUtil getParamUtil() {
        return instance;
    }

    /**
     * Récupère un paramètre dans le json de configuration
     * @param key clé de la valeur a rechercher
     * @return valeur de la clé s'il trouve
     */
    public Object getParam(String key) {
        return this.findValue(config, key);
    }

    /**
     * Fonction recursive ayant pour but de retrouver une clé dans le fichier json
     * @param key clé de la valeur a rechercher
     * @return valeur de la clé s'il trouve
     */
    private Object findValue(JSONObject json, String key) {
        String[] keys = key.split("\\.");
        Object value;
        if (keys.length > 0) {
            value = json.get(keys[0]);
        } else {
            value = json.get(key);
        }

        if (value == null)
            throw new JSONException("Not found");

        if (keys.length > 1) {
            if (!(value instanceof JSONObject))
                throw new JSONException("Not found");

            String[] newKeys = Arrays.copyOfRange(keys, 1, keys.length);
            return findValue((JSONObject) value, String.join(".", newKeys));
        }
        return value;
    }
}
